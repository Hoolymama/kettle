/*
 *  JVector.cpp
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
#include <math.h>

#include "JVector.h"

JVector::JVector(): x(0.0), y(0.0), z(0.0) {}


JVector::JVector( const JVector& rhs){
	x=rhs.x;
	y=rhs.y;
	z=rhs.z;
}

JVector::JVector( double xx, double yy, double zz ){
	x=xx;
	y=yy;
	z=zz;
}

JVector::JVector( const double rhs[3]   ){
	x=rhs[0];
	y=rhs[1];
	z=rhs[2];
}

JVector::~JVector() {}

JVector& JVector::operator= ( const JVector& src )
{
	x=src.x;
	y=src.y;
	z=src.z;
	return (*this);
}

double& JVector::operator()( unsigned i ) { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	return z;
}

double JVector::operator()( unsigned i ) const { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

double& JVector::operator[]( unsigned i ) { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

double	JVector::operator[]( unsigned i )const { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

JVector JVector::operator^( const JVector& rhs) const
{
	return JVector( y * rhs.z - z * rhs.y   ,  z * rhs.x - x * rhs.z  , x * rhs.y - y * rhs.x   );
}

JVector& JVector::operator/=( double rhs ) {
	if (rhs != 0.0)  {
		double recip = 1.0 / rhs;
		x =x*recip;
		y =y*recip;
		z =z*recip;
	}
	return (*this);
}

JVector 	    JVector::operator/( double rhs ) const {
	if (rhs == 0.0) return JVector(x,y,z);
	double recip = 1.0 / rhs;
	return JVector(x*recip,y*recip,z*recip);
}

JVector& JVector::operator*=( double rhs ){
	x =x*rhs;
	y =y*rhs;
	z =z*rhs;
	return (*this);
}

JVector JVector::operator*( double rhs ) const {
	return JVector(x*rhs,y*rhs,z*rhs);
}


// JVector  JVector::operator*( const JFloatMatrix& m) const{

// 	return JVector(
// 		x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] + m.matrix[3][0],
// 		x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] + m.matrix[3][1],
// 		x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] + m.matrix[3][2]
// 	);
// }


// JVector& JVector::operator*=( const JFloatMatrix& m ){

// 	double xx,yy,zz;
	
// 	xx = x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] + m.matrix[3][0];
// 	yy = x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] + m.matrix[3][1];
// 	zz = x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] + m.matrix[3][2];
	
// 	x = xx;
// 	y=  yy;
// 	z=  zz;
	
// 	return *this;
	
// }

// JVector  JVector::normalTransform( const JFloatMatrix& m) const{

// 	return JVector(
// 		x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] ,
// 		x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] ,
// 		x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] 
// 	);
// }
	
// 	JVector& JVector::transformAsNormal( const JFloatMatrix& m ){

// 	double xx,yy,zz;
	
// 	xx = x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0];
// 	yy = x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1];
// 	zz = x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2];
	
// 	x = xx;
// 	y=  yy;
// 	z=  zz;
	
// 	return *this;
	
// }
	
	
	
JVector JVector::operator+( const JVector& rhs) const{
	return  JVector(x+rhs.x,y+rhs.y,z+rhs.z);
}

JVector& JVector::operator+=( const JVector& rhs ){
	x += rhs.x;
	y +=rhs.y;
	z +=rhs.z;
	return (*this);
}

JVector JVector::operator-() const{
	return  JVector(-x,-y,-z);
}

JVector JVector::operator-( const JVector& rhs ) const{
	return  JVector(x - rhs.x ,y- rhs.y,z- rhs.z);
}

JVector& JVector::operator-=( const JVector& rhs ){
	x -= rhs.x;
	y -=rhs.y;
	z -=rhs.z;
	return (*this);
}


double JVector::operator*( const JVector& rhs ) const{

	return (double)(x * rhs.x + y * rhs.y + z * rhs.z);
}

bool JVector::operator!=( const JVector& rhs ) const{
	if (x != rhs.x) return true;
	if (y != rhs.y) return true;
	if (z != rhs.z) return true;
	return false;
}

bool JVector::operator==( const JVector& rhs ) const{
	if (x != rhs.x) return false;
	if (y != rhs.y) return false;
	if (z != rhs.z) return false;
	return true;
}

double JVector::length() const{
	return  (double)sqrt( x * x + y * y + z * z );
}

JVector JVector::normal() const{
	
	double mag = (double)sqrt( x * x + y * y + z * z );
	if (mag > 0) {
		double recip = 1.0 / mag;
		return JVector( x * recip, y * recip,  z * recip );
	}
	return  JVector(0.0,0.0,0.0);
}

void JVector::normalize(){
	double mag = (double)sqrt( x * x + y * y + z * z );
	if (mag > 0) {
		double recip = 1.0 / mag;
		x *= recip;
		y *= recip;
		z *= recip; 
	}
}


double JVector::angle( const JVector& other ) const{
	return acos( this->normal()* other.normal() );
}

bool JVector::isEquivalent( const JVector& rhs,  double tolerance  )  const{
	if ( fabs(this->x - rhs.x) > tolerance ) return false;
	if ( fabs(this->y - rhs.y) > tolerance ) return false;
	if ( fabs(this->z - rhs.z) > tolerance ) return false;
	return true;
	
}

bool JVector::isParallel( const JVector& other, double tolerance  ) const{
	JVector cross = (*this)^other;
	if (fabs(cross.x) >tolerance ) return false;
	if (fabs(cross.y) >tolerance ) return false;
	if (fabs(cross.z) >tolerance ) return false;
	return true;
}



const JVector  JVector::zero = JVector(0.0,0.0,0.0);
const JVector  JVector::one = JVector(1.0,1.0,1.0);
const JVector  JVector::xAxis = JVector(1.0,0.0,0.0);
const JVector  JVector::yAxis = JVector(0.0,1.0,0.0);
const JVector  JVector::zAxis = JVector(0.0,0.0,1.0);
const JVector  JVector::xNegAxis = JVector(-1.0,0.0,0.0);
const JVector  JVector::yNegAxis = JVector(0.0,-1.0,0.0);
const JVector  JVector::zNegAxis = JVector(0.0,0.0,-1.0);


