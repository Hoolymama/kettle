/*
 *  ioUtils.h
 *  jtools
 *
 *  Created by Julian Mann on 27/03/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#ifndef ioutils_h
#define ioutils_h
#include "jTypes.h"

#include "byteSwap.h"

namespace ioUtil {
	
	inline int writeValue(const JIntArray & val, std::ostream &os) {
		unsigned len = val.size() ;
		for (unsigned i=0;i<len;i++) {
			int n = val[i];
			swapInt(&n);
			os.write((char*)&n , sizeof(int));
			if (os.fail()) return 0;
		}
		return 1;
	}
	
	inline int writeValue(const JFloatArray & val, std::ostream &os) {
		unsigned len = val.size() ;
		for (unsigned i=0;i<len;i++) {
			float n = val[i];
			swapFloat(&n);
			os.write((char*)&n , sizeof(float));
			if (os.fail()) return 0;
		}
		return 1;
	}
		
	inline int writeValue(const JDoubleArray & val, std::ostream &os) {
		unsigned len = val.size() ;
		for (unsigned i=0;i<len;i++) {
			double n = val[i];
			swapDouble(&n);
			os.write((char*)&n , sizeof(double));
			if (os.fail()) return 0;
		}
		return 1;
	}
	
	inline int writeValue(const JFloatVectorArray & val, std::ostream &os) {	
		unsigned len = val.size() ;
		for (unsigned i=0;i<len;i++) {
			const JFloatVector & f = val[i]; 
			for (unsigned  j = 0;j<3;j++) {
				float n = f(j);
				swapFloat(&n);
				os.write((char*)&n , sizeof(float));
				if (os.fail()) return 0;
			}
		}
		return 1;
	}
	inline int writeValue(const JVectorArray & val, std::ostream &os) {	
		unsigned len = val.size() ;
		for (unsigned i=0;i<len;i++) {
			const JVector & f = val[i]; 
			for (unsigned  j = 0;j<3;j++) {
				double n = f(j);
				swapDouble(&n);
				os.write((char*)&n , sizeof(double));
				if (os.fail()) return 0;
			}
		}
		return 1;
	}
	
	inline int writeValue(const JFloatMatrix & val, std::ostream &os) {
		
		for (unsigned i=0;i<4;i++) {
			for (unsigned  j = 0;j<4;j++) {
				float n = val[i][j];
				swapFloat(&n);
				os.write((char*)&n , sizeof(float));
				if (os.fail()) return 0;
			}
		}
		return 1;
	}
	
	inline int writeValue(const JFloatVector & val, std::ostream &os) {
		
		for (unsigned  j = 0;j<3;j++) {
			float n = val[j];
			swapFloat(&n);
			os.write((char*)&n , sizeof(float));
			if (os.fail()) return 0;
		}
		return 1;
	}
	
	inline int writeValue(float val, std::ostream &os) {
		swapFloat(&val);
		os.write((char*)&val , sizeof(float) );
		return os.fail()?0:1;
	}
	
	inline int writeValue(double  val, std::ostream &os) {
		swapDouble(&val);
		os.write((char*)&val , sizeof(double) );
		return os.fail()?0:1;
	}
	
	inline int writeValue(int  val, std::ostream &os) {
		swapInt(&val);
		os.write((char*)&val , sizeof(int) );
		return os.fail()?0:1;
	}
	
	inline int writeValue(unsigned  val, std::ostream &os) {
		swapInt((int*)&val);
		os.write((char*)&val , sizeof(unsigned) );
		return os.fail()?0:1;
	}
	
	///////////////////////////////////// [ W R I T E - A S C I I] /////////////////////////////////
	inline int writeValueA(const JFloatMatrixArray & val, std::ostream &os) {
		JFloatMatrixArray::const_iterator iter = val.begin();
		os << val.size() << " elements:" << "[ "; 
		while (iter != val.end()) {
			os << *iter;
			iter++;
			if (iter != val.end()) {
				os << "," << std::endl;
			}
		}
		os  <<  " ]" << std::endl;
		return 1;
	}
	
	
	inline int writeValueA(const JFloatVectorArray & val, std::ostream &os) {
		JFloatVectorArray::const_iterator iter = val.begin();
		os << val.size() << " elements:" << "[ " ; 
		unsigned count = 0;
		while (iter != val.end()) {
			os << *iter;
			iter++;
			if (iter != val.end()) {
				os << ", ";
			}
			if (count %4 ==3) os << std::endl;
			count++;
		}
		os << " ]" << std::endl;
		return 1;
	}
	
	inline int writeValueA(const JFloatArray & val, std::ostream &os) {
		JFloatArray::const_iterator iter = val.begin();
		os << val.size() << " elements:" << "[ " ; 
		unsigned count = 0;
		while (iter != val.end()) {
			os << *iter;
			iter++;
			if (iter != val.end()) {
				os << ", ";
			}
			if (count %16 ==15) os << std::endl;
			count++;
		}
		os  << " ]" << std::endl;
		return 1;
	}
	
	inline int writeValueA(const JIntArray & val, std::ostream &os) {
		JIntArray::const_iterator iter = val.begin();
		os << val.size() << " elements:" << "[ "; 
		unsigned count = 0;
		while (iter != val.end()) {
			os << *iter;
			iter++;
			if (iter != val.end()) {
				os << ", ";
			}
			if (count %24 ==23) os << std::endl;
			count++;
		}
		os << " ]" << std::endl;
		return 1;
	}
	
	///////////////////////////////////// [ R E A D ] /////////////////////////////////
	
	inline int readValue(std::string & val, std::istream &is) {
		unsigned len = val.size();
		is.read(&val[0],len);
		if (is.fail()) return 0;
		// std::cerr << "just read in" << val << std::endl;
		return 1; 
	}


	inline int readValue(JIntArray & val, std::istream &is) {
		unsigned len = val.size();
		is.read((char*)&val[0] , sizeof(int)*len ); 
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<len;i++)  
				swapInt( &val[i] );
		}
		return 1;
	}
	
	inline int readValue(JFloatArray & val, std::istream &is) {
		unsigned len = val.size();
		is.read((char*)&val[0] , sizeof(float)*len ); 
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<len;i++)  
				swapFloat( &val[i] );
		}
		return 1;
	}
	
	inline int readValue(JDoubleArray & val, std::istream &is) {
		unsigned len = val.size();
		is.read((char*)&val[0] , sizeof(double)*len ); 
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<len;i++)  
				swapDouble( &val[i] );
		}
		return 1;
	}
	

	inline int readValue(JFloatVectorArray & val, std::istream &is) {	
		
		unsigned len = val.size();
		is.read((char*)&val[0] , sizeof(float)*3*len ); 
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<len;i++) {
				JFloatVector & fv = val[i];
				for (unsigned j=0;j<3;j++) {
					swapFloat( &(fv(j)) );
				}
			}
		}
		return 1;
	}
		inline int readValue(JVectorArray & val, std::istream &is) {	
		
		unsigned len = val.size();
		is.read((char*)&val[0] , sizeof(double)*3*len ); 
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<len;i++) {
				JVector & fv = val[i];
				for (unsigned j=0;j<3;j++) {
					swapDouble( &(fv(j)) );
				}
			}
		}
		return 1;
	}
	
	inline int readValue(JFloatMatrix & val, std::istream &is) {
		
		is.read((char*)&val , sizeof(float)*16 );
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<4;i++) {
				for (unsigned j=0;j<4;j++) {
					swapFloat( &(val[i][j]) );
				}
			}
		}
		return 1;
	}
	
	inline int readValue(JFloatVector & val, std::istream &is) {
		is.read((char*)&val , sizeof(float)*3 );
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<3;i++) {	
				swapFloat( &(val(i)) );
			}
		}
		return 1;
	}
	
	inline int readValue(JVector & val, std::istream &is) {
		is.read((char*)&val , sizeof(double)*3 );
		if ( is.fail()) {
			return 0;
		} else {
			for (unsigned i=0;i<3;i++) {	
				swapDouble( &(val(i)) );
			}
		}
		return 1;
	}
	
	inline int readValue(float & val, std::istream &is) {
		is.read((char*)&val , sizeof(float) );
		if ( is.fail()) {
			return 0;
		} else {
			swapFloat( &val);
		}
		return 1;
	}
	
	inline int readValue(double & val, std::istream &is) {
		is.read((char*)&val , sizeof(double) );
		if ( is.fail()) {
			return 0;
		} else {
			swapDouble( &val);
		}
		return 1;
	}
	
	inline int readValue(int & val, std::istream &is) {
		is.read((char*)&val , sizeof(int) );
		if ( is.fail()) {
			return 0;
		} else {
			swapInt( &val);
		}
		return 1;
	}
	
	inline int readValue(unsigned & val, std::istream &is) {
		is.read((char*)&val , sizeof(unsigned) );
		if ( is.fail()) {
			return 0;
		} else {
			swapInt((int*)&val);
		}
		return 1;
	}
}



#endif

