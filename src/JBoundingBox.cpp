#include "JBoundingBox.h"

// const float bigNum = 10e+37f;

JBoundingBox::JBoundingBox(){
	clear();
}

///

JBoundingBox::JBoundingBox( const JBoundingBox & src ){

	minx = src.minX();
	maxx = src.maxX();
	miny = src.minY();
	maxy = src.maxY();
	minz = src.minZ();
	maxz = src.maxZ();

}

///
JBoundingBox::JBoundingBox( const JFloatVector &corner1, const JFloatVector &corner2 ){
	minx = corner1.x;
	maxx = corner2.x;
	miny = corner1.y;
	maxy = corner2.y;
	minz = corner1.z;
	maxz = corner2.z;
}


///
JBoundingBox::~JBoundingBox(){}
///
void		JBoundingBox::clear(){
	

	minx =	0.0f;
	maxx = 	0.0f;
	miny =  	0.0f;
	maxy = 	0.0f;
	minz =  	0.0f;
	maxz =  	0.0f;

}

///  
// void				JBoundingBox::transformUsing ( const JFloatMatrix &matrix );
///
void	JBoundingBox::expand( const JFloatVector & point ){
	if (point.x > maxx) maxx = point.x;
	if (point.x < minx) minx = point.x;

	if (point.y > maxy) maxy = point.y;
	if (point.y < miny) miny = point.y;

	if (point.z > maxz) maxz = point.z;
	if (point.z < minz) minz = point.z;
}

/// 
void	JBoundingBox::expand( const JBoundingBox & box ){
	expand(box.minCorner());
	expand(box.maxCorner());
}

/// 
bool	JBoundingBox::contains( const JFloatVector & point ) const {
	if (point.x < minx) return false;
	if (point.x > maxx) return false;
	if (point.y < miny) return false;
	if (point.y > maxy) return false;
	if (point.z < minz) return false;
	if (point.z > maxz) return false;
	return true;
}

float				JBoundingBox::width() const {
	return maxx - minx;
}
///
float				JBoundingBox::height() const {
	return maxy - miny;
}
///
float	JBoundingBox::depth() const {
	return maxz - minz;
}
///
JFloatVector		JBoundingBox::center() const{
	return JFloatVector( (maxx + minx) * 0.5f , (maxy + miny ) * 0.5f  , (maxz + minz) * 0.5f  );
}
///
JFloatVector		JBoundingBox::minCorner() const{
	return JFloatVector(minx, miny, minz);
}
///
JFloatVector		JBoundingBox::maxCorner() const{
	return JFloatVector(maxx, maxy, maxz);
}


///
JBoundingBox & 	JBoundingBox::operator=( const JBoundingBox & other ){

	minx = other.minX();

	maxx = other.maxX();

	miny = other.minY();

	maxy = other.maxY();

	minz = other.minZ();

	maxz = other.maxZ();

	return (*this);

}


