/*
 *  jTypes.h
 *  jtools
 *
 *  Created by Julian Mann on 22/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#ifndef jTypes_H
#define jTypes_H

#include <vector>
#include <string>
#include "JFloatVector.h"
#include "JVector.h"
#include "JFloatMatrix.h"

typedef std::vector<int> JIntArray;
typedef std::vector<float> JFloatArray;
typedef std::vector<double> JDoubleArray;
typedef std::vector<JFloatVector> JFloatVectorArray;
typedef std::vector<JVector> JVectorArray;
typedef std::vector<JFloatMatrix> JFloatMatrixArray;
typedef std::vector<std::string>  JStringArray;

class jTypes {
	
public:
	
	enum axis {xAxis, yAxis ,zAxis };

};

typedef jTypes::axis axis;


#endif 

