#include "JBoundingBox.h"
#include "jPdcAttr.h"
#include "jPdcFile.h"
#include "ioUtils.h"



/////////////////////////////////////////////////////////////////////////////
jPdcFile::jPdcFile ( const std::string& fn, int &st ):
m_numParticles(0), m_numAttr(0) 
{


	m_filename = fn; 
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);

	if (is.fail())  {
		st = 0;
		return;
	}


	std::string fType = "PDC ";
	const char* fTypeChar = fType.c_str();
	is.read((char*)fTypeChar , ( 4 *(sizeof(char))));
	if (std::string(fTypeChar) != fType) {st = 0;return;}

	int numAttr;


	st = ioUtil::readValue(m_version, is) ;if (! st) return;

	st = ioUtil::readValue(m_endian, is) ; if (! st) return;

	st = ioUtil::readValue(m_bitInfo1, is) ; if (! st) return;

	st = ioUtil::readValue(m_bitInfo2, is) ; if (! st) return;	

	st = ioUtil::readValue(m_numParticles, is) ;if (! st) return;

	st = ioUtil::readValue(numAttr, is) ; if (! st) return;



	//cerr << "jPdcFile - reading data points" << endl;
	for (int i=0; i<numAttr; i++) {


		int nameLength;
		st = ioUtil::readValue(nameLength, is) ; if  (! st)  return;
		std::string attrName("X");
		attrName.resize(nameLength);
		st = ioUtil::readValue(attrName, is) ; if  (! st)  return;
		if (attrName == "ghostFrames")  {
			st = 1;return;
		}


		int attrType;
		st = ioUtil::readValue(attrType, is) ;


		m_attrMap[attrName] = jPdcAttr(  attrName,  attrType,  int(is.tellg()) , i);
		if (i < (numAttr - 1)) {
			switch (attrType){
				case jPdcAttr::kIntAttr: 			is.seekg(sizeof(int) , std::ios::cur); 							break;
				case jPdcAttr::kIntArrayAttr:		is.seekg(sizeof(int) * m_numParticles , std::ios::cur);			break;
				case jPdcAttr::kDoubleAttr:			is.seekg(sizeof(double) , std::ios::cur); 							break;
				case jPdcAttr::kDoubleArrayAttr:		is.seekg(sizeof(double) * m_numParticles , std::ios::cur); 		break;
				case jPdcAttr::kVectorAttr:			is.seekg(sizeof(double) * 3 , std::ios::cur); 						break;
				case jPdcAttr::kVectorArrayAttr:		is.seekg(sizeof(double) * 3 * m_numParticles, std::ios::cur);		break;
			}
		}
	}
	is.close();
}


jPdcFile::~jPdcFile(){
}

void jPdcFile::getHeaderInfo(int& version, int& endian, int& bitinfo1, int& bitinfo2) const 
{
	version = m_version;
	endian = m_endian;
	bitinfo1 = m_bitInfo1;
	bitinfo2 = m_bitInfo2;
}




int jPdcFile::fileNumber(int *st) const {

size_t  start = 0, end = 0;
	JStringArray sa;
    while ( end != std::string::npos)
    {
        end = m_filename.find( ".", start);

        // If at end, use length=maxLength.  Else use length=end-start.
        sa.push_back( m_filename.substr( start,
                       (end == std::string::npos) ? std::string::npos : end - start));

        // If at end, use start=maxSize.  Else use start=end+delimiter.
        start = (   ( end > (std::string::npos - 1) ) ?  std::string::npos  :  end + 1);
    }

	unsigned sl = sa.size();
	if ( sl < 2) {
		*st=0;
		return 0;
	}
	return atoi((sa[1]).c_str());
}

int jPdcFile::particleCount() const {
	return m_numParticles;
}

int jPdcFile::attributeCount() const {
	return int(m_attrMap.size());
}

JStringArray jPdcFile::getNames() const {
	JStringArray names;
	jPdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter !=m_attrMap.end()) {
		if (iter->second.isValid()) names.push_back(iter->first);
		iter++;
	}
	return names;
}

JStringArray jPdcFile::getSortedNames() const {

	JStringArray names(m_attrMap.size());
	jPdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter !=m_attrMap.end()) {
		if (iter->second.isValid()) names[(iter->second.m_dataIndex)] = iter->first;
		iter++;
	}
	return names;
}

JStringArray jPdcFile::getTypes() const {
	JStringArray types;
	jPdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter !=m_attrMap.end()) {
		if (iter->second.isValid()) types.push_back(iter->second.m_attrTypeStr);
		iter++;
	}
	return types;
}


bool jPdcFile::attributeExists(const std::string& name) const {
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	return (iter != m_attrMap.end());
}

jPdcAttr::jPdcAttrType jPdcFile::typeFromName (const std::string& name) const {

	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) {
		return jPdcAttr::kInvalid;
	} 
	return iter->second.m_attrType;
}

std::string jPdcFile::typeStringFromName (const std::string& name) const {

	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) {
		return "not found";
	} 
	return iter->second.m_attrTypeStr;
}


/////////// GET DATA BY NAME /////////////////////////////////////
int jPdcFile::getData(int& data, const std::string& name)  const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kIntAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	int st = ioUtil::readValue(data, is) ;
	is.close();
	return st;
}

int jPdcFile::getData(JIntArray& data, const std::string& name) const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kIntArrayAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	data.resize(m_numParticles);
	int st = ioUtil::readValue(data, is) ;
	is.close();
	return st;
}

int jPdcFile::getData(double& data, const std::string& name)  const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kDoubleAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	int st = ioUtil::readValue(data, is) ;
	is.close();
	return st;
}

int jPdcFile::getData(JDoubleArray& data, const std::string& name)  const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kDoubleArrayAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	data.resize(m_numParticles);
	int st = ioUtil::readValue(data, is) ;
	is.close();
	return st;;
}

int jPdcFile::getData(JVector& data, const std::string& name) const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kVectorAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	int st = ioUtil::readValue(data, is);
	is.close();
	return st;
}

int jPdcFile::getData(JVectorArray& data, const std::string& name)  const {
	std::ifstream		is;
	is.open(m_filename.c_str(),  std::ios::in | std::ios::binary);
	jPdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) return 0;
	if ( iter->second.m_attrType != jPdcAttr::kVectorArrayAttr)  return 0;
	is.seekg( iter->second.m_dataPos , std::ios::beg);  
	if (is.fail()) return 0;
	data.resize(m_numParticles);
	int st = ioUtil::readValue(data, is) ;
	is.close();
	return st;
}

int jPdcFile::boundingBox(JBoundingBox &bb)  {
	JVectorArray points;
	


	if (!getData(points, std::string("position"))) return 0;

	unsigned plen =points.size() ;
	if (!plen) return 0;
	JFloatVector fp(float(points[0].x),float(points[0].y),float(points[0].z));

	bb =  JBoundingBox(fp,fp );

	for (unsigned i = 0;i< plen;i++) {
		fp = JFloatVector(float(points[i].x),float(points[i].y),float(points[i].z));
		bb.expand( fp );
	}

	return 1;
}
