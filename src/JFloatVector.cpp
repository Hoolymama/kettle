/*
 *  JFloatVector.cpp
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
#include <math.h>

#include "JFloatVector.h"

JFloatVector::JFloatVector(): x(0.0f), y(0.0f), z(0.0f) {}


JFloatVector::JFloatVector( const JFloatVector& rhs){
	x=rhs.x;
	y=rhs.y;
	z=rhs.z;
}

JFloatVector::JFloatVector( float xx, float yy, float zz ){
	x=xx;
	y=yy;
	z=zz;
}

JFloatVector::JFloatVector( const float rhs[3]   ){
	x=rhs[0];
	y=rhs[1];
	z=rhs[2];
}

JFloatVector::~JFloatVector() {}

JFloatVector& JFloatVector::operator= ( const JFloatVector& src )
{
	x=src.x;
	y=src.y;
	z=src.z;
	return (*this);
}

float& JFloatVector::operator()( unsigned i ) { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	return z;
}

float JFloatVector::operator()( unsigned i ) const { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

float& JFloatVector::operator[]( unsigned i ) { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

float	JFloatVector::operator[]( unsigned i )const { 
	if ( i==0)  return x;
	if ( i==1)  return y;
	 return z;
}

JFloatVector JFloatVector::operator^( const JFloatVector& rhs) const
{
	return JFloatVector( y * rhs.z - z * rhs.y   ,  z * rhs.x - x * rhs.z  , x * rhs.y - y * rhs.x   );
}

JFloatVector& JFloatVector::operator/=( float rhs ) {
	if (rhs != 0.0f)  {
		float recip = 1.0f / rhs;
		x =x*recip;
		y =y*recip;
		z =z*recip;
	}
	return (*this);
}

JFloatVector 	    JFloatVector::operator/( float rhs ) const {
	if (rhs == 0.0f) return JFloatVector(x,y,z);
	float recip = 1.0f / rhs;
	return JFloatVector(x*recip,y*recip,z*recip);
}

JFloatVector& JFloatVector::operator*=( float rhs ){
	x =x*rhs;
	y =y*rhs;
	z =z*rhs;
	return (*this);
}

JFloatVector JFloatVector::operator*( float rhs ) const {
	return JFloatVector(x*rhs,y*rhs,z*rhs);
}


JFloatVector  JFloatVector::operator*( const JFloatMatrix& m) const{

	return JFloatVector(
		x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] + m.matrix[3][0],
		x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] + m.matrix[3][1],
		x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] + m.matrix[3][2]
	);
}


JFloatVector& JFloatVector::operator*=( const JFloatMatrix& m ){

	float xx,yy,zz;
	
	xx = x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] + m.matrix[3][0];
	yy = x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] + m.matrix[3][1];
	zz = x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] + m.matrix[3][2];
	
	x = xx;
	y=  yy;
	z=  zz;
	
	return *this;
	
}

JFloatVector  JFloatVector::normalTransform( const JFloatMatrix& m) const{

	return JFloatVector(
		x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0] ,
		x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1] ,
		x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2] 
	);
}
	
	JFloatVector& JFloatVector::transformAsNormal( const JFloatMatrix& m ){

	float xx,yy,zz;
	
	xx = x*m.matrix[0][0] + y*m.matrix[1][0] + z*m.matrix[2][0];
	yy = x*m.matrix[0][1] + y*m.matrix[1][1] + z*m.matrix[2][1];
	zz = x*m.matrix[0][2] + y*m.matrix[1][2] + z*m.matrix[2][2];
	
	x = xx;
	y=  yy;
	z=  zz;
	
	return *this;
	
}
	
	
	
JFloatVector JFloatVector::operator+( const JFloatVector& rhs) const{
	return  JFloatVector(x+rhs.x,y+rhs.y,z+rhs.z);
}

JFloatVector& JFloatVector::operator+=( const JFloatVector& rhs ){
	x += rhs.x;
	y +=rhs.y;
	z +=rhs.z;
	return (*this);
}

JFloatVector JFloatVector::operator-() const{
	return  JFloatVector(-x,-y,-z);
}

JFloatVector JFloatVector::operator-( const JFloatVector& rhs ) const{
	return  JFloatVector(x - rhs.x ,y- rhs.y,z- rhs.z);
}

JFloatVector& JFloatVector::operator-=( const JFloatVector& rhs ){
	x -= rhs.x;
	y -=rhs.y;
	z -=rhs.z;
	return (*this);
}


float JFloatVector::operator*( const JFloatVector& rhs ) const{

	return (float)(x * rhs.x + y * rhs.y + z * rhs.z);
}

bool JFloatVector::operator!=( const JFloatVector& rhs ) const{
	if (x != rhs.x) return true;
	if (y != rhs.y) return true;
	if (z != rhs.z) return true;
	return false;
}

bool JFloatVector::operator==( const JFloatVector& rhs ) const{
	if (x != rhs.x) return false;
	if (y != rhs.y) return false;
	if (z != rhs.z) return false;
	return true;
}

float JFloatVector::length() const{
	return  (float)sqrt( x * x + y * y + z * z );
}

JFloatVector JFloatVector::normal() const{
	
	float mag = (float)sqrt( x * x + y * y + z * z );
	if (mag > 0) {
		float recip = 1.0f / mag;
		return JFloatVector( x * recip, y * recip,  z * recip );
	}
	return  JFloatVector(0.0,0.0,0.0);
}

void JFloatVector::normalize(){
	float mag = (float)sqrt( x * x + y * y + z * z );
	if (mag > 0) {
		float recip = 1.0f / mag;
		x *= recip;
		y *= recip;
		z *= recip; 
	}
}


float JFloatVector::angle( const JFloatVector& other ) const{
	return acos( this->normal()* other.normal() );
}

bool JFloatVector::isEquivalent( const JFloatVector& rhs,  float tolerance  )  const{
	if ( fabs(this->x - rhs.x) > tolerance ) return false;
	if ( fabs(this->y - rhs.y) > tolerance ) return false;
	if ( fabs(this->z - rhs.z) > tolerance ) return false;
	return true;
	
}

bool JFloatVector::isParallel( const JFloatVector& other, float tolerance  ) const{
	JFloatVector cross = (*this)^other;
	if (fabs(cross.x) >tolerance ) return false;
	if (fabs(cross.y) >tolerance ) return false;
	if (fabs(cross.z) >tolerance ) return false;
	return true;
}



const JFloatVector  JFloatVector::zero = JFloatVector(0.0f,0.0f,0.0f);
const JFloatVector  JFloatVector::one = JFloatVector(1.0f,1.0f,1.0f);
const JFloatVector  JFloatVector::xAxis = JFloatVector(1.0f,0.0f,0.0f);
const JFloatVector  JFloatVector::yAxis = JFloatVector(0.0f,1.0f,0.0f);
const JFloatVector  JFloatVector::zAxis = JFloatVector(0.0f,0.0f,1.0f);
const JFloatVector  JFloatVector::xNegAxis = JFloatVector(-1.0f,0.0f,0.0f);
const JFloatVector  JFloatVector::yNegAxis = JFloatVector(0.0f,-1.0f,0.0f);
const JFloatVector  JFloatVector::zNegAxis = JFloatVector(0.0f,0.0f,-1.0f);


