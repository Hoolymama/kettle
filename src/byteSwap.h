/*
 *  byteSwap.h
 *  jtools
 *
 *  Created by Julian Mann on 28/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#ifndef byteSwap_h
#define byteSwap_h


#if defined (__BIG_ENDIAN__)
inline int swapDouble(double* n) {
  return 1;
}

inline int swapInt(int * n) {
  return 1;
}


inline int swapFloat(float * n) {
  return 1;
}
#else
inline int swapDouble(double* n) {
   unsigned char* cptr, tmp;
   cptr = (unsigned char*) n;
   
   tmp = cptr[0];
   cptr[0] = cptr[7];
   cptr[7] = tmp;
   
   tmp = cptr[1];
   cptr[1] = cptr[6];
   cptr[6] = tmp;
   
   tmp = cptr[2];
   cptr[2] = cptr[5];
   cptr[5] =tmp;
   
   tmp = cptr[3];
   cptr[3] = cptr[4];
   cptr[4] = tmp;

return 1;

}

inline int swapInt(int * n) {
   unsigned char* cptr, tmp;
   cptr = (unsigned char*) n;
   
   tmp = cptr[0];
   cptr[0] = cptr[3];
   cptr[3] = tmp;
   
   tmp = cptr[1];
   cptr[1] = cptr[2];
   cptr[2] = tmp;
   
   return 1;

}

inline int swapFloat(float * n) {
   unsigned char* cptr, tmp;
   cptr = (unsigned char*) n;
   
   tmp = cptr[0];
   cptr[0] = cptr[3];
   cptr[3] = tmp;
   
   tmp = cptr[1];
   cptr[1] = cptr[2];
   cptr[2] = tmp;
   
   return 1;

}




#endif 
#endif 

/* if defined byteSwap */

