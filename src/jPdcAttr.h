/*
 *  pdcAttr.h
 *  jtools
 *
 *  Created by Julian Mann on 15/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
 #ifndef _jPdcAttr
#define _jPdcAttr

 
// #include <maya/MIOStream.h>
// #include <maya/MString.h>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>


class jPdcAttr {
	
	public:
	enum jPdcAttrType{
		kIntAttr, 
		kIntArrayAttr, 
		kDoubleAttr, 
		kDoubleArrayAttr, 
		kVectorAttr, 
		kVectorArrayAttr, 
		kInvalid };

	jPdcAttr() ;
	jPdcAttr(const std::string & name, int type, int pos, int i ) ;
	
	~jPdcAttr() ;


	friend std::ostream& operator<<(std::ostream &os, const jPdcAttr &attr)
	{
		os << attr.m_attrName ;
		if (attr.m_attrType == kIntAttr) { os << " is an int" ;}
		else if  (attr.m_attrType == kIntArrayAttr) { os << " is an array of ints" ;}
		else if  (attr.m_attrType == kDoubleAttr) { os << " is a double" ;}
		else if  (attr.m_attrType == kDoubleArrayAttr) { os << " is an array of doubles" ;}
		else if  (attr.m_attrType == kVectorAttr) { os << " is a vector" ;}
		else if  (attr.m_attrType == kVectorArrayAttr) { os << " is an array of vectors" ;}
		else if  (attr.m_attrType == kInvalid) { os << " is invalid" ;}

		return os ;
	}

	bool isValid() const { return (m_attrType != kInvalid);}
	
	std::string m_attrName;
	std::string m_attrTypeStr;
	jPdcAttrType m_attrType;
	int m_dataPos;
	int m_dataIndex;


private:


	
};

#endif

typedef std::vector<jPdcAttr> jPdcAttrArray;

