/*
 *  lineBoxKD.h
 *  jtools
 *
 *  Created by Julian Mann on 9/17/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _lineBoxKD
#define _lineBoxKD

#include <math.h>
#include "jTypes.h"
#include "JFloatVector.h"

#include "lineBox.h"



class     lineBox ;

typedef vector<lineBox*> LINE_LIST;


struct kdNode  {
	bool bucket;							/// is this node a bucket
	axis cutAxis;							/// if not, this axis will divide it 
	float cutVal;							/// at this value
	kdNode *loChild, *hiChild;				/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	LINE_LIST * overlapList;			/// list of lineBoxs whose boumding boxes intersect this bucket
	
	
};


class lineBoxKD 
	{
	public:
		
		lineBoxKD();			
		
		~lineBoxKD();  
		
		const kdNode * root();
		
		lineBoxKD& operator=(const lineBoxKD & otherData );
		
		void 		build(); 							/// build root from m_perm
		
		kdNode * 	build( int low,  int high ); 	/// build children recursively
		
		int size() ;

		void  	setMaxPointsPerBucket( int b) {if (b < 1) {m_maxPointsPerBucket = 1;} else {m_maxPointsPerBucket = b;}}
		
		bool 	init();
		
		bool 	populate(const JFloatVectorArray &vertices, const JFloatArray &uCoords) ;
		
		void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;
		
		void makeEmpty() ; 
		
		void makeEmpty(kdNode * p) ; 
		
		void  setOverlapList(kdNode * p,  lineBox * b  );
		
		LINE_LIST & lineList();
		
		void closestLine(const kdNode * p,const JFloatVector &searchPoint,  float & radius,  lineBox &  result )  const ;
		
	private:	
		
		axis  findMaxAxis(const  int low,const  int  high) const;
		
		void  searchList(const LINE_LIST * overlapList,const JFloatVector &searchPoint, float & radius, lineBox & result) const   ;
		
		LINE_LIST * m_perm;				/// pointer to (permutations) - a list of pointers to elements of _lineBoxVector
		
		kdNode * m_pRoot;							/// pointer to root of tree
		
		int m_maxPointsPerBucket;				/// at build time keep recursing until no bucket holds more than this
		
	} ;

#endif


