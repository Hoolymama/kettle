/*
 *  lineBoxKD.cpp
 *  jtools
 *
 *  Created by Julian Mann on 9/17/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#include "lineBoxKD.h"


#define P_TOLERANCE 0.000001

#include "lineBoxKD.h"

lineBoxKD::lineBoxKD() //  constructor 
:m_perm(0),
m_pRoot(0),
m_maxPointsPerBucket(4)
{
	m_pRoot = 0;
	m_perm = new LINE_LIST;
	
}

lineBoxKD::~lineBoxKD()
{
	if (m_perm) {
		LINE_LIST::iterator p =  m_perm->begin();
		while (p != m_perm->end()) {
			delete *p;
			*p =0;
			p++;
		}
		delete m_perm;
		m_perm = 0;
	}
	
	makeEmpty(m_pRoot);  // recursively delete tree 
}

void lineBoxKD::makeEmpty(kdNode * p) {
	if (p) { // if the pointer aint NULL
		if (!(p->bucket)) {
			makeEmpty(p->loChild);	// recurse through kids
			makeEmpty(p->hiChild);
		}  else {
			delete p->overlapList;
			p->overlapList  = 0;
		}
		delete p;
		p = 0;					// zap the little bugger
	}
}

const kdNode * lineBoxKD::root(){
	return m_pRoot;
};

//populate the tree with lineBox objects
bool lineBoxKD::populate(const JFloatVectorArray &vertices, const JFloatArray &uCoords)  {

	
	unsigned int len = vertices.size() ;	
	if (len < 2) return false;
	len--;
	
	//double end=0.0;
	//double start;
	
	for (unsigned i = 0; i < len; i++) {
		unsigned int j = i+1;
		lineBox * t = new lineBox(vertices[i],vertices[j],uCoords[i] ,uCoords[j]);
		m_perm->push_back(t) ;
	}
	return 1;
}

int lineBoxKD::size() {
	return m_perm->size();
}

void lineBoxKD::build(){
	int low = 0;
	int high = (size() -1);
	m_pRoot = build(low, high);
	
  	
	LINE_LIST::iterator currentBox = m_perm->begin();
	while(currentBox != m_perm->end()){
		setOverlapList(m_pRoot , *currentBox);
		currentBox++;
	}
}


kdNode *  lineBoxKD::build(	 int low,  int high	){
	// // cout << "in subBuild routine " << endl;
	kdNode * p = new kdNode;
	
 	if (((high - low) + 1) <= m_maxPointsPerBucket) {
		// only bucket nodes will hold an overlapList
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
		p->overlapList = new LINE_LIST;
	} else {
		
		p->bucket = false;
		p->cutAxis = findMaxAxis(low, high);
		int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		p->cutVal = ((*m_perm)[mid])->center(p->cutAxis);
		p->loChild = build(low, mid);
		p->hiChild = build(mid+1, high);
	}
	return p;
}


void lineBoxKD::wirthSelect(  int left,  int right,  int k, axis cutAxis )  
{
	int n = (right - left) + 1;
	if (n <= 1) return;
	register int i,j,l,m;
	lineBox *x;
	lineBox *tmp;
	
	l=left;
	m=right;
	while (l<m) {
		x = (*m_perm)[k];
		i=l;
		j=m;
		do {
			while (  ((*m_perm)[i])->center(cutAxis) <  x->center(cutAxis)  ) i++;
			while (  ((*m_perm)[j])->center(cutAxis) >  x->center(cutAxis)  ) j--;
			
			if (i<=j) {
				// swap
				tmp = (*m_perm)[i];  
				(*m_perm)[i] =(*m_perm)[j] ;  
				(*m_perm)[j] =tmp ;
				i++; j--;
			}
		} while(i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
}

axis lineBoxKD::findMaxAxis(const  int low, const  int high) const {
	
	
	// The idea here is just to find the axis containing the longest 
	// side of the bounding rectangle of the points
	
	// From a vector of N points we just take sqrtN samples
	// in order to keep the time down to O(N)
	// should be ok though
	float minx , miny,  minz , maxx , maxy , maxz, tmpVal;
	float sx, sy, sz;
	
	int  num = (high - low ) +1;
	int interval = int(sqrt(float(num)));
	// unsigned int intervalSq = interval*interval;
	int i;
	JFloatVector p = ((*m_perm)[low])->center();
	minx = p.x;
	maxx = minx;
	miny = p.y;
	maxy = miny;
	minz = p.z;
	maxz = minz;
	
	for (i= (low + interval); i<=high;i+=interval ) {
		p = ((*m_perm)[i])->center();
		tmpVal= p.x;
		if (tmpVal < minx) {
			minx = tmpVal;
		} else {
			if (tmpVal > maxx) {
				maxx = tmpVal;
			}
		}
		tmpVal= p.y;
		if (tmpVal < miny) {
			miny = tmpVal;
		} else {
			if (tmpVal > maxy) {
				maxy = tmpVal;
			}
		}
		tmpVal= p.z;
		if (tmpVal < minz) {
			minz = tmpVal;
		} else {
			if (tmpVal > maxz) {
				maxz = tmpVal;
			}
		}
	}
	sx = maxx - minx;
	sy = maxy - miny;
	sz = maxz - minz;
	
	if (sx > sy) {
		// y is not the greatest
		if (sx > sz) {
			return jTypes::xAxis;
		} else {
			return jTypes::zAxis;
		}
	} else {
		// x is not the greatest
		if (sy > sz) {
			return jTypes::yAxis;
		} else {
			return jTypes::zAxis;
		}
	}
}

void  lineBoxKD::setOverlapList(kdNode * p,  lineBox * b  )  {
	// recursive setOverlapList method
	// put lineBox in every bucket it overlaps
	if (p->bucket) {
		p->overlapList->push_back(b);
	} else {
		if (b->min(p->cutAxis) < p->cutVal) {
			setOverlapList(p->loChild, b);
		}
		if (b->max(p->cutAxis) > p->cutVal) {
			setOverlapList(p->hiChild, b);
		}
	}
}


void  lineBoxKD::searchList(
							 const LINE_LIST * overlapList,
							 const JFloatVector &searchPoint,
							 float & radius,
							 lineBox & result
							 ) const {
	
	LINE_LIST::const_iterator curr;
	curr = overlapList->begin();
	
	while(curr != overlapList->end()){
		if ((*curr)->sphereIntersectsBB(searchPoint, radius)  ) {
			// if the box is within our search radius, then the lineangle might also be
			float dist = 0;
			
			if ( (*curr)->sphereIntersectsLine(searchPoint, radius, dist)) {
				// cerr << "line is within " << radius << " -  actual dist to line is " << dist << endl;
				// if the line is within the radius then it is currently the closest line
				// because the radius has been shrinking
				// NOTE - if successful, the lineBox object will have cached the bary coords and the hit point
				// so if this lineBox is the final result - i.e. the closest, then the calling function
				// can just pick up the cached closest point rather than recalculate.
				// It can also ask the line for th parameter
				
				// Now shrink the radius and set the result
				radius	=  dist;
				result =   **curr;
			 	// cerr << "updating result   - "  << endl;
			}  else {
    			// cerr << "line is not within   - " << radius << endl;
			}
		} else {
			//  cerr << "box is not within " << radius  << endl;
		}
		curr++;
	}
}

// recursive function to find the closest line
void lineBoxKD::closestLine(
							 const kdNode * p,
							 const JFloatVector &searchPoint,
							 float & radius,  // radius is passed as reference but is not const, this is how we shrink it during the search.
							 lineBox & result )  const  {
	
	if (p->bucket) {
		searchList( p->overlapList, searchPoint, radius, result);
	} else {
		float diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closestLine(p->loChild,searchPoint,radius,result);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				closestLine(p->hiChild,searchPoint,radius,result);
			}
		} else { // we are in the hi child so search it
			closestLine(p->hiChild,searchPoint,radius,result);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				closestLine(p->loChild,searchPoint,radius,result);
			}
		}
	}
}

LINE_LIST & lineBoxKD::lineList(){
	return *m_perm;
}

