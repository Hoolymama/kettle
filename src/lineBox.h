/*
 *  lineBox.h
 *  jtools
 *
 *  Created by Julian Mann on 9/16/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef lineBox_H
#define lineBox_H

#include <iostream>
#include <math.h>
#include "jTypes.h"

#include "JBoundingBox.h"

#include "JFloatVector.h"

// #include <vector>

// #include "errorMacros.h"
// #include "mayaMath.h"

using namespace std;

class lineBox{
public:
	lineBox();
	lineBox( 
			const JFloatVector &start, 
			const JFloatVector &end,
			float uStart, 
			float uEnd);
	
	~lineBox();
	
	// lineBox& operator=(const lineBox& other);
	
	//enum axis {xAxis, yAxis ,zAxis };
	
	const JFloatVector & center() const;
	float center(axis a) const ;
	float min(axis a) const ;
	float max(axis a) const ;
	int id() const;
	
	bool sphereIntersectsBB(const JFloatVector &c,   float r) const  ;
	bool sphereIntersectsLine(const JFloatVector &c,  float r, float &dist) ;
	
	const JBoundingBox & box() const ;
	const JFloatVector & start() const ;
	const JFloatVector & end() const ;

   	const float & uStart() const ;
   	const float & uEnd() const ;
	const JFloatVector & cachedPoint() const;
	const JFloatVector & tangent() const;
	const float & cachedParam() const;
	const float & cachedDist() const;
	const float & length() const;
	float calcU() const ; // calculates the U value at the cached point
private:
	
 	void computeBoundingBox();
	JFloatVector m_start; // start and end points
	JFloatVector m_end; // start and end points
	
	JBoundingBox m_box; 
	JFloatVector m_center;
	
	float m_uStart; // uCoords of start and end
	float m_uEnd; // uCoords of start and end
	float m_cachedParam;
	float m_cachedDist;
	JFloatVector m_cachedPt;
	JFloatVector m_tangent;
	float m_length;
};

#endif

