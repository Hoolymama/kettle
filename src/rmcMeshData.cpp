#include <sstream>
#include "rmcMeshData.h"

rmcMeshData::rmcMeshData(const char* filename)
{
	m_valid=false;
	if (! rmcMeshData::isRMCB(filename)) {return;}
	std::ifstream is( filename,  std::ios::in | std::ios::binary); if (is.fail()) return;
	is.seekg(sizeof(char)*4, std::ios::beg); 	// put the pointer past the "RMBC"


	if ( !ioUtil::readValue(m_num_polys, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_num_vertices, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_num_face_vertices, is)) {is.close();return;}

	m_bound.resize(2);
	m_loops.resize(m_num_polys);
	m_faceVertexCount.resize(m_num_polys);
	m_faceVertexIds.resize(m_num_face_vertices);
	m_vertices.resize(m_num_vertices);
	m_normals.resize(m_num_vertices);
	m_coord_s.resize(m_num_face_vertices);
	m_coord_t.resize(m_num_face_vertices);
	
	if ( !ioUtil::readValue(m_bound, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_loops, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_faceVertexCount, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_faceVertexIds, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_vertices, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_normals, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_coord_s, is)) {is.close();return;}
	if ( !ioUtil::readValue(m_coord_t, is)) {is.close();return;}
	is.close();
	m_valid=true;
}

rmcMeshData::~rmcMeshData() {}


int rmcMeshData::isRMCB (const char * filename) {
	int result = 0;
	std::ifstream is( filename,  std::ios::in | std::ios::binary); if (is.fail()) {return 0;}
	char ftype[5];
	is.getline(ftype,5);
	if( ! strcmp(ftype, "RMCB")  ) {
			 result = 1;
	}
	// std::cerr << "type is  RMCB" <<  std::endl;
	is.close();
	return result;
}

int rmcMeshData::isRMCA (const char * filename) {
	int result = 0;
	std::ifstream is( filename,  std::ios::in); if (is.fail()) {return 0;}
	char ftype[5];
	is.getline(ftype,5);
	if( ! strcmp(ftype, "RMCA")  ) {
			 result = 1;
	}
	// std::cerr << "type is  RMCA" <<  std::endl;
	is.close();
	return result;
}

int rmcMeshData::readHeader( 
					   const char * filename,
					   unsigned & num_polys,
					   unsigned & num_vertices,
					   unsigned & num_face_vertices,
					   JFloatVectorArray & bound
					   ) {

	if (! rmcMeshData::isRMCB(filename)) {return 0;}
	std::ifstream is( filename,  std::ios::in | std::ios::binary); if (is.fail()) {return 0;}
	is.seekg(sizeof(char)*4, std::ios::beg); 	// put the pointer past the "RMBC"

	if ( !ioUtil::readValue(num_polys, is)) {is.close();return 0;}
	if ( !ioUtil::readValue(num_vertices, is)) {is.close();return 0;}
	if ( !ioUtil::readValue(num_face_vertices, is)) {is.close();return 0;}
	//std::cerr << "rmcMeshData::readHeader - read numbers " <<  std::endl;

	bound.resize(2);
	//std::cerr << "rmcMeshData bound is " << bound[0]  << " " << bound[1] << std::endl;

	if ( !ioUtil::readValue(bound, is)) {is.close();return 0;}
	is.close();
	return 1;
}
	
int rmcMeshData::getBoundingBox(  const char * filename, JBoundingBox &bb ){
	if (! rmcMeshData::isRMCB(filename)) {return 0;}
	std::ifstream is( filename,  std::ios::in | std::ios::binary); if (is.fail()) {return 0;}
	is.seekg( (sizeof(char)*4) +(sizeof(int) * 3) , std::ios::beg); 	// put the pointer to the BB"

	JFloatVector c1, c2;
	if ( !ioUtil::readValue(c1, is)) {is.close();return 0;}
	if ( !ioUtil::readValue(c2, is)) {is.close();return 0;}
	bb = JBoundingBox(c1,c2);

	return 1;
}

int rmcMeshData::writeB( 
		const char * filename,
		unsigned num_polys,
		unsigned num_vertices,
		unsigned num_face_vertices,
		const JFloatVectorArray & bound,
		const JIntArray & numLoops, 
		const JIntArray & faceVertexCount, 
		const JIntArray & faceVertexIds,
		const JFloatVectorArray & vertices,
		const JFloatVectorArray & normals,
		const JFloatArray & coord_s,
		const JFloatArray & coord_t
		)
{
	
	std::ofstream os(filename, std::ios::out | std::ios::binary);	
	if (os.fail())  return 0;
	
	// file type
	const char * ftype = "RMCB"; // renderman mesh cache binary
	os.write(ftype , sizeof(char)*4 );
	if (os.fail()) return 0;

	if (  !ioUtil::writeValue(num_polys, os)) return 0;
	if (  !ioUtil::writeValue(num_vertices, os)) return 0;
	if (  !ioUtil::writeValue(num_face_vertices, os)) return 0;

	if (  !ioUtil::writeValue(bound, os)) return 0;
	if (  !ioUtil::writeValue(numLoops, os)) return 0;
	if (  !ioUtil::writeValue(faceVertexCount, os)) return 0;
	if (  !ioUtil::writeValue(faceVertexIds, os)) return 0;

	if (  !ioUtil::writeValue(vertices, os)) return 0;
	if (  !ioUtil::writeValue(normals, os)) return 0;
	if (  !ioUtil::writeValue(coord_s, os)) return 0;
	if (  !ioUtil::writeValue(coord_t, os)) return 0;
	
	os.close();
	
	return 1;
}


int rmcMeshData::writeA( 
						 const char * filename,
						 unsigned num_polys,
						 unsigned num_vertices,
						 unsigned num_face_vertices,
						const JFloatVectorArray & bound,
						 const JIntArray & numLoops, 
						 const JIntArray & faceVertexCount, 
						 const JIntArray & faceVertexIds,
						 const JFloatVectorArray & vertices,
						 const JFloatVectorArray & normals,
						 const JFloatArray & coord_s,
						 const JFloatArray & coord_t
						 )
{
	
	std::ofstream os(filename, std::ios::out );	
	if (os.fail())  return 0;
	
	// file type
	os << "RMCA" << std::endl;
	os << "polygons: " << num_polys << std::endl;
	os << "vertices: " << num_vertices << std::endl;
	os << "face vertices: " << num_face_vertices << std::endl<< std::endl;

	os << "Bound: " << std::endl;
	ioUtil::writeValueA(bound, os);	os << std::endl;
	os << "Loops: " << std::endl;
	ioUtil::writeValueA(numLoops, os); os << std::endl;
	os << "Face Vertex Count: " << std::endl;
	ioUtil::writeValueA(faceVertexCount, os); os << std::endl;
	os << "Face Vertex Ids: " << std::endl;
	ioUtil::writeValueA(faceVertexIds, os); os << std::endl;
	
	os << "Vertices: " << std::endl;
	ioUtil::writeValueA(vertices, os); os << std::endl;
	os << "Normals: " << std::endl;
	ioUtil::writeValueA(normals, os); os << std::endl;
	os << "S Coordinates: " << std::endl;
	ioUtil::writeValueA(coord_s, os); os << std::endl;
	os << "T Coordinates: " << std::endl;
	ioUtil::writeValueA(coord_t, os); os << std::endl;
	os << "End of File: " << filename << std::endl;
	os.close();
	
	return 1;
}




