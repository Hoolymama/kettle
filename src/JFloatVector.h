/*
 *  JFloatVector.h
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
 #ifndef _JFloatVector
#define _JFloatVector

 
#define JFloatVector_kTol 1.0e-5F

#include <vector>
#include <fstream>

#include "JFloatMatrix.h"

class JFloatMatrix;

class  JFloatVector  
{
public:
	
	JFloatVector();

	JFloatVector( const JFloatVector&);

	JFloatVector( float xx, float yy, float zz = 0.0);

	JFloatVector( const float[3] );

	~JFloatVector();
	
 	JFloatVector&		operator= ( const JFloatVector& src );
	
 	float&     		 	operator()( unsigned i );
	
 	float				operator()( unsigned i ) const;
	
 	float&     		 	operator[]( unsigned i );
	
	float				operator[]( unsigned i )const;
	
 	JFloatVector			operator^( const JFloatVector& rhs) const;
	
 	JFloatVector&		operator/=( float scalar );
	
 	JFloatVector			operator/( float scalar ) const;
	
 	JFloatVector& 		operator*=( float scalar );
	
 	JFloatVector   		operator*( float scalar ) const;

 	JFloatVector   		operator+( const JFloatVector& other) const;
	
	JFloatVector&		operator+=( const JFloatVector& other );
	
 	JFloatVector   		operator-() const;
	
 	JFloatVector   		operator-( const JFloatVector& other ) const;

	JFloatVector&		operator-=( const JFloatVector& rhs );
 	
	JFloatVector  		operator*( const JFloatMatrix&) const;
	
 	JFloatVector&		operator*=( const JFloatMatrix&);

	JFloatVector  normalTransform( const JFloatMatrix& m) const;

	JFloatVector& transformAsNormal( const JFloatMatrix& m );

 	float				operator*( const JFloatVector& other ) const;
	
 	bool       		   	operator!=( const JFloatVector& other ) const;
	
 	bool				operator==( const JFloatVector& other ) const;
	
 	float      		   	length() const;
	
 	JFloatVector  		normal() const;
	
	void				normalize();
	
 	float      		 	angle( const JFloatVector& other ) const;
	
	bool				isEquivalent( const JFloatVector& other,  float tolerance = JFloatVector_kTol )  const;
	
 	bool       		   	isParallel( const JFloatVector& other, float tolerance = JFloatVector_kTol ) const;


/*
	future implementations

 	 JFloatVector		operator*( const JFloatMatrix&,   const JFloatVector& );

	 MStatus			get( float[3] ) const;
	
	 JFloatVector		transformAsNormal( const JFloatMatrix & matrix ) const;

	 (ostream)&		operator<<( (ostream)& os,	const JFloatVector& v );
 
*/ 
	
	friend std::ostream& operator<<(std::ostream &os, const JFloatVector &v) {
		os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
		return os ;
	}
 


	static const JFloatVector zero;

	static const JFloatVector one;

	static const JFloatVector xAxis;

	static const JFloatVector yAxis;

	static const JFloatVector zAxis;

	static const JFloatVector xNegAxis;

	static const JFloatVector yNegAxis;

	static const JFloatVector zNegAxis;

	float x;

	float y;

	float z;

protected:


private:


};




#endif /* _JFloatVector */

