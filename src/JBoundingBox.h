/*
 *  JBoundingBox.h
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#ifndef _JBoundingBox
#define _JBoundingBox

//-


#include "JFloatMatrix.h"
#include "JFloatVector.h"

class JBoundingBox  
{
public:
	///
    JBoundingBox();  
	///
	JBoundingBox( const JBoundingBox & src );  
	///
	JBoundingBox( const JFloatVector &corner1, const JFloatVector &corner2 );

	///
	~JBoundingBox();
	///
	void	clear();
	///  
    // void	transformUsing ( const JFloatMatrix &matrix );
	///
	void	expand( const JFloatVector & point );
	/// 
	void	expand( const JBoundingBox & box );
	
	/// 
	bool	contains( const JFloatVector & point ) const;
	///
	float	width() const;
	///
	float	height() const;
	///
	float	depth() const;
	///
	JFloatVector	center() const;
	///
	JFloatVector	minCorner() const;
	///
	JFloatVector	maxCorner() const;
	///
	JBoundingBox & operator=( const JBoundingBox & other );

	const float & minX() const { return minx;} ;

	const float & maxX() const { return maxx;};

	const float & minY() const { return miny;};

	const float & maxY() const { return maxy;};

	const float & minZ() const { return minz;};

	const float & maxZ() const { return maxz;};

private: 

	float minx, maxx, miny, maxy, minz, maxz;

};


#endif /* _JBoundingBox_ */
