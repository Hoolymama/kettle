/*
 *  lineBox.cpp
 *  jtools
 *
 *  Created by Julian Mann on 9/16/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#include "lineBox.h"


const float EPSILON =1e-12;
//const float NEG_EPSILON = -0.0001;

lineBox::lineBox(){}

lineBox::lineBox(
				 const JFloatVector &start, 
				 const JFloatVector &end,
				 float uStart, 
				 float uEnd
):
m_start(start),
m_end(end),
m_uStart(uStart), 
m_uEnd(uEnd)
{	
	JFloatVector diff = m_end - m_start;

	m_tangent = diff.normal();
	m_length = m_tangent.length();
	computeBoundingBox();
}

lineBox::~lineBox(){}


void lineBox::computeBoundingBox(){
	m_box = JBoundingBox(m_start,m_end);
	m_center = m_box.center();
	if (
		(m_box.width() < EPSILON) ||
		(m_box.height() < EPSILON) ||
		(m_box.depth() < EPSILON) 
		){
		m_box.expand(m_box.maxCorner() + JFloatVector(EPSILON,EPSILON,EPSILON));
		m_box.expand(m_box.minCorner() - JFloatVector(EPSILON,EPSILON,EPSILON));
	}	
}

const JFloatVector & lineBox::center() const {return m_center;}

float  lineBox::center(axis a) const {return  m_center[a];}

float  lineBox::min(axis a) const {return m_box.minCorner()[a];}

float  lineBox::max(axis a) const {return m_box.maxCorner()[a];}

const JFloatVector & lineBox::start() const {return m_start;}

const JFloatVector & lineBox::end() const {return m_end;}

const JBoundingBox & lineBox::box() const {return m_box;}

const float & lineBox::uStart() const {return m_uStart;}

const float & lineBox::uEnd() const {return m_uEnd;}

// determine if the given sphere intersects this bounding box
bool lineBox::sphereIntersectsBB(const JFloatVector &c, float r) const  {
	// params are center and radius
	float s, d = 0;
	
	if (c.x < m_box.minCorner().x){ s = c.x - m_box.minCorner().x;d += s*s;}
	else if (c.x > m_box.maxCorner().x) {s = c.x - m_box.maxCorner().x; d += s*s;}
	
	if (c.y < m_box.minCorner().y) { s = c.y - m_box.minCorner().y;d += s*s; }
	else if (c.y > m_box.maxCorner().y) {s = c.y - m_box.maxCorner().y; d += s*s;}
	
	if (c.z < m_box.minCorner().z) { s = c.z - m_box.minCorner().z;d += s*s; }
	else if (c.z > m_box.maxCorner().z){ s = c.z - m_box.maxCorner().z; d += s*s; }
	
	return d <= r*r;
}

// determine if the given sphere intersects the line.
// If so, store the point and coords of the closest point to the sphere center
// and send back the distance in dist

bool lineBox::sphereIntersectsLine(const JFloatVector &c, float r, float &dist) {
	float param;
	
	
	JFloatVector ab = m_end-m_start;	
	param = ((c-m_start)*ab) / (ab*ab);
	if (param<0) param=0;
	if (param>1) param=1;

	
	JFloatVector pt  = m_start + (ab * param );
	
	JFloatVector v = pt - c;
	float sqDist = (v*v);
	if ((sqDist) <= (r*r)) {  // bingo
		dist = sqrt(sqDist) ;
    	m_cachedParam = param;
    	m_cachedDist = dist;
		m_cachedPt = pt;
		return true;
	}
	return false;
}





const JFloatVector & lineBox::cachedPoint() const  {
	return m_cachedPt;
}
const JFloatVector & lineBox::tangent() const{
	return m_tangent;
}
const float & lineBox::length() const{
	return m_length;
}
const float & lineBox::cachedParam() const  {
	return m_cachedParam;
}
const float & lineBox::cachedDist() const  {
	return m_cachedDist;
}
float lineBox::calcU() const {
	return (m_uStart + (m_cachedParam * (m_uEnd - m_uStart)));
}



/*
lineBox& lineBox::operator=(const lineBox& other) {
	if (this != &other) {
		m_p = other.m_p;
		m_v = other.m_v;
		m_box = other.m_box;
		m_uCoords = other.m_uCoords;
		m_cachedParam = other.m_cachedParam;
		m_cachedDist = other.m_cachedDist;
		m_cachedPt = other.m_cachedPt;
		m_id = other.m_id;
		m_length = other.m_length;
		m_tangent = other.m_tangent; 
	}
	return *this;
}
*/
