/*
 *  JVector.h
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
 #ifndef _JVector
#define _JVector

 
#define JVector_kTol 1.0e-5F

#include <vector>
#include <fstream>


// maybe do matrix stuff later
// #include "JFloatMatrix.h"

// class JFloatMatrix;

class  JVector  
{
public:
	
	JVector();

	JVector( const JVector&);

	JVector( double xx, double yy, double zz = 0.0);

	JVector( const double[3] );

	~JVector();
	
 	JVector&		operator= ( const JVector& src );
	
 	double&     		 	operator()( unsigned i );
	
 	double				operator()( unsigned i ) const;
	
 	double&     		 	operator[]( unsigned i );
	
	double				operator[]( unsigned i )const;
	
 	JVector			operator^( const JVector& rhs) const;
	
 	JVector&		operator/=( double scalar );
	
 	JVector			operator/( double scalar ) const;
	
 	JVector& 		operator*=( double scalar );
	
 	JVector   		operator*( double scalar ) const;

 	JVector   		operator+( const JVector& other) const;
	
	JVector&		operator+=( const JVector& other );
	
 	JVector   		operator-() const;
	
 	JVector   		operator-( const JVector& other ) const;

	JVector&		operator-=( const JVector& rhs );
 	
	// JVector  		operator*( const JFloatMatrix&) const;
	
 // 	JVector&		operator*=( const JFloatMatrix&);

	// JVector  normalTransform( const JFloatMatrix& m) const;

	// JVector& transformAsNormal( const JFloatMatrix& m );

 	double				operator*( const JVector& other ) const;
	
 	bool       		   	operator!=( const JVector& other ) const;
	
 	bool				operator==( const JVector& other ) const;
	
 	double      		   	length() const;
	
 	JVector  		normal() const;
	
	void				normalize();
	
 	double      		 	angle( const JVector& other ) const;
	
	bool				isEquivalent( const JVector& other,  double tolerance = JVector_kTol )  const;
	
 	bool       		   	isParallel( const JVector& other, double tolerance = JVector_kTol ) const;


/*
	future implementations

 	 JVector		operator*( const JFloatMatrix&,   const JVector& );

	 MStatus			get( double[3] ) const;
	
	 JVector		transformAsNormal( const JFloatMatrix & matrix ) const;

	 (ostream)&		operator<<( (ostream)& os,	const JVector& v );
 
*/ 
	
	friend std::ostream& operator<<(std::ostream &os, const JVector &v) {
		os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
		return os ;
	}
 


	static const JVector zero;

	static const JVector one;

	static const JVector xAxis;

	static const JVector yAxis;

	static const JVector zAxis;

	static const JVector xNegAxis;

	static const JVector yNegAxis;

	static const JVector zNegAxis;

	double x;

	double y;

	double z;

protected:


private:


};




#endif /* _JVector */

