/*
 *  jPdcAttr.cpp
 *  jtools
 *
 *  Created by Julian Mann on 15/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#include "jPdcAttr.h"



 jPdcAttr::jPdcAttr()
 :m_attrName(""),
 m_attrTypeStr("invalid"),
 m_attrType(jPdcAttr::kInvalid),
 m_dataPos(0)
 {
 }

 jPdcAttr::jPdcAttr(const std::string& name, int type, int pos , int i)
 :m_attrName(name), m_dataPos(pos), m_dataIndex(i)
 {
 	if (( type >= 0 ) && ( type <= 5)) {
 		m_attrType = (jPdcAttrType(type));
 	} else {
 		m_attrType = jPdcAttr::kInvalid;
 	}
 	
 	
 	enum jPdcAttrType{kIntAttr, kIntArrayAttr, kDoubleAttr, kDoubleArrayAttr, kVectorAttr, kVectorArrayAttr, kInvalid };
 	if ( m_attrType == jPdcAttr::kIntAttr) m_attrTypeStr = "int";
 	else if ( m_attrType == jPdcAttr::kIntArrayAttr) m_attrTypeStr = "intArray";
 	else if ( m_attrType == jPdcAttr::kDoubleAttr) m_attrTypeStr = "double";
 	else if ( m_attrType == jPdcAttr::kDoubleArrayAttr) m_attrTypeStr = "doubleArray";
 	else if ( m_attrType == jPdcAttr::kVectorAttr) m_attrTypeStr = "vector";
 	else if ( m_attrType == jPdcAttr::kVectorArrayAttr) m_attrTypeStr = "vectorArray";
	else m_attrTypeStr = "invalid"; // this can't happen - I hope
	
}

jPdcAttr::~jPdcAttr() {}

