/*
 *  JFloatMatrix.h
 *  jtools
 *
 *  Created by Julian Mann on 14/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#ifndef _JFloatMatrix
#define _JFloatMatrix

#include <vector>
#include <fstream>
#include "JFloatVector.h"

#define JFloatMatrix_kTol	1.0e-5F

// *****************************************************************************

class  JFloatVector;

class  JFloatMatrix  
{

public:
	
	JFloatMatrix();

	JFloatMatrix( const JFloatMatrix & src );
	
	JFloatMatrix(
		float m00,float m01,float m02,float m03,
		float m10,float m11,float m12,float m13,
		float m20,float m21,float m22,float m23,
		float m30,float m31,float m32,float m33
	);

	JFloatMatrix( const double m[4][4] );

	JFloatMatrix( const float m[4][4] );

	~JFloatMatrix();

	JFloatMatrix &   	setToIdentity();
	
	float&				operator()(unsigned row, unsigned col );
	
	float				operator()(unsigned row, unsigned col ) const;
	
	float* 		    	operator[]( unsigned row );
	
	const float* 		operator[]( unsigned row ) const;	
	
	
 	JFloatMatrix&		operator = (const JFloatMatrix & rhs);
	
	JFloatMatrix &   	operator+=( const JFloatMatrix & rhs );
	
 	JFloatMatrix    	operator+ ( const JFloatMatrix & rhs ) const;
	
	JFloatMatrix &   	operator+=( const JFloatVector & rhs );
	
 	JFloatMatrix    	operator+ ( const JFloatVector & rhs ) const;

 	JFloatMatrix &   	operator-=( const JFloatMatrix & rhs );
	
 	JFloatMatrix		operator- ( const JFloatMatrix & rhs ) const;	
	
 	JFloatMatrix     	operator* ( const JFloatMatrix & rhs ) const;
 	
	JFloatMatrix &   	operator*=( const JFloatMatrix & rhs );
	
 	JFloatMatrix     	transpose() const;

 	JFloatMatrix &   	operator*=( float );
	
 	JFloatMatrix     	operator* ( float ) const;
	
 	bool          		operator==( const JFloatMatrix & other ) const;	
	
	bool           		operator!=( const JFloatMatrix & other ) const;
	
	JFloatMatrix &   	orientZ(  JFloatVector front,  JFloatVector up ) ;

	JFloatMatrix     	orientZ(  JFloatVector front,  JFloatVector up ) const;	
	
	JFloatMatrix &   	scale( float val );

	JFloatMatrix     	scale( float val) const;
	
 	JFloatMatrix     	inverse() const;

	friend std::ostream& operator<<(std::ostream &os, const JFloatMatrix &m) {
		os << "[ " ;
		os << "[" << m.matrix[0][0] << ", " << m.matrix[0][1] << ", " << m.matrix[0][2] << ", " << m.matrix[0][3] << "], ";
		os << "[" << m.matrix[1][0] << ", " << m.matrix[1][1] << ", " << m.matrix[1][2] << ", " << m.matrix[1][3] << "], ";
		os << "[" << m.matrix[2][0] << ", " << m.matrix[2][1] << ", " << m.matrix[2][2] << ", " << m.matrix[2][3] << "], ";
		os << "[" << m.matrix[3][0] << ", " << m.matrix[3][1] << ", " << m.matrix[3][2] << ", " << m.matrix[3][3] << "] ";
		os << "]" ;
		return os ;
	}
	
/*	
	// implement these at some point in the future
	
	
	MStatus		get( double dest[4][4] ) const;
	
	MStatus		get( float dest[4][4] ) const;
	
 	
 	JFloatMatrix &   	setToProduct( const JFloatMatrix & left,  const JFloatMatrix & rhs );
	

 	bool           		isEquivalent( const JFloatMatrix & other,  float tolerance = JFloatMatrix_kTol )  const;

 	JFloatMatrix     	homogenize() const;
	
 	JFloatMatrix     	adjoint() const;
	
 	float       		det4x4() const;
	
 	float         		det3x3() const;

	
*/
 	float matrix[4][4];

	static const JFloatMatrix zero;

	static const JFloatMatrix identity;
	
protected:


private:

};



inline float& JFloatMatrix::operator()(unsigned row, unsigned col )
{
	return matrix[row][col];
}

inline float JFloatMatrix::operator()(unsigned row, unsigned col ) const
{
	return matrix[row][col];
}

inline float* JFloatMatrix::operator[]( unsigned row )
{
	return matrix[row];
}

inline const float* JFloatMatrix::operator[]( unsigned row ) const
{
	return matrix[row];
}

#endif /* _JFloatMatrix */
