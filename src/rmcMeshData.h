
#ifndef _rmcMeshData
#define _rmcMeshData

#include <iostream>
#include <fstream>

#include "JFloatMatrix.h"
#include "JFloatVector.h"
#include "JBoundingBox.h"

#include "jTypes.h"
#include "ioUtils.h"
// #include "byteSwap.h"


class rmcMeshData
{
public:

	rmcMeshData(const char* filename); // build from file
	
	~rmcMeshData();

	static int writeB( 
			const char * filename,
			unsigned num_polys,
			unsigned num_vertices,
			unsigned num_face_vertices,
			const JFloatVectorArray & bound,
			const JIntArray & loops, 
			const JIntArray & faceVertexCount,
			const JIntArray & faceVertexIds,
			const JFloatVectorArray & vertices,
			const JFloatVectorArray & normals,
			const JFloatArray & coord_s,
			const JFloatArray & coord_t
			);

	static int writeA( 
					   const char * filename,
					   unsigned num_polys,
					   unsigned num_vertices,
					   unsigned num_face_vertices,
					   const JFloatVectorArray & bound,
					   const JIntArray & loops, 
					   const JIntArray & faceVertexCount,
					   const JIntArray & faceVertexIds,
					   const JFloatVectorArray & vertices,
					   const JFloatVectorArray & normals,
					   const JFloatArray & coord_s,
					   const JFloatArray & coord_t
					   );

	static int readHeader( 
					   const char * filename,
					   unsigned & num_polys,
					   unsigned & num_vertices,
					   unsigned & num_face_vertices,
					   JFloatVectorArray & bound
					   );
	
	static int getBoundingBox(  const char * filename , JBoundingBox &bb);
	
	static int isRMCB (const char * filename) ;
	
	static int isRMCA (const char * filename) ;

	unsigned int numFaces() const { return m_num_polys; }

	unsigned int numVertices() const  { return m_num_vertices; }
	
	unsigned int numFaceVertices() const  { return m_num_face_vertices; }
	
	const JFloatVectorArray & bound() const  {return m_bound;}	

	const JIntArray & loops() const {return m_loops;}
	
	const JIntArray & faceVertexCount()  const {return m_faceVertexCount;}
	
	const JIntArray & faceVertexIds() const  {return m_faceVertexIds;}
	
	const JFloatVectorArray & vertices()  const {return m_vertices;}
	
	const JFloatVectorArray & normals()  const {return m_normals;}
	
	JFloatVectorArray & vertices()  {return m_vertices;}
	
	JFloatVectorArray & normals()  {return m_normals;}
		
	const JFloatArray & coordS()  const {return m_coord_s;}
	
	const JFloatArray & coordT() const  {return m_coord_t;}	
	
	bool isValid() const {return m_valid;}


private:
 
	unsigned int m_num_polys;

	unsigned int m_num_vertices;
	
	unsigned int m_num_face_vertices;
	 
	JFloatVectorArray m_bound;
	
	JIntArray  m_loops;
	
	JIntArray  m_faceVertexCount;
	
	JIntArray  m_faceVertexIds;
	
	JFloatVectorArray  m_vertices;
	
	JFloatVectorArray  m_normals;
	
	JFloatArray  m_coord_s;
	
	JFloatArray  m_coord_t;
	
	bool m_valid;

};

#endif

