 #ifndef _jPdcFile
#define _jPdcFile

 
// #include <maya/MIOStream.h>
#include <string.h> 
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <map>

//#include <netinet/in.h>
// #include <maya/MStatus.h>
// #include <maya/MVector.h>
// #include <maya/std::string.h>
// #include <maya/MIntArray.h>
// #include <maya/MDoubleArray.h>
// #include <maya/MVectorArray.h>
// #include <maya/MStringArray.h>
// #include <maya/MBoundingBox.h>
 #include "JBoundingBox.h"
#include "jTypes.h"
#include "byteSwap.h"
// #include "mayaIoUtils.h"

#include "jPdcAttr.h"


struct StrCmp
{
	bool operator()(const std::string & m1, const std::string & m2) const
	{
		return (m1.compare(m2) < 0);
	}
};


typedef std::map<std::string, jPdcAttr, StrCmp>  jPdcAttrMap;


class jPdcFile {
public:

	// open the pdc file 
	jPdcFile ( const std::string& fn, int &st );
	
	// close the file and clean up
	~jPdcFile() ;
	
	// attribute names
	JStringArray getNames() const ;

	JStringArray getSortedNames() const ;
	
	bool attributeExists(const std::string& name) const ;

	JStringArray getTypes() const;
	
	jPdcAttr::jPdcAttrType typeFromName (const std::string& name) const;
	
	std::string typeStringFromName (const std::string& name) const ;

	int fileNumber(int *st=NULL) const;
	// number of particles
	int	particleCount() const ;
	
	// number of artributes
	int	attributeCount() const ;

	// get data by attribute name
	int	getData(int& data, const std::string& name)  const;
	int	getData(JIntArray& data, const std::string& name) const;
	int	getData(double& data, const std::string& name)  const;
	int	getData(JDoubleArray& data, const std::string& name) const;
	int	getData(JVector& data, const std::string& name)  const;
	int	getData(JVectorArray& data, const std::string& name)  const;

	int boundingBox(JBoundingBox &bb) ;
	// MStatus boundingBox(MBoundingBox &bb) ;


	void getHeaderInfo(int& version, int& endian, int& bitinfo1, int& bitinfo2) const;


	friend std::ostream& operator<<(std::ostream &os, const jPdcFile &file){


		os << "** " << file.m_filename << " **" << std::endl;
		os << "version " << file.m_version << std::endl;
		os << "endian " << file.m_endian << std::endl;
		os << "bitInfo1 " << file.m_bitInfo1 << std::endl;
		os << "bitInfo2 " << file.m_bitInfo2 << std::endl;
		os << "number of Particles " <<file.m_numParticles  << std::endl;
		os << "number of Attributes " << file.attributeCount() << std::endl;
		os << "-----------------------------------"<<std::endl;

		jPdcAttrMap::const_iterator iter = file.m_attrMap.begin();
		while (iter !=file.m_attrMap.end()) {
			os <<  iter->second << std::endl;
			iter++;
		}
		return os ;
	}


private:

	int					m_version;
	int					m_endian;
	int					m_bitInfo1;
	int					m_bitInfo2;
	int					m_numParticles;
	int					m_numAttr;
	
	jPdcAttrMap				m_attrMap;
	std::string				m_filename; 

};

#endif

