
#include <math.h>
#include "JFloatMatrix.h"

/**/
JFloatMatrix::JFloatMatrix(){
	setToIdentity();
}

JFloatMatrix::JFloatMatrix( const JFloatMatrix & rhs ){

	matrix[0][0] = rhs.matrix[0][0];
	matrix[0][1] = rhs.matrix[0][1];
	matrix[0][2] = rhs.matrix[0][2];
	matrix[0][3] = rhs.matrix[0][3];
							   
	matrix[1][0] = rhs.matrix[1][0];
	matrix[1][1] = rhs.matrix[1][1];
	matrix[1][2] = rhs.matrix[1][2];
	matrix[1][3] = rhs.matrix[1][3];
							   
	matrix[2][0] = rhs.matrix[2][0];
	matrix[2][1] = rhs.matrix[2][1];
	matrix[2][2] = rhs.matrix[2][2];
	matrix[2][3] = rhs.matrix[2][3];
							   
	matrix[3][0] = rhs.matrix[3][0];
	matrix[3][1] = rhs.matrix[3][1];
	matrix[3][2] = rhs.matrix[3][2];
	matrix[3][3] = rhs.matrix[3][3];

}

JFloatMatrix::JFloatMatrix(
	float m00,float m01,float m02,float m03,
	float m10,float m11,float m12,float m13,
	float m20,float m21,float m22,float m23,
	float m30,float m31,float m32,float m33
) {
	matrix[0][0] = m00;
	matrix[0][1] = m01;
	matrix[0][2] = m02;
	matrix[0][3] = m03;

	matrix[1][0] = m10;
	matrix[1][1] = m11;
	matrix[1][2] = m12;
	matrix[1][3] = m13;
	
	matrix[2][0] = m20;
	matrix[2][1] = m21;
	matrix[2][2] = m22;
	matrix[2][3] = m23;
	
	matrix[3][0] = m30;
	matrix[3][1] = m31;
	matrix[3][2] = m32;
	matrix[3][3] = m33;
}

///

JFloatMatrix::JFloatMatrix( const double m[4][4] ){
	matrix[0][0] = float(m[0][0]);
	matrix[0][1] = float(m[0][1]);
	matrix[0][2] = float(m[0][2]);
	matrix[0][3] = float(m[0][3]);

	matrix[1][0] = float(m[1][0]);
	matrix[1][1] = float(m[1][1]);
	matrix[1][2] = float(m[1][2]);
	matrix[1][3] = float(m[1][3]);
	
	matrix[2][0] = float(m[2][0]);
	matrix[2][1] = float(m[2][1]);
	matrix[2][2] = float(m[2][2]);
	matrix[2][3] = float(m[2][3]);
	
	matrix[3][0] = float(m[3][0]);
	matrix[3][1] = float(m[3][1]);
	matrix[3][2] = float(m[3][2]);
	matrix[3][3] = float(m[3][3]);
}
///
JFloatMatrix::JFloatMatrix( const float m[4][4] ){
	matrix[0][0] = m[0][0];
	matrix[0][1] = m[0][1];
	matrix[0][2] = m[0][2];
	matrix[0][3] = m[0][3];

	matrix[1][0] = m[1][0];
	matrix[1][1] = m[1][1];
	matrix[1][2] = m[1][2];
	matrix[1][3] = m[1][3];
	
	matrix[2][0] = m[2][0];
	matrix[2][1] = m[2][1];
	matrix[2][2] = m[2][2];
	matrix[2][3] = m[2][3];
	
	matrix[3][0] = m[3][0];
	matrix[3][1] = m[3][1];
	matrix[3][2] = m[3][2];
	matrix[3][3] = m[3][3];
}
///
JFloatMatrix::~JFloatMatrix(){}

JFloatMatrix &   	JFloatMatrix::setToIdentity(){
	matrix[0][0] = 1.0f; matrix[0][1] = 0.0f; matrix[0][2] = 0.0f; matrix[0][3] = 0.0f; 
	matrix[1][0] = 0.0f; matrix[1][1] = 1.0f; matrix[1][2] = 0.0f; matrix[1][3] = 0.0f; 
	matrix[2][0] = 0.0f; matrix[2][1] = 0.0f; matrix[2][2] = 1.0f; matrix[2][3] = 0.0f; 
	matrix[3][0] = 0.0f; matrix[3][1] = 0.0f; matrix[3][2] = 0.0f; matrix[3][3] = 1.0f; 
	return (*this);
}

JFloatMatrix&	JFloatMatrix::operator = (const JFloatMatrix &rhs) {
	
	matrix[0][0] = rhs(0,0);
	matrix[0][1] = rhs(0,1);
	matrix[0][2] = rhs(0,2);
	matrix[0][3] = rhs(0,3);

	matrix[1][0] = rhs(1,0);
	matrix[1][1] = rhs(1,1);
	matrix[1][2] = rhs(1,2);
	matrix[1][3] = rhs(1,3);
	
	matrix[2][0] = rhs(2,0);
	matrix[2][1] = rhs(2,1);
	matrix[2][2] = rhs(2,2);
	matrix[2][3] = rhs(2,3);
	
	matrix[3][0] = rhs(3,0);
	matrix[3][1] = rhs(3,1);
	matrix[3][2] = rhs(3,2);
	matrix[3][3] = rhs(3,3);

	return (*this);
}


JFloatMatrix &   	JFloatMatrix::operator+=( const JFloatMatrix & rhs ){
	matrix[0][0] += rhs(0,0);
	matrix[0][1] += rhs(0,1);
	matrix[0][2] += rhs(0,2);
	matrix[0][3] += rhs(0,3);

	matrix[1][0] += rhs(1,0);
	matrix[1][1] += rhs(1,1);
	matrix[1][2] += rhs(1,2);
	matrix[1][3] += rhs(1,3);
	
	matrix[2][0] += rhs(2,0);
	matrix[2][1] += rhs(2,1);
	matrix[2][2] += rhs(2,2);
	matrix[2][3] += rhs(2,3);
	
	matrix[3][0] += rhs(3,0);
	matrix[3][1] += rhs(3,1);
	matrix[3][2] += rhs(3,2);
	matrix[3][3] += rhs(3,3);
	return (*this);

}

JFloatMatrix    	JFloatMatrix::operator+ ( const JFloatMatrix & rhs ) const
{
	return JFloatMatrix(
	matrix[0][0] + rhs(0,0),
	matrix[0][1] + rhs(0,1),
	matrix[0][2] + rhs(0,2),
	matrix[0][3] + rhs(0,3),

	matrix[1][0] + rhs(1,0),
	matrix[1][1] + rhs(1,1),
	matrix[1][2] + rhs(1,2),
	matrix[1][3] + rhs(1,3),

	matrix[2][0] + rhs(2,0),
	matrix[2][1] + rhs(2,1),
	matrix[2][2] + rhs(2,2),
	matrix[2][3] + rhs(2,3),

	matrix[3][0] + rhs(3,0),
	matrix[3][1] + rhs(3,1),
	matrix[3][2] + rhs(3,2),
	matrix[3][3] + rhs(3,3)
	);

}



JFloatMatrix &   	JFloatMatrix::operator+=( const JFloatVector & rhs ){

	matrix[3][0] += rhs.x;
	matrix[3][1] += rhs.y;
	matrix[3][2] += rhs.z;

	return (*this);

}


JFloatMatrix    	JFloatMatrix::operator+ ( const JFloatVector & rhs ) const
{
	return JFloatMatrix(
	matrix[0][0] ,
	matrix[0][1] ,
	matrix[0][2] ,
	matrix[0][3] ,

	matrix[1][0] ,
	matrix[1][1] ,
	matrix[1][2] ,
	matrix[1][3] ,

	matrix[2][0] ,
	matrix[2][1] ,
	matrix[2][2] ,
	matrix[2][3] ,

	matrix[3][0] + rhs.x,
	matrix[3][1] + rhs.y,
	matrix[3][2] + rhs.z,
	matrix[3][3] 
	);

}





JFloatMatrix &   	JFloatMatrix::operator-=( const JFloatMatrix & rhs ){
	matrix[0][0] -= rhs(0,0);
	matrix[0][1] -= rhs(0,1);
	matrix[0][2] -= rhs(0,2);
	matrix[0][3] -= rhs(0,3);

	matrix[1][0] -= rhs(1,0);
	matrix[1][1] -= rhs(1,1);
	matrix[1][2] -= rhs(1,2);
	matrix[1][3] -= rhs(1,3);
	
	matrix[2][0] -= rhs(2,0);
	matrix[2][1] -= rhs(2,1);
	matrix[2][2] -= rhs(2,2);
	matrix[2][3] -= rhs(2,3);
	
	matrix[3][0] -= rhs(3,0);
	matrix[3][1] -= rhs(3,1);
	matrix[3][2] -= rhs(3,2);
	matrix[3][3] -= rhs(3,3);
	return (*this);

}

JFloatMatrix    	JFloatMatrix::operator- ( const JFloatMatrix & rhs ) const
{
	return JFloatMatrix(
	matrix[0][0] - rhs.matrix[0][0],
	matrix[0][1] - rhs.matrix[0][1],
	matrix[0][2] - rhs.matrix[0][2],
	matrix[0][3] - rhs.matrix[0][3],

	matrix[1][0] - rhs.matrix[1][0],
	matrix[1][1] - rhs.matrix[1][1],
	matrix[1][2] - rhs.matrix[1][2],
	matrix[1][3] - rhs.matrix[1][3],

	matrix[2][0] - rhs.matrix[2][0],
	matrix[2][1] - rhs.matrix[2][1],
	matrix[2][2] - rhs.matrix[2][2],
	matrix[2][3] - rhs.matrix[2][3],

	matrix[3][0] - rhs.matrix[3][0],
	matrix[3][1] - rhs.matrix[3][1],
	matrix[3][2] - rhs.matrix[3][2],
	matrix[3][3] - rhs.matrix[3][3]
	);

}

/*
JFloatMatrix     	JFloatMatrix::operator* ( const JFloatMatrix & rhs ) const
{
	return JFloatMatrix(
        matrix[0][0]*rhs.matrix[0][0] +
        matrix[0][1]*rhs.matrix[1][0] +
        matrix[0][2]*rhs.matrix[2][0] +
        matrix[0][3]*rhs.matrix[3][0],

        matrix[0][0]*rhs.matrix[0][1] +
        matrix[0][1]*rhs.matrix[1][1] +
        matrix[0][2]*rhs.matrix[2][1] +
        matrix[0][3]*rhs.matrix[3][1],

        matrix[0][0]*rhs.matrix[0][2] +
        matrix[0][1]*rhs.matrix[1][2] +
        matrix[0][2]*rhs.matrix[2][2] +
        matrix[0][3]*rhs.matrix[3][2],

        matrix[0][0]*rhs.matrix[0][3] +
        matrix[0][1]*rhs.matrix[1][3] +
        matrix[0][2]*rhs.matrix[2][3] +
        matrix[0][3]*rhs.matrix[3][3],

        matrix[1][0]*rhs.matrix[0][0] +
        matrix[1][1]*rhs.matrix[1][0] +
        matrix[1][2]*rhs.matrix[2][0] +
        matrix[1][3]*rhs.matrix[3][0],

        matrix[1][0]*rhs.matrix[0][1] +
        matrix[1][1]*rhs.matrix[1][1] +
        matrix[1][2]*rhs.matrix[2][1] +
        matrix[1][3]*rhs.matrix[3][1],

        matrix[1][0]*rhs.matrix[0][2] +
        matrix[1][1]*rhs.matrix[1][2] +
        matrix[1][2]*rhs.matrix[2][2] +
        matrix[1][3]*rhs.matrix[3][2],

        matrix[1][0]*rhs.matrix[0][3] +
        matrix[1][1]*rhs.matrix[1][3] +
        matrix[1][2]*rhs.matrix[2][3] +
        matrix[1][3]*rhs.matrix[3][3],

        matrix[2][0]*rhs.matrix[0][0] +
        matrix[2][1]*rhs.matrix[1][0] +
        matrix[2][2]*rhs.matrix[2][0] +
        matrix[2][3]*rhs.matrix[3][0],

        matrix[2][0]*rhs.matrix[0][1] +
        matrix[2][1]*rhs.matrix[1][1] +
        matrix[2][2]*rhs.matrix[2][1] +
        matrix[2][3]*rhs.matrix[3][1],

        matrix[2][0]*rhs.matrix[0][2] +
        matrix[2][1]*rhs.matrix[1][2] +
        matrix[2][2]*rhs.matrix[2][2] +
        matrix[2][3]*rhs.matrix[3][2],

        matrix[2][0]*rhs.matrix[0][3] +
        matrix[2][1]*rhs.matrix[1][3] +
        matrix[2][2]*rhs.matrix[2][3] +
        matrix[2][3]*rhs.matrix[3][3],

        matrix[3][0]*rhs.matrix[0][0] +
        matrix[3][1]*rhs.matrix[1][0] +
        matrix[3][2]*rhs.matrix[2][0] +
        matrix[3][3]*rhs.matrix[3][0],

        matrix[3][0]*rhs.matrix[0][1] +
        matrix[3][1]*rhs.matrix[1][1] +
        matrix[3][2]*rhs.matrix[2][1] +
        matrix[3][3]*rhs.matrix[3][1],

        matrix[3][0]*rhs.matrix[0][2] +
        matrix[3][1]*rhs.matrix[1][2] +
        matrix[3][2]*rhs.matrix[2][2] +
        matrix[3][3]*rhs.matrix[3][2],

        matrix[3][0]*rhs.matrix[0][3] +
        matrix[3][1]*rhs.matrix[1][3] +
        matrix[3][2]*rhs.matrix[2][3] +
        matrix[3][3]*rhs.matrix[3][3]);

}
*/
JFloatMatrix     	JFloatMatrix::operator* ( const JFloatMatrix & rhs ) const
{
	
	JFloatMatrix tmp;
	tmp.matrix[0][0]= (
					   matrix[0][0]*rhs.matrix[0][0] +
					   matrix[0][1]*rhs.matrix[1][0] +
					   matrix[0][2]*rhs.matrix[2][0] +
					   matrix[0][3]*rhs.matrix[3][0]
					   );
	
	tmp.matrix[0][1]= (
					   matrix[0][0]*rhs.matrix[0][1] +
					   matrix[0][1]*rhs.matrix[1][1] +
					   matrix[0][2]*rhs.matrix[2][1] +
					   matrix[0][3]*rhs.matrix[3][1]
					   );
	
	tmp.matrix[0][2]= (
					   matrix[0][0]*rhs.matrix[0][2] +
					   matrix[0][1]*rhs.matrix[1][2] +
					   matrix[0][2]*rhs.matrix[2][2] +
					   matrix[0][3]*rhs.matrix[3][2]
					   );
	
	tmp.matrix[0][3]= (
					   matrix[0][0]*rhs.matrix[0][3] +
					   matrix[0][1]*rhs.matrix[1][3] +
					   matrix[0][2]*rhs.matrix[2][3] +
					   matrix[0][3]*rhs.matrix[3][3]
					   );
	
	tmp.matrix[1][0]= (
					   matrix[1][0]*rhs.matrix[0][0] +
					   matrix[1][1]*rhs.matrix[1][0] +
					   matrix[1][2]*rhs.matrix[2][0] +
					   matrix[1][3]*rhs.matrix[3][0]
					   );
	
	tmp.matrix[1][1]= (
					   matrix[1][0]*rhs.matrix[0][1] +
					   matrix[1][1]*rhs.matrix[1][1] +
					   matrix[1][2]*rhs.matrix[2][1] +
					   matrix[1][3]*rhs.matrix[3][1]
					   );
	
	tmp.matrix[1][2]= (
					   matrix[1][0]*rhs.matrix[0][2] +
					   matrix[1][1]*rhs.matrix[1][2] +
					   matrix[1][2]*rhs.matrix[2][2] +
					   matrix[1][3]*rhs.matrix[3][2]
					   );
	
	tmp.matrix[1][3]= (
					   matrix[1][0]*rhs.matrix[0][3] +
					   matrix[1][1]*rhs.matrix[1][3] +
					   matrix[1][2]*rhs.matrix[2][3] +
					   matrix[1][3]*rhs.matrix[3][3]
					   );
	
	tmp.matrix[2][0]= (
					   matrix[2][0]*rhs.matrix[0][0] +
					   matrix[2][1]*rhs.matrix[1][0] +
					   matrix[2][2]*rhs.matrix[2][0] +
					   matrix[2][3]*rhs.matrix[3][0]
					   );
	
	tmp.matrix[2][1]= (
					   matrix[2][0]*rhs.matrix[0][1] +
					   matrix[2][1]*rhs.matrix[1][1] +
					   matrix[2][2]*rhs.matrix[2][1] +
					   matrix[2][3]*rhs.matrix[3][1]
					   );
	
	tmp.matrix[2][2]= (
					   matrix[2][0]*rhs.matrix[0][2] +
					   matrix[2][1]*rhs.matrix[1][2] +
					   matrix[2][2]*rhs.matrix[2][2] +
					   matrix[2][3]*rhs.matrix[3][2]
					   );
	
	tmp.matrix[2][3]= (
					   matrix[2][0]*rhs.matrix[0][3] +
					   matrix[2][1]*rhs.matrix[1][3] +
					   matrix[2][2]*rhs.matrix[2][3] +
					   matrix[2][3]*rhs.matrix[3][3]
					   );
	
	tmp.matrix[3][0]= (
					   matrix[3][0]*rhs.matrix[0][0] +
					   matrix[3][1]*rhs.matrix[1][0] +
					   matrix[3][2]*rhs.matrix[2][0] +
					   matrix[3][3]*rhs.matrix[3][0]
					   );
	
	tmp.matrix[3][1]= (
					   matrix[3][0]*rhs.matrix[0][1] +
					   matrix[3][1]*rhs.matrix[1][1] +
					   matrix[3][2]*rhs.matrix[2][1] +
					   matrix[3][3]*rhs.matrix[3][1]
					   );
	
	tmp.matrix[3][2]= (
					   matrix[3][0]*rhs.matrix[0][2] +
					   matrix[3][1]*rhs.matrix[1][2] +
					   matrix[3][2]*rhs.matrix[2][2] +
					   matrix[3][3]*rhs.matrix[3][2]
					   );
	
	tmp.matrix[3][3]= (
					   matrix[3][0]*rhs.matrix[0][3] +
					   matrix[3][1]*rhs.matrix[1][3] +
					   matrix[3][2]*rhs.matrix[2][3] +
					   matrix[3][3]*rhs.matrix[3][3]
					   );
	
	return tmp;
}


JFloatMatrix &   	JFloatMatrix::operator*=( const JFloatMatrix & rhs ){

	JFloatMatrix tmp;
	tmp.matrix[0][0]= (
        matrix[0][0]*rhs.matrix[0][0] +
        matrix[0][1]*rhs.matrix[1][0] +
        matrix[0][2]*rhs.matrix[2][0] +
        matrix[0][3]*rhs.matrix[3][0]
	);

	tmp.matrix[0][1]= (
        matrix[0][0]*rhs.matrix[0][1] +
        matrix[0][1]*rhs.matrix[1][1] +
        matrix[0][2]*rhs.matrix[2][1] +
        matrix[0][3]*rhs.matrix[3][1]
	);

	tmp.matrix[0][2]= (
        matrix[0][0]*rhs.matrix[0][2] +
        matrix[0][1]*rhs.matrix[1][2] +
        matrix[0][2]*rhs.matrix[2][2] +
        matrix[0][3]*rhs.matrix[3][2]
	);

	tmp.matrix[0][3]= (
        matrix[0][0]*rhs.matrix[0][3] +
        matrix[0][1]*rhs.matrix[1][3] +
        matrix[0][2]*rhs.matrix[2][3] +
        matrix[0][3]*rhs.matrix[3][3]
	);

	tmp.matrix[1][0]= (
        matrix[1][0]*rhs.matrix[0][0] +
        matrix[1][1]*rhs.matrix[1][0] +
        matrix[1][2]*rhs.matrix[2][0] +
        matrix[1][3]*rhs.matrix[3][0]
	);

	tmp.matrix[1][1]= (
        matrix[1][0]*rhs.matrix[0][1] +
        matrix[1][1]*rhs.matrix[1][1] +
        matrix[1][2]*rhs.matrix[2][1] +
        matrix[1][3]*rhs.matrix[3][1]
	);

	tmp.matrix[1][2]= (
        matrix[1][0]*rhs.matrix[0][2] +
        matrix[1][1]*rhs.matrix[1][2] +
        matrix[1][2]*rhs.matrix[2][2] +
        matrix[1][3]*rhs.matrix[3][2]
	);

	tmp.matrix[1][3]= (
        matrix[1][0]*rhs.matrix[0][3] +
        matrix[1][1]*rhs.matrix[1][3] +
        matrix[1][2]*rhs.matrix[2][3] +
        matrix[1][3]*rhs.matrix[3][3]
	);

	tmp.matrix[2][0]= (
        matrix[2][0]*rhs.matrix[0][0] +
        matrix[2][1]*rhs.matrix[1][0] +
        matrix[2][2]*rhs.matrix[2][0] +
        matrix[2][3]*rhs.matrix[3][0]
	);

	tmp.matrix[2][1]= (
        matrix[2][0]*rhs.matrix[0][1] +
        matrix[2][1]*rhs.matrix[1][1] +
        matrix[2][2]*rhs.matrix[2][1] +
        matrix[2][3]*rhs.matrix[3][1]
	);

	tmp.matrix[2][2]= (
        matrix[2][0]*rhs.matrix[0][2] +
        matrix[2][1]*rhs.matrix[1][2] +
        matrix[2][2]*rhs.matrix[2][2] +
        matrix[2][3]*rhs.matrix[3][2]
	);

	tmp.matrix[2][3]= (
        matrix[2][0]*rhs.matrix[0][3] +
        matrix[2][1]*rhs.matrix[1][3] +
        matrix[2][2]*rhs.matrix[2][3] +
        matrix[2][3]*rhs.matrix[3][3]
	);

	tmp.matrix[3][0]= (
        matrix[3][0]*rhs.matrix[0][0] +
        matrix[3][1]*rhs.matrix[1][0] +
        matrix[3][2]*rhs.matrix[2][0] +
        matrix[3][3]*rhs.matrix[3][0]
	);

	tmp.matrix[3][1]= (
        matrix[3][0]*rhs.matrix[0][1] +
        matrix[3][1]*rhs.matrix[1][1] +
        matrix[3][2]*rhs.matrix[2][1] +
        matrix[3][3]*rhs.matrix[3][1]
	);

	tmp.matrix[3][2]= (
        matrix[3][0]*rhs.matrix[0][2] +
        matrix[3][1]*rhs.matrix[1][2] +
        matrix[3][2]*rhs.matrix[2][2] +
        matrix[3][3]*rhs.matrix[3][2]
	);

	tmp.matrix[3][3]= (
        matrix[3][0]*rhs.matrix[0][3] +
        matrix[3][1]*rhs.matrix[1][3] +
        matrix[3][2]*rhs.matrix[2][3] +
        matrix[3][3]*rhs.matrix[3][3]
	);
	(*this) = tmp;
	return (*this);
	
}

JFloatMatrix     	JFloatMatrix::transpose() const {

	return JFloatMatrix (
	matrix[0][0] ,matrix[1][0] ,matrix[2][0] ,matrix[3][0],
	matrix[0][1] ,matrix[1][1] ,matrix[2][1] ,matrix[3][1],
	matrix[0][2] ,matrix[1][2] ,matrix[2][2] ,matrix[3][2],
	matrix[0][3] ,matrix[1][3] ,matrix[2][3] ,matrix[3][3]
	);

}

JFloatMatrix &   	JFloatMatrix::operator*=( float val ){

	matrix[0][0] *= val;
	matrix[0][1] *= val;
	matrix[0][2] *= val;
	matrix[0][3] *= val;

	matrix[1][0] *= val;
	matrix[1][1] *= val;
	matrix[1][2] *= val;
	matrix[1][3] *= val;
	
	matrix[2][0] *= val;
	matrix[2][1] *= val;
	matrix[2][2] *= val;
	matrix[2][3] *= val;
	
	matrix[3][0] *= val;
	matrix[3][1] *= val;
	matrix[3][2] *= val;
	matrix[3][3] *= val;
	return (*this);

}

JFloatMatrix     	JFloatMatrix::operator* ( float val) const{
	return JFloatMatrix(
	matrix[0][0] * val,
	matrix[0][1] * val,
	matrix[0][2] * val,
	matrix[0][3] * val,

	matrix[1][0] * val,
	matrix[1][1] * val,
	matrix[1][2] * val,
	matrix[1][3] * val,

	matrix[2][0] * val,
	matrix[2][1] * val,
	matrix[2][2] * val,
	matrix[2][3] * val,

	matrix[3][0] * val,
	matrix[3][1] * val,
	matrix[3][2] * val,
	matrix[3][3] * val
	);

}

JFloatMatrix &   	JFloatMatrix::scale( float val ){


	JFloatMatrix scaleMat;
	scaleMat.matrix[0][0] *= val;
	scaleMat.matrix[1][1] *= val;
	scaleMat.matrix[2][2] *= val;
	
	 (*this) *= scaleMat;
	
	return (*this);

}

JFloatMatrix     	JFloatMatrix::scale( float val) const{

	JFloatMatrix scaleMat;
	scaleMat.matrix[0][0] *= val;
	scaleMat.matrix[1][1] *= val;
	scaleMat.matrix[2][2] *= val;

	return (*this) * scaleMat;
}




bool          JFloatMatrix::operator==( const JFloatMatrix & rhs ) const{
	  
	if ( matrix[0][0] != rhs.matrix[0][0]) return false;
	if ( matrix[0][1] != rhs.matrix[0][1]) return false;
	if ( matrix[0][2] != rhs.matrix[0][2]) return false;
	if ( matrix[0][3] != rhs.matrix[0][3]) return false;

	if ( matrix[1][0] != rhs.matrix[1][0]) return false;
	if ( matrix[1][1] != rhs.matrix[1][1]) return false;
	if ( matrix[1][2] != rhs.matrix[1][2]) return false;
	if ( matrix[1][3] != rhs.matrix[1][3]) return false;

	if ( matrix[2][0] != rhs.matrix[2][0]) return false;
	if ( matrix[2][1] != rhs.matrix[2][1]) return false;
	if ( matrix[2][2] != rhs.matrix[2][2]) return false;
	if ( matrix[2][3] != rhs.matrix[2][3]) return false;

	if ( matrix[3][0] != rhs.matrix[3][0]) return false;
	if ( matrix[3][1] != rhs.matrix[3][1]) return false;
	if ( matrix[3][2] != rhs.matrix[3][2]) return false;
	if ( matrix[3][3] != rhs.matrix[3][3]) return false;
	return true;
}

bool           		JFloatMatrix::operator!=( const JFloatMatrix & other ) const{

	return (! ( (*this) == other ) );
}

JFloatMatrix     	JFloatMatrix::inverse() const{


    float fA0 = matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
    float fA1 = matrix[0][0]*matrix[1][2] - matrix[0][2]*matrix[1][0];
    float fA2 = matrix[0][0]*matrix[1][3] - matrix[0][3]*matrix[1][0];
    float fA3 = matrix[0][1]*matrix[1][2] - matrix[0][2]*matrix[1][1];
    float fA4 = matrix[0][1]*matrix[1][3] - matrix[0][3]*matrix[1][1];
    float fA5 = matrix[0][2]*matrix[1][3] - matrix[0][3]*matrix[1][2];
    float fB0 = matrix[2][0]*matrix[3][1] - matrix[2][1]*matrix[3][0];
    float fB1 = matrix[2][0]*matrix[3][2] - matrix[2][2]*matrix[3][0];
    float fB2 = matrix[2][0]*matrix[3][3] - matrix[2][3]*matrix[3][0];
    float fB3 = matrix[2][1]*matrix[3][2] - matrix[2][2]*matrix[3][1];
    float fB4 = matrix[2][1]*matrix[3][3] - matrix[2][3]*matrix[3][1];
    float fB5 = matrix[2][2]*matrix[3][3] - matrix[2][3]*matrix[3][2];

    float fDet = fA0*fB5-fA1*fB4+fA2*fB3+fA3*fB2-fA4*fB1+fA5*fB0;
    if (fabs(fDet) <= JFloatMatrix_kTol )
    {
        return JFloatMatrix::zero;
    }

    JFloatMatrix kInv;
    kInv.matrix[0][0] =  + matrix[1][1]*fB5 - matrix[1][2]*fB4 + matrix[1][3]*fB3;
    kInv.matrix[1][0] =  - matrix[1][0]*fB5 + matrix[1][2]*fB2 - matrix[1][3]*fB1;
    kInv.matrix[2][0] =  + matrix[1][0]*fB4 - matrix[1][1]*fB2 + matrix[1][3]*fB0;
    kInv.matrix[3][0] =  - matrix[1][0]*fB3 + matrix[1][1]*fB1 - matrix[1][2]*fB0;
    kInv.matrix[0][1] =  - matrix[0][1]*fB5 + matrix[0][2]*fB4 - matrix[0][3]*fB3;
    kInv.matrix[1][1] =  + matrix[0][0]*fB5 - matrix[0][2]*fB2 + matrix[0][3]*fB1;
    kInv.matrix[2][1] =  - matrix[0][0]*fB4 + matrix[0][1]*fB2 - matrix[0][3]*fB0;
    kInv.matrix[3][1] =  + matrix[0][0]*fB3 - matrix[0][1]*fB1 + matrix[0][2]*fB0;
    kInv.matrix[0][2] = + matrix[3][1]*fA5 - matrix[3][2]*fA4 + matrix[3][3]*fA3;
    kInv.matrix[1][2] =  - matrix[3][0]*fA5 + matrix[3][2]*fA2 - matrix[3][3]*fA1;
    kInv.matrix[2][2] = + matrix[3][0]*fA4 - matrix[3][1]*fA2 + matrix[3][3]*fA0;
    kInv.matrix[3][2] =  - matrix[3][0]*fA3 + matrix[3][1]*fA1 - matrix[3][2]*fA0;
    kInv.matrix[0][3] = - matrix[2][1]*fA5 + matrix[2][2]*fA4 - matrix[2][3]*fA3;
    kInv.matrix[1][3] = + matrix[2][0]*fA5 - matrix[2][2]*fA2 + matrix[2][3]*fA1;
    kInv.matrix[2][3] = - matrix[2][0]*fA4 + matrix[2][1]*fA2 - matrix[2][3]*fA0;
    kInv.matrix[3][3] = + matrix[2][0]*fA3 - matrix[2][1]*fA1 + matrix[2][2]*fA0;

    float fInvDet = ((float)1.0)/fDet;
    kInv.matrix[0][0] *= fInvDet;
    kInv.matrix[0][1] *= fInvDet;
    kInv.matrix[0][2] *= fInvDet;
    kInv.matrix[0][3] *= fInvDet;
    kInv.matrix[1][0] *= fInvDet;
    kInv.matrix[1][1] *= fInvDet;
    kInv.matrix[1][2] *= fInvDet;
    kInv.matrix[1][3] *= fInvDet;
    kInv.matrix[2][0] *= fInvDet;
    kInv.matrix[2][1] *= fInvDet;
    kInv.matrix[2][2] *= fInvDet;
    kInv.matrix[2][3] *= fInvDet;
    kInv.matrix[3][0] *= fInvDet;
    kInv.matrix[3][1] *= fInvDet;
    kInv.matrix[3][2] *= fInvDet;
    kInv.matrix[3][3] *= fInvDet;

    return kInv;

}

JFloatMatrix &   	JFloatMatrix::orientZ(  JFloatVector front,  JFloatVector up ) {


	front.normalize();
	JFloatVector side = front^up;
	side.normalize();
	up = side^front;

	matrix[0][0] = side.x;
	matrix[0][1] = side.y;
	matrix[0][2] = side.z;

	matrix[1][0] = up.x;
	matrix[1][1] = up.y;
	matrix[1][2] = up.z;

	matrix[2][0] = front.x;
	matrix[2][1] = front.y;
	matrix[2][2] = front.z;

	return (*this);

}

JFloatMatrix     	JFloatMatrix::orientZ(  JFloatVector front,  JFloatVector up )const{

	front.normalize();
	JFloatVector side = front^up;
	side.normalize();
	up = side^front;
	
	return JFloatMatrix(
	side.x,
	side.y,
	side.z,
	0.0f,
	up.x,
	up.y,
	up.z,
	0.0f,
	front.x,
	front.y,
	front.z,
	0.0f,
	matrix[3][0] ,
	matrix[3][1] ,
	matrix[3][2] ,
	matrix[3][3] 
	);

}




const JFloatMatrix  JFloatMatrix::zero = JFloatMatrix(0.0f,0.0f,0.0f,0.0f, 0.0f,0.0f,0.0f,0.0f, 0.0f,0.0f,0.0f,0.0f, 0.0f,0.0f,0.0f,0.0f );

const JFloatMatrix  JFloatMatrix::identity =  JFloatMatrix(1.0f,0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f,0.0f, 0.0f,0.0f,0.0f,1.0f );


/* 
JFloatMatrix     	JFloatMatrix::homogenize() const {


}

///
MStatus		get( double dest[4][4] ) const;
///
MStatus		get( float dest[4][4] ) const;
///
///


bool        JFloatMatrix::isEquivalent( const JFloatMatrix & other,  float tolerance = JFloatMatrix_kTol )  const{
	

}
*/
/*
///
JFloatMatrix &   	setToProduct( const JFloatMatrix & left,  const JFloatMatrix & rhs );
///
///

///

///


///

///

///
friend OPENMAYA_EXPORT JFloatMatrix	operator* ( float,	const JFloatMatrix & rhs );
///

///
///

///
JFloatMatrix     	adjoint() const;
///
///
float       		det4x4() const;
///
float         		det3x3() const;
///

///
// friend OPENMAYA_EXPORT IOS_REF(ostream)& operator<< ( IOS_REF(ostream)& os,	const JFloatMatrix& m );

/// the matrix data
float matrix[4][4];



*/
