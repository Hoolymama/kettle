/***************************************************************************
                        triKdTree.cpp  -  description
                           -------------------
  begin                : Mon Apr 3 2006
  copyright            : (C) 2006 by Julian Mann
  email                : julian.mann@gmail.com

this is almost the same as ptKdTree
the exception being that the data stored - i.e. the triangles in bounding boxes,
may be in a bucket, but may overlap another bucket. Therefore each bucket node
must have attached a list of nodes that overlap. This is the overlap list
***************************************************************************/

#define P_TOLERANCE 0.000001

#include "triKdTree.h"

triKdTree::triKdTree() //  constructor
	: m_perm(0),
	  m_pRoot(0),
	  m_maxPointsPerBucket(4)
{
	m_pRoot = 0;
	m_perm = new TRI_LIST;

}

triKdTree::~triKdTree()
{
	if (m_perm) {
		TRI_LIST::iterator p =  m_perm->begin();
		while (p != m_perm->end()) {
			delete *p;
			*p = 0;
			p++;
		}
		delete m_perm;
		m_perm = 0;
	}

	makeEmpty(m_pRoot);  // recursively delete tree
}

void triKdTree::makeEmpty(triDataKdNode *p) {
	if (p) { // if the pointer aint NULL
		if (!(p->bucket)) {
			makeEmpty(p->loChild);	// recurse through kids
			makeEmpty(p->hiChild);
		}
		else {
			delete p->overlapList;
			p->overlapList  = 0;
		}
		delete p;
		p = 0;					// zap the little bugger
	}
}

const triDataKdNode *triKdTree::root() {
	return m_pRoot;
};

//populate the tree with triData objects
/*
MStatus triKdTree::populate(const MFnDynSweptGeometryData  &g)  {
 	MStatus st;
	MString method("triKdTree::populate");

	unsigned int len = g.triangleCount(&st);mser;

	if (len) {
		for (unsigned i = 0; i < len; i++) {
			MDynSweptTriangle tri =  g.sweptTriangle(i);
			triData * t = new triData(tri, i);
			m_perm->push_back(t) ;
		}
	} else {
		return MS::kFailure;
	}
	return MS::kSuccess;
}
*/

//populate the tree with triData objects, set index from calling func
MStatus triKdTree::populate(
  const MPointArray &lastPoints,
  const MPointArray &points,
  const MIntArray &triangleVertices
)  {
	MStatus st;

	unsigned int len = triangleVertices.length();
	unsigned counter = 0;
	unsigned triCounter = 0;
	MVectorArray lastTriangle(3);
	MVectorArray triangle(3);
	while (counter < len) {
		for (unsigned j = 0; j < 3; j++) {
			const unsigned &index = triangleVertices[counter];
			lastTriangle.set(MVector(lastPoints[index]), j);
			triangle.set(MVector(points[index]), j);

			counter++;
		}
		triData *t = new triData(lastTriangle, triangle, triCounter);
		m_perm->push_back(t) ;
		m_ordered_triangles.push_back(t);
		triCounter ++;
	}
	return MS::kSuccess;
}






//populate the tree with triData objects, set index from calling func
MStatus triKdTree::populate(const MFnDynSweptGeometryData  &g, unsigned id)  {
	MStatus st;

	unsigned int len = g.triangleCount(&st); mser;

	for (unsigned i = 0; i < len; i++) {
		MDynSweptTriangle tri =  g.sweptTriangle(i);
		triData *t = new triData(tri, i, id);
		m_perm->push_back(t) ;
		m_ordered_triangles.push_back(t);
	}

	return MS::kSuccess;
}

MStatus triKdTree::populate(const MFnDynSweptGeometryData  &g, unsigned id,
                            const MBoundingBox &box)  {
	MStatus st;
	MString method("triKdTree::populate");
	unsigned int len = g.triangleCount(&st); mser;
	for (unsigned i = 0; i < len; i++) {
		MDynSweptTriangle tri =  g.sweptTriangle(i);
		if (! box.contains(MPoint(tri.vertex(0)))) { continue; }
		if (! box.contains(MPoint(tri.vertex(1)))) { continue; }
		if (! box.contains(MPoint(tri.vertex(2)))) { continue; }
		triData *t = new triData(tri, i, id);
		m_perm->push_back(t) ;
		m_ordered_triangles.push_back(t);
	}
	return MS::kSuccess;
}

//populate the tree with triData objects, set index and parent index as tri and face indices
MStatus triKdTree::populate(const MDagPath  &meshPath)  {
	MStatus st;
	MString method("triKdTree::populate");

	MItMeshPolygon iter(meshPath, MObject::kNullObj, &st); msert;
	// int prevIndex;
	MPointArray triPoints(3);
	MIntArray vertIds(3);
	unsigned i = 0;
	int triCount;
	for (iter.reset(); !iter.isDone(); iter.next() , i++) {

		if (!iter.hasValidTriangulation() ) { continue; }

		st = iter.numTriangles(triCount);
		for (int j = 0; j < triCount; j++) {

			st = iter.getTriangle( j, triPoints, vertIds, MSpace::kWorld );

			MVector  A = MVector(triPoints[0]);
			MVector  B = MVector(triPoints[1]);
			MVector  C = MVector(triPoints[2]);

			triData *t = new triData(A, B, C, i, j);
			m_perm->push_back(t) ;
			m_ordered_triangles.push_back(t);
		}
	}
	return MS::kSuccess;
}

int triKdTree::size() {
	return int(m_perm->size());
}

void triKdTree::build() {
	int low = 0;
	int high = (size() - 1);
	m_pRoot = build(low, high);
	TRI_LIST::iterator currentBox = m_perm->begin();
	while (currentBox != m_perm->end()) {
		setOverlapList(m_pRoot , *currentBox);
		currentBox++;
	}
}


triDataKdNode   *triKdTree::build(	 int low,  int high	) {
	// // cout << "in subBuild routine " << endl;
	triDataKdNode *p = new triDataKdNode;

	if (((high - low) + 1) <= m_maxPointsPerBucket) {
		// only bucket nodes will hold an overlapList
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
		p->overlapList = new TRI_LIST;
	}
	else {

		p->bucket = false;
		p->cutAxis = findMaxAxis(low, high);
		int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		p->cutVal = ((*m_perm)[mid])->center(p->cutAxis);
		p->loChild = build(low, mid);
		p->hiChild = build(mid + 1, high);
	}
	return p;
}


void triKdTree::wirthSelect(  int left,  int right,  int k, axis cutAxis )
{
	int n = (right - left) + 1;
	if (n <= 1) { return; }
	int i, j, l, m;
	triData *x;
	triData *tmp;

	l = left;
	m = right;
	while (l < m) {
		x = (*m_perm)[k];
		i = l;
		j = m;
		do {
			while (  ((*m_perm)[i])->center(cutAxis) <  x->center(cutAxis)  ) { i++; }
			while (  ((*m_perm)[j])->center(cutAxis) >  x->center(cutAxis)  ) { j--; }

			if (i <= j) {
				// swap
				tmp = (*m_perm)[i];
				(*m_perm)[i] = (*m_perm)[j] ;
				(*m_perm)[j] = tmp ;
				i++; j--;
			}
		}
		while (i <= j);
		if (j < k) { l = i; }
		if (k < i) { m = j; }
	}
}

axis triKdTree::findMaxAxis(const  int low, const  int high) const {


	// The idea here is just to find the axis containing the longest
	// side of the bounding rectangle of the points

	// From a vector of N points we just take sqrtN samples
	// in order to keep the time down to O(N)
	// should be ok though
	double minx , miny,  minz , maxx , maxy , maxz, tmpVal;
	double sx, sy, sz;

	int  num = (high - low ) + 1;
	int interval = int(sqrt(double(num)));
	// unsigned int intervalSq = interval*interval;
	int i;
	MPoint p = ((*m_perm)[low])->center();
	minx = p.x;
	maxx = minx;
	miny = p.y;
	maxy = miny;
	minz = p.z;
	maxz = minz;

	for (i = (low + interval); i <= high; i += interval ) {
		p = ((*m_perm)[i])->center();
		tmpVal = p.x;
		if (tmpVal < minx) {
			minx = tmpVal;
		}
		else {
			if (tmpVal > maxx) {
				maxx = tmpVal;
			}
		}
		tmpVal = p.y;
		if (tmpVal < miny) {
			miny = tmpVal;
		}
		else {
			if (tmpVal > maxy) {
				maxy = tmpVal;
			}
		}
		tmpVal = p.z;
		if (tmpVal < minz) {
			minz = tmpVal;
		}
		else {
			if (tmpVal > maxz) {
				maxz = tmpVal;
			}
		}
	}
	sx = maxx - minx;
	sy = maxy - miny;
	sz = maxz - minz;

	if (sx > sy) {
		// y is not the greatest
		if (sx > sz) {
			return mayaMath::xAxis;
		}
		else {
			return mayaMath::zAxis;
		}
	}
	else {
		// x is not the greatest
		if (sy > sz) {
			return mayaMath::yAxis;
		}
		else {
			return mayaMath::zAxis;
		}
	}
}

void  triKdTree::setOverlapList(triDataKdNode *p,  triData *tb  )  {
	// recursive setOverlapList method
	// put triBox in every bucket it overlaps
	if (p->bucket) {
		p->overlapList->push_back(tb);
	}
	else {
		if (tb->min(p->cutAxis) < p->cutVal) {
			setOverlapList(p->loChild, tb);
		}
		if (tb->max(p->cutAxis) > p->cutVal) {
			setOverlapList(p->hiChild, tb);
		}
	}
}


void  triKdTree::searchList(
  const TRI_LIST *overlapList,
  const MPoint &searchPoint,
  double &radius,
  triData &result,
  int ignoreMin,
  int ignoreMax
) const {

	TRI_LIST::const_iterator currentTriBoxPtr;
	currentTriBoxPtr = overlapList->begin();

	while (currentTriBoxPtr != overlapList->end()) {
		if (((*currentTriBoxPtr)->id() <  ignoreMin)
		    ||  ((*currentTriBoxPtr)->id() >  ignoreMax)) { // if it's not me
			if ((*currentTriBoxPtr)->sphereIntersectsBB(searchPoint, radius)  ) {
				// if the box is within our search radius, then the triangle might also be
				double dist = 0;
				// sphereIntersectsTri is in mayaMath.h
				if ( (*currentTriBoxPtr)->sphereIntersectsTri(searchPoint, radius, dist)) {
					// cerr << "tri is within " << radius << " -  actual dist to tri is " << dist << endl;
					// if the tri is within the radius then it is currently the closest triangle
					// because the radius has been shrinking
					// NOTE - if successful, the triData object will have cached the bary coords and the hit point
					// so if this triData is the final result - i.e. the closest, then the calling function
					// can just pick up the cached closest point rather than recalculate.
					// It can also ask the triangle to calculate map values from the cached barycentrics

					// Now shrink the radius and set the result
					radius	=  dist;
					result =   **currentTriBoxPtr;
					// cerr << "updating result   - "  << endl;
				}
			}
		}
		currentTriBoxPtr++;
	}
}

// pointOnTriangleInfo version
void  triKdTree::searchList(
  const TRI_LIST *overlapList,
  const MPoint &searchPoint,
  double &radius,
  pointOnTriangleInfo &pInfo,
  int ignoreMin,
  int ignoreMax
) const {

	TRI_LIST::const_iterator currentTriBoxPtr;
	currentTriBoxPtr = overlapList->begin();
	bool found = false;
	while (currentTriBoxPtr != overlapList->end()) {
		if (((*currentTriBoxPtr)->id() <  ignoreMin)
		    ||  ((*currentTriBoxPtr)->id() >  ignoreMax)) { // if it's not me
			if ((*currentTriBoxPtr)->sphereIntersectsBB(searchPoint, radius)  ) {
				// if the box is within our search radius, then the triangle might also be
				double dist = 0;
				// sphereIntersectsTri is in mayaMath.h

				if ( (*currentTriBoxPtr)->sphereIntersectsTri(searchPoint, radius, pInfo)) {
					found = true;
					// Now shrink the radius and set the result
					radius	=  pInfo.distance();
					// result =   **currentTriBoxPtr;
					// cerr << "updating result   - "  << endl;
				}
			}
		}
		currentTriBoxPtr++;
	}
	// now we have the closest point, so calc the UVs
}



// recursive function to find the closest triangle
void triKdTree::closestTri(
  const triDataKdNode *p,
  const MPoint &searchPoint,
  double &radius,   // radius is passed as reference but is not const, it shrinks  during the search.
  triData &result,
  int ignoreMin,
  int ignoreMax )  const  {
	//bool found = false;
	if (p->bucket) {
		searchList( p->overlapList, searchPoint, radius, result, ignoreMin, ignoreMax);
	}
	else {
		double diff = searchPoint[(p->cutAxis)] -
		              p->cutVal; // distance to the cut wall for this bucket
		if (diff < 0) { // we are in the lo child so search it
			closestTri(p->loChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				closestTri(p->hiChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			}
		}
		else {   // we are in the hi child so search it
			closestTri(p->hiChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				closestTri(p->loChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			}
		}
	}
}


// recursive function to find the closest triangle
void triKdTree::closestTri(
  const triDataKdNode *p,
  const MPoint &searchPoint,
  double &radius,   // radius is passed as reference but is not const, it shrinks  during the search.
  pointOnTriangleInfo &result,
  int ignoreMin,
  int ignoreMax )  const  {
	//bool found = false;
	if (p->bucket) {
		searchList( p->overlapList, searchPoint, radius, result, ignoreMin, ignoreMax);
	}
	else {
		double diff = searchPoint[(p->cutAxis)] -
		              p->cutVal; // distance to the cut wall for this bucket
		if (diff < 0) { // we are in the lo child so search it
			closestTri(p->loChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				closestTri(p->hiChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			}
		}
		else {   // we are in the hi child so search it
			closestTri(p->hiChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				closestTri(p->loChild, searchPoint, radius, result, ignoreMin, ignoreMax);
			}
		}
	}
}


void  triKdTree::boxSearch(const triDataKdNode *p, const MBoundingBox &searchBox,
                           TRI_LIST *resultBoxes, int ignoreMin, int ignoreMax) const {
	if (p->bucket) {
		makeResultBoxes( p->overlapList, searchBox, resultBoxes, ignoreMin, ignoreMax) ;
	}
	else {
		if (searchBox.min()[(p->cutAxis)]  < p->cutVal) {
			boxSearch(p->loChild, searchBox, resultBoxes, ignoreMin, ignoreMax);
		}
		if (searchBox.max()[(p->cutAxis)] > p->cutVal) {
			boxSearch(p->hiChild, searchBox, resultBoxes, ignoreMin, ignoreMax);
		}
	}
}


void  triKdTree::makeResultBoxes(const TRI_LIST *overlapList,
                                 const MBoundingBox &searchBox , TRI_LIST *resultBoxes, int ignoreMin,
                                 int ignoreMax) const  {
	TRI_LIST::const_iterator currentTriBox;
	currentTriBox = overlapList->begin();

	while (currentTriBox != overlapList->end()) {

		if (((*currentTriBox)->id() <  ignoreMin)
		    ||  ((*currentTriBox)->id() >  ignoreMax)) { // if it's not me
			if (searchBox.intersects((*currentTriBox)->box())) {
				//	if (!((*currentTriBox)->met(searchBox))){ // if we havn't already tested against this triangle
				resultBoxes->push_back(*currentTriBox);
				//	searchBox->registerVisit(*currentTriBox);
				//	}
			}
		}
		currentTriBox++;
	}
}




TRI_LIST &triKdTree::triangleList() {
	return *m_perm;
}
const TRI_LIST &triKdTree::orderedTriangleList() const {
	return m_ordered_triangles;
}
