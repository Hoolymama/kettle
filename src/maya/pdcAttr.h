/*
 *  pdcAttr.h
 *  jtools
 *
 *  Created by Julian Mann on 15/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */
 #ifndef _pdcAttr
#define _pdcAttr

 
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <vector>

class pdcAttr {
	
	public:
	enum PdcAttrType{
		kIntAttr, 
		kIntArrayAttr, 
		kDoubleAttr, 
		kDoubleArrayAttr, 
		kVectorAttr, 
		kVectorArrayAttr, 
		kInvalid };

	pdcAttr() ;
	pdcAttr(const MString& name, int type, int pos, int i ) ;
	
	~pdcAttr() ;


	friend ostream& operator<<(ostream &os, const pdcAttr &attr)
	{
		os << attr.m_attrName ;
		if (attr.m_attrType == kIntAttr) { os << " is an int" ;}
		else if  (attr.m_attrType == kIntArrayAttr) { os << " is an array of ints" ;}
		else if  (attr.m_attrType == kDoubleAttr) { os << " is a double" ;}
		else if  (attr.m_attrType == kDoubleArrayAttr) { os << " is an array of doubles" ;}
		else if  (attr.m_attrType == kVectorAttr) { os << " is a vector" ;}
		else if  (attr.m_attrType == kVectorArrayAttr) { os << " is an array of vectors" ;}
		else if  (attr.m_attrType == kInvalid) { os << " is invalid" ;}

		return os ;
	}

	bool isValid() const { return (m_attrType != kInvalid);}
	
	MString m_attrName;
	MString m_attrTypeStr;
	PdcAttrType m_attrType;
	int m_dataPos;
	int m_dataIndex;


private:


	
};

#endif

typedef std::vector<pdcAttr> pdcAttrArray;

