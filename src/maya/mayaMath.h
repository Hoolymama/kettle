/*
 *  mayaMath.h
 *  jtools
 *
 *  Created by Julian Mann on 06/12/2006.
 *  Copyright 2006 hoolyMama. All rights reserved.
 *
 */

#ifndef mayaMath_H
#define mayaMath_H

#include <maya/MIOStream.h>
#include <math.h>

#include <algorithm>
// #include <stdlib.h>

#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MAngle.h>

#include <maya/MBoundingBox.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>

#include <maya/MMatrix.h>
#include <maya/MQuaternion.h>
#include <maya/MPointArray.h>

#include <maya/MDynSweptTriangle.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MEulerRotation.h>

#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFloatPoint.h>
#include <maya/MColor.h>


class mayaMath {

public:

	struct Sphere {
		MPoint center;
		double radius;
		MVector change;
	} ;




	enum axis {xAxis, yAxis , zAxis, xAxisNeg, yAxisNeg , zAxisNeg };

	enum RotateOrder {xyz, yzx , zxy, xzy, yxz , zyx };

	enum RotateUnit {kRadians, kDegrees};

	static const double  double_pi  ;
	static const double  single_pi  ;
	static const double  half_pi    ;
	static const double  quarter_pi;

	static MTransformationMatrix::RotationOrder mtmRotOrder(short order);

	static unsigned randCount(double f);

	static MStatus randomPoints( unsigned nPoints, const MVector   &t0_0,
	                             const MVector   &t1_0, const MVector   &t2_0, const MVector   &t0_1,
	                             const MVector   &t1_1, const MVector   &t2_1 , MVectorArray &result, long seedVal = -1) ;

	// static unsigned randomPoints( double f, const MVector &  t0_0,const MVector &  t1_0,const MVector &  t2_0,const MVector &  t0_1,const MVector &  t1_1,const MVector &  t2_1 , MVectorArray &result, long seedVal=-1) ;

	static void randomizeSpeeds(double randomFactor, double speed, MVectorArray &vels);

	static unsigned randomPoints(double f,  const MVector &p1,  const MVector &p2,
	                             const MVector &p3, MVectorArray &result  , long seedVal = -1);

	static unsigned randomPoints(  double f, const MVector &p1,  const MVector &p2,
	                               MVectorArray &result , long seedVal = -1 );

	static unsigned randomPoints(  double f, const MVector &p1,  const MVector &p2,
	                               MVectorArray &pos , MDoubleArray &alpha , long seedVal = -1 );

	static unsigned randomPoints(double f,  const MVector &p0,  const MVector &p1,
	                             const MVector &p2, const MVector &uv0, const MVector &uv1, const MVector &uv2,
	                             MFloatPointArray &result , MFloatArray &Us, MFloatArray &Vs , long seedVal = -1  );

	static unsigned randomPoints(  double f, MPointArray &pos , long seedVal );

	static MStatus sunflower (	int num, float radius, MVectorArray &result, float low,
	                            float high, float interval = 2.399827f, float heightPower = 1.0, float radialPower = 1.0);

	static MStatus spiral (int numSpokes, float pointsPerSpoke, float radius,
	                       float startAngle, float angle, float radialPower, float spokeOffset,
	                       MVectorArray &result);

	// static int intersectRaySphere (const MPoint &p, const MVector &d, const MPoint &center, const double &radius, double &t, MPoint &q) ;


	static bool  testMovingSphereSphere(
	  const mayaMath::Sphere &s0,
	  const mayaMath::Sphere &s1,
	  double &t);



	static bool  intersectRaySphere (
	  const MPoint &p,
	  const MVector &d,
	  mayaMath::Sphere s,
	  double &t,
	  MPoint &q
	) ;



	static MStatus transformPoints( const MMatrix &mat, const double &r, MPointArray &pts) ;

	static MStatus transformPoints(  const MPoint &P,    MVector  X,    double scale,
	                                 const MVectorArray &points,    MVectorArray &result) ;

	static void sphericalSpread(const MVector &dir,  double spread, unsigned count,
	                            MVectorArray &result, long seedVal = -1    );

	static void randomizeLength(double randomSpeed, MVectorArray &vels, long seedVal = -1 );

	static MStatus  worldToBary(const MVector &A,   const MVector &B,   const MVector &C,
	                            const MVector &P,  MVector &bary  ) ;

	static MStatus  worldToBary(const MFloatPoint &A, const MFloatPoint &B,
	                            const MFloatPoint &C,   const MFloatVector &N,   const MFloatPoint &P,
	                            MFloatVector &bary  );

	static bool  pointInTriangle2d(	const float2 &a, const float2 &b, const float2 &c,
	                                const float2 &p,  MFloatVector &bary  ) ;

	static MVector closestPointOnTriangle(const MVector &p, const MVector &a,
	                                      const MVector &b, const MVector &c, MVector &bary);

	static bool baryIntersections( const MVector &bary1,  const MVector &bary2,
	                               MVectorArray &resBarys  ) ;

	static bool triTriIntersection(   const MVector P0,   const MVector P1,
	                                  const MVector P2, const MVector Q0,  const MVector Q1,   const MVector Q2, MPoint &start,
	                                  MPoint &end   );

	static MFloatPoint calcFaceCenter (const MFloatPoint &a, const MFloatPoint &b,
	                                   const MFloatPoint &c, const MFloatPoint &d) ;

	static double  triArea( const MVector &p1,   const MVector &p2, const MVector &p3 );

	static double  triArea(const MVectorArray &v);

	static MVector  triNormal( const MPoint &p1, const MPoint &p2, const MPoint &p3 );

	static double baryToValue( const double a,    const double b,     const double c,
	                           const MVector &bary   );

	static MVector baryToUV(const MDoubleArray &U, const MDoubleArray &V,
	                        const MVector &bary ) ;

	static MVector baryToUV(const MVector &UV0, const MVector &UV1, const MVector &UV2,
	                        const MVector &bary ) ;

	static MVector baryToWorld(   const MVector &A,    const MVector &B,   const MVector &C,
	                              const MVector &bary    ) ;

	static MVector baryToWorld(   const MVectorArray &V,    const MVector &bary   ) ;

	static int  getRegion(const MVector &bary) ;

	static bool lineIntersectsPlane(const MVector &N, const MVector &P,    const MVector &A,
	                                const MVector &B,   MVector &r  ) ;

	static short sign(double v)  ;

	static bool inRange(const double &v) ;

	static MFloatPoint closestPointOnLine(
	  const MFloatPoint &p,
	  const MFloatPoint &a,
	  const MFloatPoint &b,
	  float &t
	);

	static MVector closestPointOnLine(const MVector &p,   const MVector &a,  const MVector &b,
	                                  double &t  );

	static MVector closestPointOnLine(const MVector &A,  const MVector &B, const MVector &P) ;


	static MVector closestPointOnInfiniteLine(const MVector &start, const MVector &direction,
	    const MVector &toPoint);

	static bool validTriangle( const MFloatPoint &A, const MFloatPoint &B,
	                           const MFloatPoint &C )  ;

	static bool validTriangle( const MFloatVector &A, const MFloatVector &B,
	                           const MFloatVector &C ) ;

	static bool spherePlaneIntersection(const MVector &pointOnPlane , const MVector &normal ,
	                                    const MVector &center, double radius) ;

	static bool boxBoxIntersection(const MBoundingBox &b1, const MBoundingBox &b2 );

	static bool sphereBoxIntersection(const MBoundingBox &box, const MVector &center,
	                                  double radius)  ;

	static bool sphereBoxIntersection(const MBoundingBox &box, const MFloatPoint &center,
	                                  float radius)  ;

	static MFloatMatrix matFromAxes(const MVector &P, const MVector &X, const MVector &Y,
	                                const MVector &Z )   ;
	// static MMatrix      matFromAxes(const MVector &P, const MVector &X, const MVector &Y,
	//                                 const MVector &Z )   ;

	static double attenuate(const double &distance, const  double &maxDistance,
	                        const  double &decay) ;

	static MStatus quadInterp( const MFloatVector &x, const MFloatVector &y,
	                           const MFloatArray &t, MFloatArray &result ) ;

	static MMatrix matFromAim(
	  const MVector &trans,
	  const MVector &front,
	  const MVector &up,
	  const MVector &scale,
	  mayaMath::axis frontAxis = mayaMath::xAxis,
	  mayaMath::axis upAxis = mayaMath::yAxis
	) ;


	static MMatrix matFromAim(
	  const MVector &trans,
	  const MVector &front,
	  const MVector &up,
	  mayaMath::axis frontAxis = mayaMath::xAxis,
	  mayaMath::axis upAxis = mayaMath::yAxis
	);

	static	MFloatMatrix matFromAim(
	  const MFloatVector &trans,
	  const MFloatVector &front,
	  const MFloatVector &up,
	  const float &scale,
	  mayaMath::axis frontAxis = mayaMath::xAxis,
	  mayaMath::axis upAxis = mayaMath::yAxis
	);

	static MFloatMatrix matFromAim(
	  const MFloatVector &trans,
	  const MFloatVector &front,
	  const MFloatVector &up,
	  mayaMath::axis frontAxis = mayaMath::xAxis,
	  mayaMath::axis upAxis = mayaMath::yAxis
	);


	static void axesFromPhi(const MVector &phi, MFloatVector &x,  MFloatVector &y,
	                        MFloatVector &z) ;

	static MFloatMatrix matFromPhi(const MVector &trans, const MVector &phi);


	static MMatrix matFromPhi(const MVector &trans, const MVector &phi,
	                          const MVector &scale)   ;

	static MMatrix matFromPhi(const MVector &phi)   ;
	static MVector phiFromMat(const MMatrix &mat);
	static void  posPhiFromMat(const MMatrix &mat, MVector &pos, MVector &phi) ;

	static MVector axisVector(mayaMath::axis a);


	static MVector phiToEuler(
	  const MVector &phi,
	  MEulerRotation::RotationOrder ord,
	  mayaMath::RotateUnit outUnit);

	static MEulerRotation phiToEuler(
	  const MVector &phi,
	  MEulerRotation::RotationOrder ord);



	// static void getPosition(const MMatrix &mat, MFloatPoint &result);

	// static void getPosition(const MMatrix &mat, MPoint &result) ;

	// static void getRotation(const MMatrix &mat,
	//                         MTransformationMatrix::RotationOrder order,
	//                         MAngle::Unit unit, MVector &result)



	///////////////////////////////////////////////
	///////////// collision stuff /////////////////

	static double eval(const double a[4], const double x) ;

	static bool sign_change(const double a, const double b) ;

	static bool equal(const double x, const double y, const double eps) ;
	static bool isZero(const double x, const double eps);



	static double find_root( const double a[4],	double x0, double v0,	double x1, double v1 ) ;

	static  void output_root(	double out[], int &i, const double x, const double lim);

	/// Find the roots of a cubic in the interval [0,lim]. Additionally treat any
	/// point on the curve that evaluates close to zero as a root, even if it
	/// doesn't actually exhibit a sign change (this means we might get more
	/// than 3 outputs). Returns the number of values added to the output array
	static int cubic_roots( const double a[4], double out[5], const double lim);	// a, b, c, d

	static MVector rgb2hcl(const MColor &color );


	static float interp( const MFloatArray &values,const float &param);

	static MFloatMatrix rotationOnly(const MFloatMatrix & rhs);
	static MMatrix rotationOnly(const MMatrix & rhs);

	static void rgbToHsv(
		const MColor &in,
		MColor &result);

	static void hsvToRgb(
		const MColor &in,
		MColor &result);

};


#endif



