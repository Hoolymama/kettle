  /***************************************************************************
                          triData.cpp  -  description
                             -------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

	a class to store triangles in a bounding box for use in a triKdTree.
 ***************************************************************************/




#include "triData.h"
#include "errorMacros.h"

const double TIMESTEP = 1.0f / 24.0f;
const double EPSILON = 0.0000001;

triData::triData(){}




triData::triData( MDynSweptTriangle &t,int id, int parentId,bool includeChange):
	m_v(3),
	m_change(3),
	m_uCoords(3),
	m_vCoords(3) ,
    m_cachedBary(MVector::zero),
    m_cachedPt(MPoint::origin),
	m_hasUVs(true) ,
	m_id(id),
	m_parentId(parentId)
{	
	m_v.set(t.vertex(0,1.0),0);
	m_v.set(t.vertex(1,1.0),1);
	m_v.set(t.vertex(2,1.0),2);
	
	
	m_uCoords.set(t.uvPoint(0).x,0);
	m_uCoords.set(t.uvPoint(1).x,1);
	m_uCoords.set(t.uvPoint(2).x,2);

	m_vCoords.set(t.uvPoint(0).y,0);
	m_vCoords.set(t.uvPoint(1).y,1);
	m_vCoords.set(t.uvPoint(2).y,2);
	
	m_normal = t.normal();
	
	m_change.set((t.vertex(0,1.0) - t.vertex(0,0.0)) ,0); 
	m_change.set((t.vertex(1,1.0) - t.vertex(1,0.0)) ,1); 
	m_change.set((t.vertex(2,1.0) - t.vertex(2,0.0)) ,2); 
	
	computeBoundingBox(includeChange);
	
	m_area = mayaMath::triArea(m_v);
}


triData::triData(
	const MVector &p0, 
	const MVector &p1, 
	const MVector &p2, int faceId, int triangleId):
	m_v(3),
	m_change(3),
	m_uCoords(3),
	m_vCoords(3) ,
    m_cachedBary(MVector::zero),
    m_cachedPt(MPoint::origin),
	m_hasUVs(false) ,
	m_id(triangleId),
	m_parentId(faceId)
{	
	m_v.set(p0,0);
	m_v.set(p1,1);
	m_v.set(p2,2);
	
	m_normal	=((p1-p0)^(p2-p0)).normal();
	
	computeBoundingBox(0);
	
	m_area = mayaMath::triArea(m_v);
}



triData::triData(
	const MVectorArray &lastTriangle,  
	const MVectorArray &triangle,  
	unsigned id
):m_v(3),
	m_change(3),
	m_uCoords(3),
	m_vCoords(3) ,
    m_cachedBary(MVector::zero),
    m_cachedPt(MPoint::origin),
	m_hasUVs(false) ,
	m_id(id),
	m_parentId(0) 
{
	m_v.copy(triangle);

	m_change.set((triangle[0] - lastTriangle[0]) ,0); 
	m_change.set((triangle[1] - lastTriangle[1]) ,1); 
	m_change.set((triangle[2] - lastTriangle[2]) ,2); 

	// cerr << "in triData constructor, avg change = " <<  (m_change[0] + m_change[1] + m_change[2]) * 0.333333333 << endl;

	m_normal	=((triangle[1]-triangle[0])^(triangle[2]-triangle[0])).normal();
	
	computeBoundingBox(1);
	
	m_area = mayaMath::triArea(m_v);
}



triData::~triData(){}

triData& triData::operator=(const triData& other) {
	if (this != &other) {
		m_v = other.m_v;
		m_change = other.m_change;
		m_box = other.m_box;
		m_normal	= other.m_normal;
		m_center	= other.m_center;
		m_uCoords = other.m_uCoords;
		m_vCoords = other.m_vCoords;
		m_cachedBary = other.m_cachedBary;
		m_cachedPt = other.m_cachedPt;
		m_hasUVs = other.m_hasUVs;
		m_id = other.m_id;
		m_parentId = other.m_parentId;
		m_area = other.m_area;
	}
	return *this;
}

// includeChange means make the bounding box contain the triangle now and the future triangle
void triData::computeBoundingBox(bool includeChange){

	m_box = MBoundingBox(m_v[0],m_v[1]);
	m_box.expand(m_v[2]);
	
	if (includeChange) {
		m_box.expand(m_v[0] + m_change[0]);	
		m_box.expand(m_v[1] + m_change[1]);	
		m_box.expand(m_v[2] + m_change[2]);	
	}
	

	if (
	(m_box.width() < EPSILON) ||
	(m_box.height() < EPSILON) ||
	(m_box.depth() < EPSILON) 
	 ){
   	m_box.expand(m_box.max() + MVector(0.0001,0.0001,0.0001));
   	m_box.expand(m_box.min() - MVector(0.0001,0.0001,0.0001));
	}
	
	m_center = m_box.center();
	
}

const MVector & triData::center() const {return m_center;}

double  triData::center(axis a) const {return  m_center[a];}

double  triData::min(axis a) const {return m_box.min()[a];}

double  triData::max(axis a) const {return m_box.max()[a];}

double  triData::area() const {return m_area;}

const int & triData::id() const { return m_id;}

int triData::parentId() const { return m_parentId;}

const int &  triData::triangleId() const { return m_id;}
		
const int &  triData::faceId() const { return m_parentId;}

const MVector & triData::vertex(int i) const {return m_v[i];}

const MVector & triData::change(int i) const {return m_change[i];}

const MVector & triData::normal() const {return m_normal;}


const MBoundingBox & triData::box() const {return m_box;}

const MDoubleArray & triData::uCoords() const {return m_uCoords;}

const MDoubleArray & triData::vCoords() const {return m_vCoords;}


// determine if the given spere intersects this bounding box
bool triData::sphereIntersectsBB(const MVector &c, double r) const  {
	// params are center and radius
	return mayaMath::sphereBoxIntersection( m_box, c, r);
}


// determine if the given sphere intersects the triangle.
// If so, store the point and bary coords of the closest point to the sphere center
// and send back the distance in dist
bool triData::sphereIntersectsTri(const MVector &c, double r, double &dist) {
	// if sphere doesn't touch plane (cheap test), then it doesn't touch triangle (more expensive test)
	if (! mayaMath::spherePlaneIntersection(m_v[0],m_normal,c ,r )) return false;
	// it touches plane so check against triangle
	MVector bary;
	MVector pt  = mayaMath::closestPointOnTriangle(c, m_v[0], m_v[1], m_v[2], bary ) ;
	// cerr << "closestPointOnTriangle to " << c << " is " << pt << endl;
	// is this point within radius ?
	MVector v = pt - c;
	double sqDist = (v*v);
	if ((sqDist) <= (r*r)) {  // bingo
   	dist = sqrt(sqDist) ;
    	m_cachedBary = bary;
      m_cachedPt = pt;
      return true;
	}
	return false;
}

bool triData::sphereIntersectsTri(
	const MVector &c, 
	double r, 
	pointOnTriangleInfo &result
) {
	// if sphere doesn't touch plane (cheap test), then it doesn't touch triangle (more expensive test)
	if (! mayaMath::spherePlaneIntersection(m_v[0],m_normal,c ,r )) return false;
	// it touches plane so check against triangle
	MVector bary;
	MPoint point  = mayaMath::closestPointOnTriangle(c, m_v[0], m_v[1], m_v[2], bary ) ;
	MVector v =  c - point;
	double sqDist = (v*v);
	if ((sqDist) <= (r*r)) {  // bingo
   		result.setPoint(point);
   		result.setNormal(m_normal);
   		result.setBary(bary);
    	result.setDistance(sqrt(sqDist));
    	result.setId(m_id);
    	result.setUCoord(calcU());
    	result.setVCoord(calcV());
    	result.setSide(  ((v * m_normal) > 0.0)  );
      	return true;
	}
	return false;
}

MVector triData::pointAtTime(const double t, const MVector &bary ) const {
	return mayaMath::baryToWorld( (m_v[0] + (m_change[0]*t)),	(m_v[1] + (m_change[1]*t)),	(m_v[2] + (m_change[2]*t)), bary);
}

bool triData::boxIntersects(const MBoundingBox& other) const {
	return m_box.intersects(other);
}

MVector triData::cachedPoint() const  {
	return m_cachedPt;
}

MVector triData::cachedBary() const  {
	return m_cachedBary;
}

double triData::calcU() const {
	 // calculate U value at cached barycentric coords
    return mayaMath::baryToValue(m_uCoords[0],  m_uCoords[1], m_uCoords[2], m_cachedBary)  ;
}
double triData::calcV() const   {
	 // calculate V value at cached barycentric coords
    return mayaMath::baryToValue(m_vCoords[0],  m_vCoords[1], m_vCoords[2], m_cachedBary)  ;
}

MVector triData::calcFaceCenter() const { 
	return (m_v[0] + m_v[1] + m_v[2] ) * 0.3333333333333333;
}

double triData::calcCenterU() const { 
	return (m_uCoords[0] + m_uCoords[1] + m_uCoords[2] ) * 0.3333333333333333;
}
double triData::calcCenterV() const { 
	return (m_vCoords[0] + m_vCoords[1] + m_vCoords[2] ) * 0.3333333333333333;
}


MVector triData::calcFaceCenterChange() const { 
	return (m_change[0] + m_change[1] + m_change[2] ) * 0.3333333333333333;
}


MVector triData::calcVelocity( double fps) const   {
	// calculate direction change over time at cached barycentric coords

	if (0.0 == fps) return MVector::zero;
	return mayaMath::baryToWorld(m_change[0],  m_change[1], m_change[2], m_cachedBary) *fps ;

}

MVector triData::calcVelocity( double fps,const MVector& bary ) const   {
	// calculate direction change over time at cached barycentric coords

	if (0.0 == fps) return MVector::zero;
	return mayaMath::baryToWorld(m_change[0],  m_change[1], m_change[2], bary) * fps ;
}

/*
void triData::getSamplingData(const MVector &bary, double sampleArea, MFloatVectorArray &uv, MFloatPointArray &pts) const {
	
	// generate barycentric coordinates at s and t offsets on this triangle and return the 
	// corresponding three UVs and points
	double sampleFactor = sampleArea / m_area;
	MVector baryS = bary + MVector(-sampleFactor,sampleFactor,0);
	MVector baryT = bary + MVector(-sampleFactor,0,sampleFactor);
	// now we have barys, get point and uv values


	MVector  tmp = baryToWorld(v0,v1,v2,bary);
	pts[0] = MFloatPoint(tmp.x, tmp.y, tmp.z );
   tmp = baryToWorld(v0,v1,v2,baryS);
	pts[1] = MFloatPoint(tmp.x, tmp.y, tmp.z );
   tmp = baryToWorld(v0,v1,v2,baryT);
	pts[2] = MFloatPoint(tmp.x, tmp.y, tmp.z );


	tmp =	baryToUV(m_uCoords,m_vCoords,bary);	
	uv[0] = MFloatVector(tmp);
	tmp =		baryToUV(m_uCoords,m_vCoords,baryS);	
	uv[1] = MFloatVector(tmp);
	tmp =		baryToUV(m_uCoords,m_vCoords,baryT);	
	uv[2] = MFloatVector(tmp);
}

*/


