#if !defined(ATTR_INLINES_H)
#define ATTR_INLINES_H

#include <maya/MPlug.h>
#include <maya/MObject.h>
#include <maya/MGlobal.h>

#include <maya/MDataBlock.h>
#include <maya/MPxNode.h>
#include <maya/MDoubleArray.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MDataHandle.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MPlugArray.h>
#include <maya/MRampAttribute.h>


#include "errorMacros.h"


inline MStatus outputData(const MObject &attribute, MDataBlock &data,
                          const MIntArray &array) {
	MStatus st;
	MDataHandle h = data.outputValue(attribute);
	MFnIntArrayData fn ;
	MObject d  = fn.create( array, &st ); mser;
	h.set(d);
	data.setClean(attribute );
	return st;
}

inline MStatus outputData(const MObject &attribute, MDataBlock &data,
                          const MDoubleArray &array) {
	MStatus st;
	MDataHandle h = data.outputValue(attribute);
	MFnDoubleArrayData fn ;
	MObject d  = fn.create( array, &st ); mser;
	h.set(d);
	data.setClean(attribute );
	return st;
}

inline MStatus outputData(const MObject &attribute, MDataBlock &data,
                          const MVectorArray &array) {
	MStatus st;
	MDataHandle h = data.outputValue(attribute);
	MFnVectorArrayData fn ;
	MObject d  = fn.create( array, &st ); mser;
	h.set(d);
	data.setClean(attribute );
	return st;
}


inline MStatus multipliedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &arrayAtt,
  const MObject &scalarAtt,
  MIntArray &arr
) {

	// generate spans array
	// this version needs a double array, a short and returns an int array
	// mult the double array by the short, then cast result as ints

	MStatus st;
	MDataHandle h = data.inputValue( arrayAtt );
	MObject o = h.data();
	MFnDoubleArrayData fn(o);

	double def = double( data.inputValue(scalarAtt).asShort() );

	arr.setLength(len);

	if (fn.array().length() == len) {
		const MDoubleArray &theArray = fn.array( );
		for (unsigned i = 0; i < len; i++) {
			arr.set(  int(theArray[i]* def )   , i);
		}
	}
	else {
		for (unsigned i = 0; i < len; i++) {
			arr.set( int(def) , i);
		}
	}
	return MS::kSuccess;
}


inline MStatus multipliedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &arrayAtt,
  const MObject &scalarAtt,
  MDoubleArray &arr
) {

	// ,fill in arr with values representing the array in att multiplied by the scalar in attDef
	MStatus st;
	MDataHandle h = data.inputValue( arrayAtt );
	MObject o = h.data();
	MFnDoubleArrayData fn(o);

	double def = data.inputValue(scalarAtt).asDouble();
	if (def == 1.0) {
		if (fn.array().length() == len) {
			arr = fn.array( );
			return MS::kSuccess;
		}
		else {
			arr.setLength(len);
			for (unsigned i = 0; i < len; i++) { arr.set(1.0 , i); }
			return MS::kSuccess;
		}
	}
	else if (def == 0.0) {
		arr.setLength(len);
		for (unsigned i = 0; i < len; i++) { arr.set(0.0 , i); }
		return MS::kSuccess;
	}
	else {
		arr.setLength(len);
		if (fn.array().length() == len) {
			const MDoubleArray &theArray = fn.array( );
			for (unsigned i = 0; i < len; i++) {
				arr.set((theArray[i]* def ) , i);
			}
		}
		else {
			for (unsigned i = 0; i < len; i++) {
				arr.set(( def ) , i);
			}
		}
		return MS::kSuccess;
	}
	return MS::kSuccess;
}



inline MStatus multipliedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &arrayAtt,
  const MObject &scalarAtt,
  MVectorArray &arr
) {

	// ,fill in arr with values representing the array in att multiplied by the scalar in attDef
	MStatus st;
	MDataHandle h = data.inputValue( arrayAtt );
	MObject o = h.data();
	MFnVectorArrayData fn(o);
	MVector def = data.inputValue(scalarAtt).asVector();
	if (def == MVector(1.0, 1.0, 1.0)) {
		if (fn.array().length() == len) {
			arr = fn.array( );
			return MS::kSuccess;
		}
		else {
			arr.setLength(len);
			for (unsigned i = 0; i < len; i++) { arr.set(MVector(1.0, 1.0, 1.0) , i); }
			return MS::kSuccess;
		}
	}
	else if (def == MVector::zero) {
		arr.setLength(len);
		for (unsigned i = 0; i < len; i++) { arr.set(MVector::zero , i); }
		return MS::kSuccess;
	}
	else {
		arr.setLength(len);
		if (fn.array().length() == len) {
			const MVectorArray &theArray = fn.array( );
			for (unsigned i = 0; i < len; i++) {

				arr.set((MVector( theArray[i].x * def.x, theArray[i].y * def.y, theArray[i].z * def.z )) ,
				        i);
			}
		}
		else {
			for (unsigned i = 0; i < len; i++) {
				arr.set(( def ) , i);
			}
		}
		return MS::kSuccess;
	}
	return MS::kSuccess;
}

inline MStatus multipliedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &arrayAtt,
  double mult,
  MVectorArray &arr
)
{
	MStatus st;
	MDataHandle h = data.inputValue( arrayAtt );
	MObject o = h.data();
	MFnVectorArrayData fn(o);
	if (fn.array().length() == len) {
		arr = fn.array( );
		if (mult != 1.0) {
			for (unsigned i = 0; i < len; i++) {
				arr.set(  MVector(arr[i] * mult) , i);
			}
		}
	}
	else {
		arr = MVectorArray(len);
	}
	return MS::kSuccess;
}


inline MStatus arrayFromMultiCompound(
  MDataBlock &data,
  const MObject &arrayAtt,
  int index,
  const MObject &att,
  MDoubleArray &arr
) {
	MStatus st;
	MArrayDataHandle ha = data.inputArrayValue( arrayAtt, &st ); msert;
	st = ha.jumpToElement( index ); msert;
	MDataHandle h = ha.inputValue(&st); msert;
	MDataHandle hc = h.child( att );
	MObject o = hc.data();
	if (o.hasFn(MFn::kDoubleArrayData))  {
		MFnDoubleArrayData fn(o);
		arr = fn.array( &st ); msert;
	}
	else {
		st = MS::kFailure;
	}
	return st;
}


inline MStatus arrayFromMultiCompound(
  MDataBlock &data,
  const MObject &arrayAtt,
  int index,
  const MObject &att,
  MVectorArray &arr
) {
	MStatus st;
	MArrayDataHandle ha = data.inputArrayValue( arrayAtt, &st ); msert;
	st = ha.jumpToElement( index ); msert;
	MDataHandle h = ha.inputValue(&st); msert;
	MDataHandle hc = h.child( att );
	MObject o = hc.data();
	if (o.hasFn(MFn::kVectorArrayData))  {
		MFnVectorArrayData fn(o);
		arr = fn.array( &st ); msert;
	}
	else {
		st = MS::kFailure;
	}
	return st;
}



inline MStatus defaultedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &att,
  const MObject &attDef,
  MVectorArray &arr
) {
	MStatus st;
	MDataHandle h = data.inputValue( att );
	MObject o = h.data();

	MFnVectorArrayData fn(o);
	if (fn.array().length() == len) {
		arr = fn.array( );
		return MS::kSuccess;
	}
	MVector def = data.inputValue(attDef).asVector();
	arr.setLength(len);
	for (unsigned i = 0; i < len; i++) { arr.set(def , i); }
	return MS::kSuccess;
}

inline MStatus defaultedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &att,
  const MVector &attDef,
  MVectorArray &arr
) {
	MStatus st;
	MDataHandle h = data.inputValue( att );
	MObject o = h.data();

	MFnVectorArrayData fn(o);
	if (fn.array().length() == len) {
		arr = fn.array( );
		return MS::kSuccess;
	}
	arr.setLength(len);
	for (unsigned i = 0; i < len; i++) { arr.set(attDef , i); }
	return MS::kSuccess;
}


inline MStatus defaultedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &att,
  const MObject &attDef,
  MDoubleArray &arr
) {
	MStatus st;
	MDataHandle h = data.inputValue( att );
	MObject o = h.data();

	MFnDoubleArrayData fn(o);
	if (fn.array().length() == len) {
		arr = fn.array( );
		return MS::kSuccess;
	}
	double def = data.inputValue(attDef).asDouble();
	arr.setLength(len);
	for (unsigned i = 0; i < len; i++) { arr.set(def , i); }
	return MS::kSuccess;
}

inline MStatus defaultedArray(
  MDataBlock &data,
  unsigned len,
  const MObject &att,
  double defaultVal,
  MDoubleArray &arr
) {
	MStatus st;
	MDataHandle h = data.inputValue( att );
	MObject o = h.data();

	MFnDoubleArrayData fn(o);
	if (fn.array().length() == len) {
		arr = fn.array( );
		return MS::kSuccess;
	}
	arr.setLength(len);
	for (unsigned i = 0; i < len; i++) { arr.set(defaultVal , i); }
	return MS::kSuccess;
}


inline MStatus getAniCurveFn(const MObject &node, const MObject &att,
                             MFnAnimCurve &functionSet) {

	MStatus st;

	MObject aniNode;
	MPlugArray plugArray;

	MPlug curvePlug(node, att);
	if (curvePlug.connectedTo(plugArray, 1, 0)) {
		aniNode = plugArray[0].node(&st); msert;
	}
	else {
		return MS::kFailure;
	}
	st = functionSet.setObject(aniNode); msert;
	return MS::kSuccess;
}

inline MStatus doRampLookup(
  const MObject &node,
  const MObject &attribute,
  const MDoubleArray &in,
  MDoubleArray &results,
  float oldMin,
  float oldMax,
  float newMin,
  float newMax )
{
	MStatus st;
	MRampAttribute rampAttr( node, attribute, &st ); msert;

	unsigned n = in.length();
	results.setLength(n);

	float oldRange = oldMax - oldMin;
	float oneOverOldRange = 1.0f;
	if (oldRange != 0.0) {
		oneOverOldRange = 1.0f / oldRange;
	}
	float newRange = newMax - newMin;

	for ( unsigned i = 0; i < n; i++ )
	{
		float v = (float(in[i]) - oldMin ) * oneOverOldRange;
		float res;
		rampAttr.getValueAtPosition( v, res, &st ); mser;
		res = (res * newRange) + newMin;
		results[i] = double( res ) ;
	}

	return MS::kSuccess;
}


inline MStatus doRampLookup(
  const MObject &node,
  const MObject &attribute,
  const MFloatArray &in,
  MFloatArray &results,
  float oldMin,
  float oldMax,
  float newMin,
  float newMax )
{
	MStatus st;
	MRampAttribute rampAttr( node, attribute, &st ); msert;

	unsigned n = in.length();
	results.setLength(n);

	float oldRange = oldMax - oldMin;
	float oneOverOldRange = 1.0f;
	if (oldRange != 0.0) {
		oneOverOldRange = 1.0f / oldRange;
	}
	float newRange = newMax - newMin;

	for ( unsigned i = 0; i < n; i++ )
	{
		float v = (in[i] - oldMin ) * oneOverOldRange;
		float res;
		rampAttr.getValueAtPosition( v, res, &st ); mser;
		results[i] = (res * newRange) + newMin;
	}

	return MS::kSuccess;
}




inline MVectorArray getVectorArray(
  MObject &node,
  MObject &att,
  unsigned expectedLen,
  MStatus *st) {

	MVectorArray res;
	MObject d;
	MFnVectorArrayData fnV;

	MPlug plug( node, att);
	*st = plug.getValue(d);
	if (*st != MS::kSuccess)  { return res; }
	*st = fnV.setObject(d);
	if (*st != MS::kSuccess)  { return res; }
	if (fnV.length() != expectedLen) {
		*st = MS::kFailure;
		return res;
	}
	return  fnV.array();
}


inline MDoubleArray getDoubleArray(
  MObject &node,
  MObject &att,
  unsigned expectedLen,
  MStatus *st) {

	MDoubleArray res;
	MObject d;
	MFnDoubleArrayData fnD;

	MPlug plug( node, att);
	*st = plug.getValue(d);
	if (*st != MS::kSuccess)  { return res; }
	*st = fnD.setObject(d);
	if (*st != MS::kSuccess)  { return res; }
	if (fnD.length() != expectedLen) {
		*st = MS::kFailure;
		return res;
	}
	return  fnD.array();
}


#endif


