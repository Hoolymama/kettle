#ifndef _lineKdTree
#define _lineKdTree

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MIntArray.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MItMeshPolygon.h>

#include "lineData.h"
#include "errorMacros.h"


class     lineData ;

typedef vector<lineData*> LINE_LIST;


struct lineDataKdNode  {
	bool bucket;							/// is this node a bucket
	axis cutAxis;							/// if not, this axis will divide it 
	double cutVal;							/// at this value
	lineDataKdNode *loChild, *hiChild;		/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	LINE_LIST * overlapList;	/// list of lineDatas whose boumding boxes intersect this bucket

	
};


class lineKdTree 
{
	public:

		lineKdTree();			

		~lineKdTree();  
		const lineDataKdNode * root();
		lineKdTree& operator=(const lineKdTree & otherData );

		void 		build(); 							/// build root from m_perm

		lineDataKdNode * 	build( int low,  int high ); 	/// build children recursively
		int size() ;
		//MStatus 	getSize(int &siz);  			/// size of m_perm and _prismVector - returns failure if they are different

		void  	setMaxPointsPerBucket( int b) {if (b < 1) {m_maxPointsPerBucket = 1;} else {m_maxPointsPerBucket = b;}}

		MStatus 	init();

		MStatus 	populate(const MFnDynSweptGeometryData &g, int id) ;

		void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;

		void makeEmpty() ; 
		
		void makeEmpty(lineDataKdNode * p) ; 

		void  setOverlapList(lineDataKdNode * p,  lineData * sph  );
		
		LINE_LIST & lineList();

  	void closestLine(const lineDataKdNode * p,const MPoint &searchPoint,  double & radius,  lineData &  result )  const ;
	
	private:	
		axis 		findMaxAxis(const  int low,const  int  high) const;
	void  searchList(const LINE_LIST * overlapList,const MPoint &searchPoint, double & radius, lineData & result) const   ;
		LINE_LIST * m_perm;				/// pointer to (permutations) - a list of pointers to elements of _lineDataVector
		lineDataKdNode * m_pRoot;							/// pointer to root of tree
		 int m_maxPointsPerBucket;				/// at build time keep recursing until no bucket holds more than this
		
} ;

#endif


