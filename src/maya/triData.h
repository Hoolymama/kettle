#ifndef triData_H
#define triData_H

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDynSweptTriangle.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>

#include <maya/MVector.h>
#include <maya/MDoubleArray.h>
#include <vector>

#include "pointOnTriangleInfo.h"
#include "errorMacros.h"
#include "mayaMath.h"


typedef mayaMath::axis axis;

using namespace std;


class triData{
	public:
		triData();
		
		// triData( MDynSweptTriangle &t, int id);
		triData( MDynSweptTriangle &t, int id, int parentId, bool includeChange=false);
		
		triData (const MVector &p0, const MVector &p1, const MVector &p2, int faceId, int triangleId);
		triData( const MVectorArray &lastTriangle,   const MVectorArray &triangle,   unsigned id );

		~triData();
		
		triData& operator=(const triData& other) ;

		const MVector & center() const;
		
		double center(axis a) const ;

		double min(axis a) const ;
		
		double max(axis a) const ;

		double area() const ;
		
		const int &  id() const;
		
		int parentId() const;
		
		const int &  triangleId() const;
		
		const int &  faceId() const;

		const MBoundingBox & box() const ;
		
		const MVector & vertex(int i) const ;
		
		const MVector & change(int i) const ;

		const MVector & normal() const;

		const MDoubleArray & uCoords() const ;
		
		const MDoubleArray & vCoords() const ;
		
    	bool sphereIntersectsBB(const MVector &c,   double r) const  ;
		
    	bool sphereIntersectsTri(const MVector &c,  double r, double &dist) ;
	
		bool sphereIntersectsTri(const MVector &c, double r, pointOnTriangleInfo &result); 

		bool boxIntersects(const MBoundingBox& other) const ;
				
		MVector cachedPoint() const;
		
		MVector cachedBary() const;
		
		double calcU() const;
		
		double calcV() const;
		
		double calcCenterU() const ;
		
		double calcCenterV() const ;
		
		MVector calcFaceCenter() const;

		MVector calcFaceCenterChange() const;
		
		
		MVector calcVelocity( double fps) const;

		MVector calcVelocity( double fps,const MVector& bary ) const;
		
		MVector pointAtTime(const double t, const MVector &bary ) const ;
	private:

 		void computeBoundingBox(bool includeChange=false);
   	
		MVectorArray m_v; 
		
		MVectorArray m_change; 
		
		MVector m_normal;
		
		MVector m_center;
		
		MBoundingBox m_box;
		
		MDoubleArray m_uCoords;
		
		MDoubleArray m_vCoords;
		
		MVector m_cachedBary;
		
		MVector m_cachedPt;
		
		bool m_hasUVs;
		
		int m_id;
		
		int m_parentId;
		
		double m_area;
		
};

#endif

