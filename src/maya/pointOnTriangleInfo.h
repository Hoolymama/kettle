#ifndef pointOnTriangleInfo_H
#define pointOnTriangleInfo_H

#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPoint.h>
#include <maya/MVector.h>

#include "errorMacros.h"
#include "mayaMath.h"

using namespace std;


class pointOnTriangleInfo{
	public:
		pointOnTriangleInfo();


		~pointOnTriangleInfo();


		const MPoint & point() const ;
		const MVector & normal() const ;
		const double & 	distance() const ;
		const double & 	uCoord() const ;
		const double & 	vCoord() const ;
		const double & 	baryU() const ;
		const double & 	baryV() const ;
		const int & 	id() const ;
		const int & 	parentId() const ;
		bool side() const ;


		void 	setPoint(const MPoint & rhs) ;
		void 	setNormal(const MVector & rhs) ;
		void 	setDistance(const double & rhs) ;
		void 	setUCoord(const double & rhs) ;
		void 	setVCoord(const double & rhs) ;
		void 	setBary(const MVector & rhs) ;
		void 	setId(const int & rhs) ;
		void 	setParentId(const int & rhs) ;
		void 	setSide(bool rhs) ;


	private:

		MPoint m_point;

		MVector m_normal;

		double m_distance;

		double m_uCoord;
		
		double m_vCoord;
		
		double m_baryU;
		
		double m_baryV;
		
		int m_id;
		
		int m_parentId;	

		bool  m_side;

};

#endif

