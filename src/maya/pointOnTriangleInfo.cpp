#include "pointOnTriangleInfo.h"
#include "errorMacros.h"

	// const double TIMESTEP = 1.0f / 24.0f;
	// const double EPSILON = 0.0000001;

	pointOnTriangleInfo::pointOnTriangleInfo():m_id(-1){}

	pointOnTriangleInfo::~pointOnTriangleInfo() {}

	void 	pointOnTriangleInfo::setPoint(const MPoint & rhs) {m_point = rhs;}
	void 	pointOnTriangleInfo::setNormal(const MVector & rhs) {m_normal = rhs;} 
	void 	pointOnTriangleInfo::setDistance(const double & rhs) {m_distance = rhs;} 
	void 	pointOnTriangleInfo::setUCoord(const double & rhs) {m_uCoord = rhs;} 
	void 	pointOnTriangleInfo::setVCoord(const double & rhs) {m_vCoord = rhs;} 
	void 	pointOnTriangleInfo::setBary(const MVector & rhs) {m_baryU = rhs.x;m_baryV = rhs.y;} 
	void 	pointOnTriangleInfo::setId(const int & rhs) {m_id = rhs;} 
	void 	pointOnTriangleInfo::setParentId(const int & rhs) {m_parentId = rhs;} 
	void 	pointOnTriangleInfo::setSide(bool rhs) {m_side = rhs;} 

	const MPoint &  pointOnTriangleInfo::point() const {return m_point; }
	const MVector & pointOnTriangleInfo::normal() const {return m_normal; }
	const double & 	pointOnTriangleInfo::distance() const {return m_distance; }
	const double & 	pointOnTriangleInfo::uCoord() const {return m_uCoord; }
	const double & 	pointOnTriangleInfo::vCoord() const {return m_vCoord; }
	const double & 	pointOnTriangleInfo::baryU() const {return m_baryU; }
	const double & 	pointOnTriangleInfo::baryV() const {return m_baryV; }
	const int & 	pointOnTriangleInfo::id() const {return m_id; }
	const int & 	pointOnTriangleInfo::parentId() const {return m_parentId; }
	bool pointOnTriangleInfo::side() const {return m_side; }


