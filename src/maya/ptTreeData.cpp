/***************************************************************************
                          ptTreeData.cpp  -  description
                             -------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    this is the maya data object containing a ptKdTree so that it can be passed
    through the dependency graph.
    It also contains some methods to make calls to the ptKdTree methods
 ***************************************************************************/


//
// Author: Julian Mann
//
#include <maya/MFnVectorArrayData.h>
#include <maya/MDataHandle.h>
#include "ptTreeData.h"
#include "errorMacros.h"
#include "jMayaIds.h"

#define EPSILON 0.0000001

const double bigNum = 99999999999.0  ;

MTypeId ptTreeData::id( k_ptTreeData );
MString ptTreeData::typeName( "ptTreeData" );

ptTreeData::ptTreeData()
{
	m_pTree = 0;
}

ptTreeData::~ptTreeData()
{
}

void *ptTreeData::creator()
{
	return new ptTreeData;
}

// the ptKdTree
ptKdTree	*ptTreeData::tree() const {return m_pTree;}

// clean up
void	ptTreeData::clear() {
	if (m_pTree) {delete m_pTree; m_pTree = 0;}
}

// create new tree
void ptTreeData::create(MDataHandle &h, int splitFunc) {

	MStatus st = MS::kSuccess;
	MString method("ptTreeData::create" );

	// clean up old stuff
	clear();

	// make a new tree
	m_pTree = new ptKdTree();
	if (m_pTree)  {
		MObject  d = h.data();
		MFnVectorArrayData fnV(d, &st);
		if (!(st.error())) {
			const MVectorArray &points = fnV.array(&st);
			if (!(st.error())) {
				m_pTree->populate(points);
				// if splitFunc is 1 (cycleAxis), forceAxis will be 0 (firstAxis)
				// if splitFunc is 0 (longestAxis), forceAxis will be -1 (dont force, just use findMaxAxis)
				int *pForceAxis = 0;
				if (splitFunc == 1) {
					pForceAxis = new int(0);
				}
				m_pTree->build(pForceAxis);
				if (pForceAxis) {delete pForceAxis; pForceAxis = 0;}
				//m_pTree->build();
			}
		}
	}
	// NOTE: there is a ptKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree

}
// create new tree
void ptTreeData::create(MDataHandle &hP, MDataHandle &hV, MDataHandle &hN,
                        int splitFunc) {

	MStatus st = MS::kSuccess;
	MString method("ptTreeData::create" );

	// clean up old stuff
	clear();

	// make a new tree
	m_pTree = new ptKdTree();
	if (m_pTree)  {

		const MVectorArray &points = 	MFnVectorArrayData(hP.data()).array();
		const MVectorArray &vels = 	MFnVectorArrayData(hV.data()).array();
		const MVectorArray &norms = 	MFnVectorArrayData(hN.data()).array();
		unsigned pl = points.length();
		unsigned vl = vels.length();
		unsigned nl = norms.length();


		if (pl) {
			if (pl == vl) {
				if (pl == nl) {
					m_pTree->populate(points, vels, norms);
				}
				else {
					m_pTree->populate(points, vels);
				}
			}
			else {
				if (pl == nl) {
					MVectorArray tmpVel(pl);
					m_pTree->populate(points, tmpVel, norms);
				}
				else {
					cerr << "Populating Tree With Points" << endl;
					m_pTree->populate(points);
				}
			}
			// if splitFunc is 1 (cycleAxis), forceAxis will be 0 (firstAxis)
			// if splitFunc is 0 (lingestAxis), forceAxis will be -1 (dont force, just use findMaxAxis)
			int *pForceAxis = 0;
			if (splitFunc == 1) {
				pForceAxis = new int(0);
			}
			m_pTree->build(pForceAxis);
			if (pForceAxis) {delete pForceAxis; pForceAxis = 0;}
		}
	}
	// NOTE: there is now a ptKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree

}



int ptTreeData::size() {
	if (m_pTree)
	{ return m_pTree->size(); }
	else
	{ return 0; }
}

MStatus ptTreeData::closestDist(
  const MVector &searchPoint,
  double &result
) const {

	MStatus st = MS::kSuccess;
	MString method("ptTreeData::closestPoint" );

	result = bigNum;
	m_pTree->closestDist( m_pTree->root(), searchPoint  , result);

	if (result < bigNum) {  // we found a point
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

// This is useful for creating a voronoi pattern
MStatus ptTreeData::closest2Dists(
  const MVector &searchPoint,
  MDoubleArray &result
) const {

	MStatus st = MS::kSuccess;
	MString method("ptTreeData::closestPoint" );


	result[0] = bigNum;
	result[1] = bigNum;

	m_pTree->closest2Dists( m_pTree->root(), searchPoint, result);
	if (result[1] < bigNum) {  // we found a point
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

MStatus ptTreeData::closest2Pts(
  const MVector &searchPoint,
  MVectorArray &result
) const {

	MStatus st = MS::kSuccess;
	MString method("ptTreeData::closestPoint" );

	MDoubleArray dists(2, bigNum) ;
	m_pTree->closest2Pts( m_pTree->root(), searchPoint, dists, result);
	if (dists[1] < bigNum) {  // we found a point
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}
/*
void ptTreeData::closestNPoints(const MVector &searchPoint,int numPts,MVectorArray &pts, MDoubleArray &dists)  const {
    KNN_QUEUE q;
    if (numPts > 0 ) {
       for (int i = 0;i<numPts;i++) {
          knnSearchData k;
          k.dist = bigNum;
          q.push(k);
   	 }
       m_pTree->closestNPts( m_pTree->root(),searchPoint, q);
       while (q.size()) {
   		 if (q.top().dist < bigNum) {
      		 pts.append(q.top().point);
       		 dists.append(q.top().dist);
          }
          q.pop();
   	 }
    }
}
*/

void ptTreeData::copy(const MPxData &otherData)
{
	m_pTree = ((const ptTreeData &)otherData).tree();
}

ptTreeData &ptTreeData::operator=(const ptTreeData &otherData ) {
	if (this != &otherData ) {
		m_pTree = otherData.tree();
	}
	return *this;
}


