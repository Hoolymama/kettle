/***************************************************************************
triTreeData.h  -  description
-------------------
begin                : Fri Mar 31 2006
copyright            : (C) 2006 by Julian Mann
email                : julian.mann@gmail.com
***************************************************************************/

#ifndef _triTreeData
#define _triTreeData

//
// Copyright (C) 2006 hoolyMama
//
// Author: Julian Mann

#include <maya/MIOStream.h>
#include <string>
#include <vector>
#include <map>

#include <math.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MVector.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MObject.h>
#include <maya/MPxData.h>
#include <maya/MTypeId.h>

#include "triKdTree.h"


//typedef std::vector<triData*> SOUP;

class triTreeData : public MPxData
{
public:

	triTreeData();

	virtual	~triTreeData();

	static	void*		creator();

	virtual	void		copy(const MPxData&);

	triTreeData& operator=(const triTreeData &);

	// type
	virtual	MTypeId		typeId() const {return id;}
	static	MTypeId		id;


	virtual	MString		name() const {return typeName;}
	static	MString		typeName;
	
	MStatus create(
		const MPointArray & lastPoints,
		const MPointArray & points,
		const MIntArray & triangleVertices
		);

	MStatus	create(	MArrayDataHandle & ah);
	void clear();

		
	triKdTree*	tree() const;

	MStatus closestPointOnTri(
		const MVector &searchPoint,
		MVector &resultPoint,
		MVector &resultUV,
		MVector &resultBary
		) const ;

	MStatus closestPointOnTri(
		const MVector &searchPoint,
		MVector &resultPoint
		) const ;

	MStatus closestPointsOnTri(
		const MVectorArray &searchPoints,
		MVectorArray &resultPoints,
		MVectorArray &resultUVs,
		MIntArray &resultIds,
		MVectorArray &resultBarys
		) const ;

	MStatus appendClosestPointsOnTri(
		const MVectorArray &searchPoints,
		MVectorArray &resultPoints,
		MVectorArray &resultUVs,
		MIntArray &resultIds,
		MVectorArray &resultBarys
		) const ;


	MStatus closestPointsOnTri(
		const MVectorArray &searchPoints,
		MVectorArray &resultPoints
		) const ;

	MStatus closestDistancesToTriPlane(
		const MVectorArray &searchPoints,
		MFloatVectorArray &resultBarys,
		MFloatArray &resultDistances,
		MIntArray &resultIds,
		MIntArray &resultParentIds
		) const ;

	MStatus closestPointsOnTriWithinRadius(
		const MFloatVectorArray &searchPoints,
		double initialRad,
		MIntArray &resultMask, 
		MFloatVectorArray &resultPoints
		) const;


	MStatus snapToClosestPointsOnTri(MVectorArray &points) const ;
	MStatus snapToClosestPointsOnTri(MVectorArray &points, MVectorArray &normals) const;
	int size();

private:

	triKdTree 	*	m_pTree;  // this is the pointer to the currentTime in the map
	
	// triKdTreeCacheMap 	m_treeCacheMap; // this is a map of pointers to trees, keyed by the MTime they represent

};

#endif


