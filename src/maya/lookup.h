/*
 *  lookup.h
 *  jTools
 *
 *  Created by Julian Mann on 14/11/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#ifndef _Lookup
#define _Lookup

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MFloatArray.h>
#include <maya/MFnAnimCurve.h>
// #include <maya/MRampAttribute.h>

class lookup {
public:
	
	
//	lookup( );
	
	
	lookup( const MFnAnimCurve &fn,
			  int res,
			  bool useMin=false,
			  bool useMax=false,				
			  float min=0.0f,
			  float max=1.0f,
			  float mult =1.0);
	

	
	
	lookup( float constantVal=1.0f) ;
		
	
	
	void create( const MFnAnimCurve &fn,
			  int res,
			  bool useMin=false,
			  bool useMax=false,				
			  float min=0.0f,
			  float max=1.0f,
			  float mult =1.0  );

	float evaluate(float t, bool smoothInterp=true) const;
	
	
	const float & minKey() const ;
	
	const float & maxKey() const ;
	
	float  findMinVal() const ;
	
	float  findMaxVal() const ;

	bool isConstantOne() const;
	
private:
		
	
	float m_min;
	
	float m_max;
	
	float m_slicesPerUnit;
	
	MFloatArray m_values;
	
	bool m_isConstantOne;
};
#endif
