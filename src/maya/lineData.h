#ifndef lineData_H
#define lineData_H

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDynSweptLine.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>

#include <maya/MVector.h>
#include <maya/MDoubleArray.h>
#include <vector>

#include "errorMacros.h"
#include "mayaMath.h"

using namespace std;

typedef mayaMath::axis axis;
 
class lineData{
public:
	lineData();
	lineData( MDynSweptLine &l,double uStart, double uEnd, int id, double dt=0.041666);
	
	~lineData();
	
	lineData& operator=(const lineData& other);
	
	const MVector & center() const;
	double center(axis a) const ;
	
	double min(axis a) const ;
	double max(axis a) const ;
	
	//double area() const ;
	int id() const;

	bool sphereIntersectsBB(const MVector &c,   double r) const  ;
	bool sphereIntersectsLine(const MVector &c,  double r, double &dist) ;
	
	const MBoundingBox & box() const ;
	const MVector & vertex(int i) const ;
	const MVector & velocity(int i) const ;
   	const MDoubleArray & uCoords() const ;
	const MVector & cachedPoint() const;
	const MVector & tangent() const;
	const double & cachedParam() const;
	const double & cachedDist() const;
	const double & length() const;
	double calcU() const ; // calculates the U value at the cached point
	MVector calcVelocity() const ; // calculates the velocity at the cached point
private:
		
 	void computeBoundingBox();
   	
	MVectorArray m_p; // start and end points
	MVectorArray m_v; // start and end velocities
	
	// MVector m_end; 
	MBoundingBox m_box; 
	MVector m_center;
	MDoubleArray m_uCoords; // uCoords of start and end
	double m_cachedParam;
	double m_cachedDist;
	MVector m_cachedPt;
	MVector m_tangent;
	int m_id;
	double m_length;
};

#endif

