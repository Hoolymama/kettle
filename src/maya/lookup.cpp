/*
 *  lookup.cpp
 *  jTools
 *
 *  Created by Julian Mann on 14/11/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */


#include "lookup.h"
#include "errorMacros.h"
#include <maya/MString.h>

// lookup::lookup() : m_min(0.0f), m_max(1.0f) ,m_slicesPerUnit(1.0f), m_values(2) , m_isConstantOne(true)
// {
// 	m_values.set(1.0f, 0);
// 	m_values.set(1.0f, 1);
// 	
// }
// 


lookup::lookup( const MFnAnimCurve &fn,
					int res,
					bool useMin,
					bool useMax,				
					float min,
					float max,
					float mult)
{
	create(fn, res , useMin,useMax, min, max, mult );
}


lookup::lookup( float constantVal) : m_min(0.0f), m_max(1.0f) ,m_slicesPerUnit(1.0f), m_values(2) 
{
	m_isConstantOne = (constantVal == 1.0f);
	m_values.set(constantVal, 0);
	m_values.set(constantVal, 1);
}



void lookup::create( const MFnAnimCurve &fn,
					int res,
					bool useMin,
					bool useMax,				
					float min,
					float max,
					float mult)
{
	
	if (res < 1) res = 1;
	// min max
	int numKeys = fn.numKeys();
	if (useMin) {m_min = min;} else {m_min = float(fn.unitlessInput(0));}
	if (useMax) {m_max = max;} else {m_max = float(fn.unitlessInput(numKeys -1));}
	if (m_max < m_min) {
		float tmp = m_max;
		m_max = m_min;
		m_min = tmp;
	}
	
	float slice =  (m_max - m_min) / float(res);
	m_slicesPerUnit = 1.0f / slice;
	
	double v;
	m_values.setLength(res+1);
	if (mult == 1.0) {
		for (int i = 0; i <= res; i++) {
			fn.evaluate( double(m_min+(slice*i)) ,v);
			m_values.set(float(v), i);
		}	
	} else {
		for (int i = 0; i <= res; i++) {
			fn.evaluate( double(m_min+(slice*i)) ,v);
			m_values.set(float(v * mult), i);
		}
	}
	m_isConstantOne = false;
}


float lookup::evaluate(float t, bool smoothInterp) const {
	//cerr << "*********** t " << t << endl;
	if (m_isConstantOne)  return 1.0f;
	if (!(m_values.length())) return 0.0f;
//	cerr << "values length: " << m_values.length() << endl;
//	cerr << "m_slicesPerUnit: " << m_slicesPerUnit << endl;
	if (t<=m_min) return m_values[0];
	if (t>=m_max) return m_values[(m_values.length()-1)];
//	cerr << "m_min " << m_min << "m_max " << m_max << endl; 
	t = (t-m_min) * m_slicesPerUnit;
	
	int a = int(t);
//	if (a >= m_values.length()) cerr  << "a : " << a << " - m_values.length() : " <<m_values.length() << endl;
	//cerr << "a index: " << a << endl;
	if (!smoothInterp) {
		return m_values[a];
	} 
	int b = a+1;
	float result =  ((t- float(a))*m_values[b]) + ((float(b) -t)* m_values[a]);

	return result;
}

float  lookup::findMinVal() const {
	unsigned n = m_values.length();
	float result = m_values[0];
	for (unsigned i=0;i<n;i++) {
		if (m_values[n] < result) {
			result = m_values[n];
		}
	}
	return result;
}

float  lookup::findMaxVal() const {
	unsigned n = m_values.length();
	float result = m_values[0];
	for (unsigned i=0;i<n;i++) {
		if (m_values[n] > result) {
			result = m_values[n];
		}
	}
	return result;
}

const float & lookup::minKey() const {return m_min;}
const float & lookup::maxKey() const {return m_max;}
bool  lookup::isConstantOne() const {return m_isConstantOne;}
