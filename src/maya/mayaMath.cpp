/*
 *  mayaMath.cpp
 *  jtools
 *
 *  Created by Julian Mann on 06/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */
#include <algorithm>    // std::max

#include "mayaMath.h"

//
// #ifdef WIN32
// double drand48()
// {
// 	return (double)rand()/(double)RAND_MAX;
// }
// void srand48(long seed)
// {
// 	srand(seed);
// }
// #endif


const double  mayaMath::double_pi =  6.28318530718 ;
const double  mayaMath::single_pi  = 3.14159265359 ;
const double  mayaMath::half_pi    = 1.57079632679 ;
const double  mayaMath::quarter_pi = 0.785398163397;
const double epsilon = 0.00000001;

// generate random points in the volume of a swept triangle

MTransformationMatrix::RotationOrder mayaMath::mtmRotOrder(short order) {
	MTransformationMatrix::RotationOrder ord;
	switch ( order ) {
		case mayaMath::xyz:
			ord = MTransformationMatrix::kXYZ; break;
		case mayaMath::yzx:
			ord = MTransformationMatrix::kYZX; break;
		case mayaMath::zxy:
			ord = MTransformationMatrix::kZXY; break;
		case mayaMath::xzy:
			ord = MTransformationMatrix::kXZY; break;
		case mayaMath::yxz:
			ord = MTransformationMatrix::kYXZ; break;
		case mayaMath::zyx:
			ord = MTransformationMatrix::kZYX; break;
		default:
			ord = MTransformationMatrix::kInvalid; break;
	}
	return ord;
}

unsigned  mayaMath::randCount(double f) {
	int n = int(f);
	n += drand48() < (f - double(n)) ? 1 : 0;
	return (n > 0) ? unsigned(n) : 0;
}

void mayaMath::randomizeSpeeds(double randomFactor, double speed, MVectorArray &vels) {
	unsigned int n = vels.length();
	unsigned int i;
	for (i = 0; i < n; i++) {
		MVector &v = vels[i]; // operate on array element by reference
		double vl = v.length() * speed;
		v = v.normal() * (vl + ((drand48() - 0.5) * randomFactor));
	}
}
MStatus mayaMath::randomPoints(
  unsigned nPoints,
  const MVector   &t0_0,
  const MVector   &t1_0,
  const MVector   &t2_0,
  const MVector   &t0_1,
  const MVector   &t1_1,
  const MVector   &t2_1 ,
  MVectorArray &result, long seedVal)
{

	if (seedVal >= 0) { srand48(seedVal); }

	double s, t, sweep, oneMinusSweep;
	MVector sweepVertex1, sweepVertex2, sweepVertex3;


	for (unsigned int n = 0; n < nPoints; n++) {
		s = drand48();
		t = drand48();
		if ((s + t) > 1) {
			s = 1.0 - s;
			t = 1.0 - t;
		}
		sweep = drand48();
		oneMinusSweep = 1.0 - sweep;
		double w = (1 - (s + t));

		sweepVertex1 = (( t0_1 * sweep) + (t0_0 * oneMinusSweep));
		sweepVertex2 = (( t1_1 * sweep) + (t1_0 * oneMinusSweep));
		sweepVertex3 = (( t2_1 * sweep) + (t2_0 * oneMinusSweep));


		result.append( (sweepVertex1 * w)  + (sweepVertex2 * s) +  (sweepVertex3 * t) );
	}

	return   MS::kSuccess;
}
//unsigned mayaMath::randomPoints(
//					  double f,
//					  const MVector &  t0_0,
//					  const MVector &  t1_0,
//					  const MVector &  t2_0,
//					  const MVector &  t0_1,
//					  const MVector &  t1_1,
//					  const MVector &  t2_1 ,
//					  MVectorArray &result, long seedVal)
//{
//
//	if (seedVal >=0) srand48(seedVal);
//
//	double s, t, sweep, oneMinusSweep;
//	MVector sweepVertex1, sweepVertex2, sweepVertex3;
//
//	unsigned int nPoints = int(f);
//	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
//
//	for(unsigned int n = 0; n < nPoints; n++){
//		s = drand48();
//		t = drand48();
//		if ((s + t) > 1) {
//			s = 1.0 - s;
//			t = 1.0 - t;
//		}
//		sweep = drand48();
//		oneMinusSweep = 1.0 - sweep;
//		double w = (1-(s+t));
//
//		sweepVertex1 = (( t0_1*sweep) + (t0_0*oneMinusSweep));
//		sweepVertex2 = (( t1_1*sweep) + (t1_0*oneMinusSweep));
//		sweepVertex3 = (( t2_1*sweep) + (t2_0*oneMinusSweep));
//
//
//		result.append( (sweepVertex1*w)  + (sweepVertex2*s) +  (sweepVertex3*t) );
//	}
//
//	return nPoints;
//}
unsigned mayaMath::randomPoints(double f,  const MVector &p1,  const MVector &p2,
                                const MVector &p3, MVectorArray &result , long seedVal )
{
	if (seedVal >= 0) { srand48(seedVal); }
	double s, t;
	int nPoints = int(f);
	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
	if (nPoints > 0) {
		for ( int n = 0; n < nPoints; n++) {
			s = drand48();
			t = drand48();
			if ((s + t) > 1) {
				s = 1.0 - s;
				t = 1.0 - t;
			}
			result.append(  (p1 * (1 - (s + t)))  + (p2 * s) +  (p3 * t)  );
		}
	}
	return nPoints;
}

unsigned mayaMath::randomPoints(
  double f,
  const MVector &p0,
  const MVector &p1,
  const MVector &p2,
  const MVector &uv0,
  const MVector &uv1,
  const MVector &uv2,
  MFloatPointArray &result ,
  MFloatArray &Us,
  MFloatArray &Vs ,
  long seedVal )
{
	if (seedVal >= 0) { srand48(seedVal); }
	double s, t, w;
	int nPoints = int(f);
	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
	if (nPoints > 0) {
		for ( int n = 0; n < nPoints; n++) {
			s = drand48();
			t = drand48();
			if ((s + t) > 1) {
				s = 1.0 - s;
				t = 1.0 - t;
			}
			w = (1.0 - (s + t));

			MVector vec = ((p0 * w)  + (p1 * s) +  (p2 * t) );

			result.append(  MFloatPoint( float(vec.x) , float(vec.y) , float(vec.z)  )  );
			Us.append(  float(       (uv0.x * w)  + (uv1.x * s) +  (uv2.x * t)    ));
			Vs.append(  float(       (uv0.y * w)  + (uv1.y * s) +  (uv2.y * t)    ));
		}
	}
	return nPoints;
}

unsigned mayaMath::randomPoints(  double f, const MVector &p1,  const MVector &p2,
                                  MVectorArray &result , long seedVal )
{
	if (seedVal >= 0) { srand48(seedVal); }

	unsigned int nPoints = int(f);
	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
	for (unsigned int n = 0; n < nPoints; n++) {
		double sweep = ( (double)n + drand48() ) / (double)nPoints;

		result.append((p1 * (1 - (sweep)))  + (p2 * sweep)  );
	}
	return nPoints;
}

unsigned mayaMath::randomPoints(  double f, const MVector &p1,  const MVector &p2,
                                  MVectorArray &pos , MDoubleArray &alpha , long seedVal )
{
	if (seedVal >= 0) { srand48(seedVal); }

	unsigned int nPoints = int(f);
	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
	for (unsigned int n = 0; n < nPoints; n++) {
		double sweep = ( (double)n + drand48() ) / (double)nPoints;

		pos.append((p1 * (1 - (sweep)))  + (p2 * sweep)  );
		alpha.append(sweep );
	}
	return nPoints;
}
/*in a unit 2d quare*/
unsigned mayaMath::randomPoints(  double f, MPointArray &pos , long seedVal )
{
	if (seedVal >= 0) { srand48(seedVal); }

	unsigned int nPoints = int(f);
	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;
	for (unsigned int n = 0; n < nPoints; n++) {
		pos.append(  drand48(), drand48(), 0.0);
	}
	return nPoints;
}

//(newVec,spread, nPoints, velocities,seedVal);

void mayaMath::sphericalSpread(
  const MVector &dir,
  double spread,
  unsigned count,
  MVectorArray &result,
  long seedVal
) {
	if (seedVal >= 0) { srand48(seedVal); }
	const double  PI  = 3.1415927;
	const double  _2PI = 2.0 * PI;

	MQuaternion q(MVector::zAxis, dir);
	double vl = dir.length();
	double min = cos(spread * PI);
	double t, w, z;
	double range = 1.0 - min;
	MVector v;
	for (; count; count--) {
		z = (drand48() * range) + min;

		t = drand48() * _2PI;
		w = (sqrt( 1.0 - z * z )) * vl;
		v.z = z * vl;
		v.x = w * cos( t );
		v.y = w * sin( t );
		result.append(v.rotateBy(q));
	}
}





void mayaMath::randomizeLength(double randomSpeed, MVectorArray &vels, long seedVal ) {
	if (seedVal >= 0) { srand48(seedVal); }

	unsigned n = vels.length();

	for (unsigned i = 0; i < n; i++) {
		double randFactor = ((drand48() - 0.5) * randomSpeed) + 1.0;
		MVector &v = vels[i];
		v = v * randFactor;
	}
}

MStatus mayaMath::transformPoints(
  const MMatrix &mat,
  const double &r,
  MPointArray &pts
)  {
	unsigned n = pts.length();
	for (unsigned i = 0; i < n; ++i)  { pts[i] = (pts[i]  * r) * mat;}
	return MS::kSuccess;
}

MStatus mayaMath::transformPoints(
  const MPoint &P,
  MVector  X,
  double scale,
  const MVectorArray &points,
  MVectorArray &result)
{
	// make a matrix
	if (X.isEquivalent(MVector::zero)) { return MS::kFailure; }
	X.normalize();
	MVector Y = MVector::yAxis;
	if  ( Y.isParallel(X) )  { Y = MVector::xAxis; }
	MVector Z = (X ^ Y).normal(); // cross prod
	Y = (Z ^ X).normal(); // re-align y to make orthonormal axes

	MMatrix mat;

	mat[0][0] = (X.x * scale);	mat[0][1] = X.y;	mat[0][2] = X.z;	mat[0][3] = 0;
	mat[1][0] = Y.x;	mat[1][1] = (Y.y * scale);	mat[1][2] = Y.z;	mat[1][3] = 0;
	mat[2][0] = Z.x;	mat[2][1] = Z.y;	mat[2][2] = (Z.z * scale);	mat[2][3] = 0;
	mat[3][0] = P.x;	mat[3][1] = P.y;	mat[3][2] = P.z;	mat[3][3] = 1.0;

	// now transform all points
	for (unsigned int i = 0; i < points.length(); i++) {
		result.append(points[i] * mat);
	}
	return MS::kSuccess;
}



// generate sunflower spiral
MStatus mayaMath::sunflower (
  int num,
  float radius,
  MVectorArray &result,
  float low,
  float high,
  float interval,
  float heightPower,
  float radialPower

) {
	// If user didn't supply a valid radius or count, then bail out
	if (!(num && radius)) { return MS::kUnknownParameter; }
	float r, theta;

	// get the amount of radius to increment by.
	radius =  radius / sqrt(float(num));

	// high and low are just stretching it out in Z to make a cone rather than a flat disc.
	float range = high - low;
	result.clear();

	for (int i = 1; i <= num; i++) {
		float fraction = float(i) / float(num);
		float heightFraction = pow(fraction, heightPower);
		float radialFraction = pow(fraction, radialPower);
		r = radius * sqrt(float(i));
		theta = i * interval;
		float x = sin(theta) * r * radialFraction;
		float z = cos(theta) * r * radialFraction;
		float y = low + (range * heightFraction);
		result.append(MVector(x, y, z));
	}
	return MS::kSuccess;
}




// generate sunflower spiral
MStatus mayaMath::spiral (int numSpokes,
                          float pointsPerSpoke, float radius,
                          float startAngle,
                          float angle,
                          float radialPower,
                          float spokeOffset,
                          MVectorArray &result)
{
	if (!(numSpokes && pointsPerSpoke && radius)) { return MS::kUnknownParameter; }

	float period = double_pi / float(numSpokes);
	// one spoke at a time - in to out
	for (int k = 0; k < numSpokes; k++) {
		float angleOffset = (k * period) + startAngle;
		for (int i = 0; i < pointsPerSpoke; i++) {
			float fraction = float(i + 1) / float(pointsPerSpoke);
			float radialFraction = pow(fraction, radialPower);
			float theta = (i * angle) + angleOffset;
			float r = radius * radialFraction  + spokeOffset;
			float x = (sin(theta) * r )  ;
			float z = (cos(theta) * r )  ;
			float y =  0.0 ;
			result.append(MVector(x, y, z));
		}
	}
	return MS::kSuccess;
}

// Intersects ray r = p + td, |d| = 1, with sphere s and, if intersecting,
// returns t value of intersection and intersection point q


bool mayaMath::intersectRaySphere (
  const MPoint &p,
  const MVector &d,
  mayaMath::Sphere s,
  double &t,
  MPoint &q
) {
	MVector dn = d.normal();
	MVector m = p - s.center;
	double b = m * dn;
	double c = (m * m) - (s.radius * s.radius);
	// Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0)
	if (c > 0.0f && b > 0.0f) { return false; }
	double discr = b * b - c;
	// A negative discriminant corresponds to ray missing sphere
	if (discr < 0.0f) { return false; }
	// Ray now found to intersect sphere, compute smallest t value of intersection
	t = -b - sqrt(discr);
	// If t is negative, ray started inside sphere so clamp t to zero
	// if (t < 0.0f) t = 0.0f;
	q = p + (t * dn);
	return true;
}

bool  mayaMath::testMovingSphereSphere(
  const mayaMath::Sphere &s0,
  const mayaMath::Sphere &s1,
  double &t)
{
	// Make static version of s1 with expanded radius
	mayaMath::Sphere sphere;
	sphere.radius =	s1.radius + s0.radius;
	sphere.center = s1.center;
	sphere.change = MVector::zero;

	const MVector &rayOrigin = s0.center;

	MVector change = (s0.change - s1.change);
	MVector rayDirection = change.normal();
	// Can now test directed segment s = s0.c + tv, v = (v0-v1)/||v0-v1|| against
	// the expanded sphere for intersection
	MPoint q;

	if (mayaMath::intersectRaySphere (rayOrigin, rayDirection  , sphere , t, q)) {
		return t <= change.length();
	}
	return false;
}

MStatus  mayaMath::worldToBary(
  const MVector &A,
  const MVector &B,
  const MVector &C,
  const MVector &P,
  MVector &bary  )  {


	// project all points onto the max ortho 2d plane
	const double epsilon = 1e-12;
	const MVector N = triNormal(A, B, C) ;
	double Ax, Ay, Bx, By, Cx, Cy, Px, Py;
	double nx = fabs(N.x);
	double ny = fabs(N.y);
	double nz = fabs(N.z);
	if (nx > ny) { // y is not greatest
		if (ny > nz) { // x is greatest so use y and z
			Ax = A.y; Ay = A.z; Bx = B.y; By = B.z;  Cx = C.y; Cy = C.z; Px = P.y; Py = P.z;
		}
		else {   // z is greater than y
			if (nz > nx) { // z is greatest so use x and y
				Ax = A.x; Ay = A.y; Bx = B.x; By = B.y;  Cx = C.x; Cy = C.y; Px = P.x; Py = P.y;
			}
			else {   // x is greatest so use y and z
				Ax = A.y; Ay = A.z; Bx = B.y; By = B.z;  Cx = C.y; Cy = C.z; Px = P.y; Py = P.z;
			}
		}
	}
	else { // x is not greatest
		if (ny > nz) { // y is greatest so use x and z
			// axis = 1;
			Ax = A.x; Ay = A.z; Bx = B.x; By = B.z;  Cx = C.x; Cy = C.z; Px = P.x; Py = P.z;
		}
		else {  //  y is not greatest
			// axis = 2;
			Ax = A.x; Ay = A.y; Bx = B.x; By = B.y;  Cx = C.x; Cy = C.y; Px = P.x; Py = P.y;
		}
	}
	double b0 =  (Bx - Ax) * (Cy - Ay) - (Cx - Ax) * (By - Ay);

	if (fabs(b0) < epsilon) {
		return MS::kFailure;
	}
	double b0Recip = 1.0 / b0 ;
	bary.x = ((Bx - Px) * (Cy - Py) - (Cx - Px) * (By - Py)) * b0Recip 	;
	bary.y = ((Cx - Px) * (Ay - Py) - (Ax - Px) * (Cy - Py)) * b0Recip	;
	bary.z = ((Ax - Px) * (By - Py) - (Bx - Px) * (Ay - Py)) * b0Recip	;

	return MS::kSuccess;
}

MStatus  mayaMath::worldToBary(
  const MFloatPoint &A,
  const MFloatPoint &B,
  const MFloatPoint &C,
  const MFloatVector &N,
  const MFloatPoint &P,
  MFloatVector &bary  )  {


	// project all points onto the max ortho 2d plane
	const  float epsilon = 1e-12f;
	float Ax, Ay, Bx, By, Cx, Cy, Px, Py;
	float nx = fabs(N.x);
	float ny = fabs(N.y);
	float nz = fabs(N.z);
	if (nx > ny) { // y is not greatest
		if (ny > nz) { // x is greatest so use y and z
			Ax = A.y; Ay = A.z; Bx = B.y; By = B.z;  Cx = C.y; Cy = C.z; Px = P.y; Py = P.z;
		}
		else {   // z is greater than y
			if (nz > nx) { // z is greatest so use x and y
				Ax = A.x; Ay = A.y; Bx = B.x; By = B.y;  Cx = C.x; Cy = C.y; Px = P.x; Py = P.y;
			}
			else {   // x is greatest so use y and z
				Ax = A.y; Ay = A.z; Bx = B.y; By = B.z;  Cx = C.y; Cy = C.z; Px = P.y; Py = P.z;
			}
		}
	}
	else { // x is not greatest
		if (ny > nz) { // y is greatest so use x and z
			// axis = 1;
			Ax = A.x; Ay = A.z; Bx = B.x; By = B.z;  Cx = C.x; Cy = C.z; Px = P.x; Py = P.z;
		}
		else {  //  y is not greatest
			// axis = 2;
			Ax = A.x; Ay = A.y; Bx = B.x; By = B.y;  Cx = C.x; Cy = C.y; Px = P.x; Py = P.y;
		}
	}
	float b0 =  (Bx - Ax) * (Cy - Ay) - (Cx - Ax) * (By - Ay);

	if (fabs(b0) < epsilon) {
		return MS::kFailure;
	}
	float b0Recip = 1.0f / b0 ;
	bary.x = ((Bx - Px) * (Cy - Py) - (Cx - Px) * (By - Py)) * b0Recip 	;
	bary.y = ((Cx - Px) * (Ay - Py) - (Ax - Px) * (Cy - Py)) * b0Recip	;
	bary.z = ((Ax - Px) * (By - Py) - (Bx - Px) * (Ay - Py)) * b0Recip	;

	return MS::kSuccess;
}


bool  mayaMath::pointInTriangle2d(const float2 &a, const float2 &b, const float2 &c,
                                  const float2 &p,  MFloatVector &bary  )
{
	const  float epsilon = 1e-12f;

	float b0 =  (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1]);

	if (fabs(b0) < epsilon) {
		return false;
	}
	float b0Recip = 1.0f / b0 ;
	bary.x = ((b[0] - p[0]) * (c[1] - p[1]) - (c[0] - p[0]) * (b[1] - p[1])) * b0Recip 	;
	if (bary.x < 0.0f) { return false; }
	bary.y = ((c[0] - p[0]) * (a[1] - p[1]) - (a[0] - p[0]) * (c[1] - p[1])) * b0Recip	;
	if (bary.y < 0.0f) { return false; }
	bary.z = 1.0 - (bary.x + bary.y);
	if (bary.z < 0.0f) { return false; }

	return true;
}



MVector mayaMath::closestPointOnTriangle(const MVector &p, const MVector &a,
    const MVector &b, const MVector &c, MVector &bary)
{
	// check if p is in vertex region outside a
	///////////////////////////////////////////////////////
	MVector ab = b - a;
	MVector ac = c - a;
	MVector ap = p - a;
	double d1 = (ab) * (ap);
	double d2 = (ac) * (ap);
	if (d1 <= 0.0 && d2 <= 0.0) {bary = MVector(1, 0, 0); return a;}
	///////////////////////////////////////////////////////

	// check if p is in vertex region outside b
	///////////////////////////////////////////////////////
	MVector bp = p - b;
	double d3 = (ab) * (bp);
	double d4 = (ac) * (bp);
	if (d3 >= 0.0 && d4 <= d3) {bary = MVector(0, 1, 0); return b;}
	///////////////////////////////////////////////////////

	// check if p is in edge region outside ab
	///////////////////////////////////////////////////////
	double vc = d1 * d4 - d3 * d2;
	if (vc <= 0.0 && d1 >= 0.0 && d3 <= 0.0) {
		double v = d1 / (d1 - d3);
		bary = MVector(1.0 - v, v, 0);
		return a + (v * ab);
	}
	///////////////////////////////////////////////////////

	// check if p is in vertex region outside c
	///////////////////////////////////////////////////////
	MVector cp = p - c;
	double d5 = (ab) * (cp);
	double d6 = (ac) * (cp);
	if (d6 >= 0.0 && d5 <= d6) {bary = MVector(0, 0, 1); return c;}
	///////////////////////////////////////////////////////

	// check if p is in edge region outside ac
	///////////////////////////////////////////////////////
	double vb = d5 * d2 - d1 * d6;
	if (vb <= 0.0 && d2 >= 0.0 && d6 <= 0.0) {
		double w = d2 / (d2 - d6);
		bary = MVector(1.0 - w, 0, w);
		return a + (w * ac);
	}
	///////////////////////////////////////////////////////

	// check if p is in edge region outside bc
	///////////////////////////////////////////////////////
	double va = d3 * d6 - d5 * d4;
	double d4md3 = (d4 - d3) ;
	double d5md6 = (d5 - d6) ;

	if (va <= 0.0 && d4md3 >= 0.0 && d5md6 >= 0.0) {
		double w = d4md3 / (d4md3 + d5md6);
		bary = MVector(0, 1.0 - w, w);
		return b + (w * (c - b));
	}
	///////////////////////////////////////////////////////

	// p is on triangle somewhere
	///////////////////////////////////////////////////////
	double denom = 1.0 / (va + vb + vc);
	double v = vb * denom;
	double w = vc * denom;
	bary = MVector( ((1.0 - v) - w) , v, w);
	return a + (ab * v) + (ac * w);
	///////////////////////////////////////////////////////
}

bool mayaMath::baryIntersections(
  const MVector &bary1,
  const MVector &bary2,
  MVectorArray &resBarys
)
{
	// if the two sets of baryCoords passed in to the function are inside the triangle then return them
	// if one is outside then return the other one and the intersection of the joining line with the edge
	// if both are outside then return all valid edge intersections (there will be zero or two of these)
	resBarys.clear();


	int r1 = getRegion( bary1);
	int r2 = getRegion( bary2);
	// test the regions
	// return false if both points are in the same set of adjacent regions - i.e.
	// both in (2-3-4) or  both in (4-5-6) or  both in (6-1-2)
	if ( (r1 > 1 && r1 < 5) && (r2 > 1 && r2 < 5) ) {
		//cerr << "EAST" <<endl;
		return false;
	}
	if ( (r1 > 3 && r1 < 7) && (r2 > 3 && r2 < 7) ) {
		//cerr << "WEST" <<endl;
		return false;
	}
	if ( (r1 == 1 || r1 == 2 || r1 == 6) && (r2 == 1 || r2 == 2 || r2 == 6)) {
		//cerr << "SOUTH" <<endl;
		return false;
	}

	MVector baryDiff = (bary2 - bary1);
	MVector baryNew;
	double fraction ;

	if (!r1) { resBarys.append(bary1); }  // testing region 0 (on the triangle)
	if (!r2) { resBarys.append(bary2); }
	if (resBarys.length() == 2) { return true; }
	//cerr << "here 1"<<endl;
	if (sign(bary1.x) != sign(bary2.x)) { // test YZ
		fraction = -(bary2.x / baryDiff.x);
		baryNew = (fraction * baryDiff) + bary2;
		if ( inRange(baryNew.y)  &&  inRange(baryNew.z) ) {
			resBarys.append(baryNew);
			if (resBarys.length() == 2) { return true; }
		}
	}


	if (sign(bary1.y) != sign(bary2.y)) { // test XZ
		fraction = -(bary2.y / baryDiff.y);
		baryNew = (fraction * baryDiff) + bary2;
		if ( inRange(baryNew.x)  &&  inRange(baryNew.z) ) {
			resBarys.append(baryNew);
			if (resBarys.length() == 2) { return true; }
		}
	}


	if (sign(bary1.z) != sign(bary2.z)) { // test XY
		fraction = -(bary2.z / baryDiff.z);
		baryNew = (fraction * baryDiff) + bary2;
		if ( inRange(baryNew.x)  &&  inRange(baryNew.y) ) {
			resBarys.append(baryNew);
			if (resBarys.length() == 2) { return true; }
		}
	}

	return false;
}

bool mayaMath::triTriIntersection(
  const MVector P0,
  const MVector P1,
  const MVector P2,
  const MVector Q0,
  const MVector Q1,
  const MVector Q2,
  MPoint &start, MPoint &end
) {
	// cerr << "in trianglesIntersect"<<endl;
	// find out which side of our plane the other vertices lie on


	MVector v0 = Q0 - P0;
	MVector v1 = Q1 - P0;
	MVector v2 = Q2 - P0;

	MVector N = triNormal(P0, P1, P2);
	double dot0 =  v0 * N;
	double dot1 =  v1 * N;
	double dot2 =  v2 * N;
	int sign0 = sign(dot0);
	int sign1 = sign(dot1);
	int sign2 = sign(dot2);

	// if they all lie on the same side of the plane (or 2 of them
	// do and the other is on the plane) then there is no intersection

	if (abs(sign0 + sign1 + sign2) >= 2) {
		return false;
	}
	// if two or more lie on the plane then there is no intersection
	if ( ((sign0 == 0) + (sign1 == 0) + (sign2 == 0)) >=  2 ) {
		return false;
	}
	// now we have two verts on one side of the plane and one on the other,
	// so get the intersections with the plane

	MVectorArray res(2, MVector::zero);
	int index = 0;

	// get the intersection points with the plane
	///////////////////////////////////////////////////
	bool crossesPlane;
	if (sign0 != sign1)  {

		crossesPlane = lineIntersectsPlane(N, P0, Q0, Q1, res[index]);
		if (crossesPlane) { index++; }
	}

	if (sign0 != sign2)  {

		crossesPlane = lineIntersectsPlane(N, P0, Q0, Q2, res[index]);
		if (crossesPlane) { index++; }
	}

	if (index < 2) {
		if (sign1 != sign2) {
			crossesPlane = lineIntersectsPlane(N, P0, Q1, Q2, res[index]);
			if (crossesPlane) { index++; }
		}
	}
	if (index != 2) {
		// something very strange happened
		return false;
	}

	// res(2) should now contain both intersections so next we project
	// everything into 2d and calculate barycentrics
	///////////////////////////////////////////////////

	MVector bary1, bary2;

	worldToBary(P0, P1, P2, res[0], bary1);
	worldToBary(P0, P1, P2, res[1], bary2);
	MVectorArray resBarys;

	bool intersected = baryIntersections(bary1, bary2, resBarys);

	if (!intersected) {
		return false;
	}
	start = baryToWorld(P0, P1, P2, resBarys[0]);
	end = baryToWorld(P0, P1, P2, resBarys[1]);
	return true;
}



///////////////////////////////////////

MFloatPoint mayaMath::calcFaceCenter (const MFloatPoint &a, const MFloatPoint &b,
                                      const MFloatPoint &c, const MFloatPoint &d) {

	MFloatPoint e1 = a + ((b - a) * 0.5);
	MFloatPoint e2 = d + ((c - d) * 0.5);
	return e1 + ((e2 - e1) * 0.5);
}


double  mayaMath::triArea(
  const MVector &p1,
  const MVector &p2,
  const MVector &p3
) {
	return (((p2 - p1) ^ (p3 - p1)).length() * 0.5);
}


double  mayaMath::triArea(const MVectorArray &v) {
	return ( ((v[1] - v[0]) ^ (v[2] - v[0])).length() * 0.5);
}

MVector  mayaMath::triNormal(
  const MPoint &p1,
  const MPoint &p2,
  const MPoint &p3
) {
	MVector v1 = p1 - p3;
	MVector v2 = p2 - p3;

	if (v1.isParallel(v2)) { return MVector::zero; }

	return (v1 ^ v2).normal();

}

double mayaMath::baryToValue( const double a,    const double b,     const double c,
                              const MVector &bary   )
{
	return  (a * bary.x + b * bary.y + c * bary.z)  ;
}

MVector mayaMath::baryToUV(const MDoubleArray &U, const MDoubleArray &V,
                           const MVector &bary )
{
	return  MVector( (U[0] * bary.x + U[1] * bary.y + U[2] * bary.z) ,
	                 (V[0] * bary.x + V[1] * bary.y + V[2] * bary.z) , 0.0);
}

MVector mayaMath::baryToUV(const MVector &UV0, const MVector &UV1, const MVector &UV2,
                           const MVector &bary )
{
	return  MVector((UV0.x * bary.x + UV1.x * bary.y + UV2.x * bary.z) ,
	                (UV0.y * bary.x + UV1.y * bary.y + UV2.y * bary.z) , 0.0);
}

MVector mayaMath::baryToWorld(   const MVector &A,    const MVector &B,
                                 const MVector &C,     const MVector &bary    )
{
	return  MVector((A.x * bary.x + B.x * bary.y + C.x * bary.z) ,
	                (A.y * bary.x + B.y * bary.y + C.y * bary.z) ,
	                (A.z * bary.x + B.z * bary.y + C.z * bary.z));
}

MVector mayaMath::baryToWorld(   const MVectorArray &V,    const MVector &bary   )
{
	return  MVector((V[0].x * bary.x + V[1].x * bary.y + V[2].x * bary.z) ,
	                (V[0].y * bary.x + V[1].y * bary.y + V[2].y * bary.z) ,
	                (V[0].z * bary.x + V[1].z * bary.y + V[2].z * bary.z));
}

int  mayaMath::getRegion(const MVector &bary) {

	/*
	 4
	 \ |
	 	\|
	 	 B
	   |\
	 5 | \  3
	   |  \
	   | 0 \
	----A----C---
	 6 |  1  \  2
	   |  	  \
	*/
	bool x = (bary.x > 0);
	bool y = (bary.y > 0);
	bool z = (bary.z > 0);

	if (x && y && z) { return 0; }

	if ((!x) && y && z) { return 3; }
	if (x && (!y) && z) { return  1; }
	if (x && y && (!z)) { return  5; }

	if ((!x) && (!y) && z) { return  2; }
	if (x && (!y) && (!z)) { return  6; }
	if ((!x) && y && (!z)) { return  4; }

	return 0;   // it's impossible to get here
}


bool mayaMath::lineIntersectsPlane(		const MVector &N,    // plane Normal
                                      const MVector &P,     // a point on the plane
                                      const MVector &A,     // start of line
                                      const MVector &B,     // end of line
                                      MVector &r            // the intersection point
                                  )  {
	const double epsilon = 1e-6;
	MVector V = (B - A);
	double denominator = N * V;

	if (fabs(denominator) < epsilon) {
		return false;
	}
	double numerator = N * (P - A);
	double U = numerator / denominator;
	if ((U < 0.0) || (U > 1.0)) {
		return false;
	}
	r = A + (U * V);
	return true;
}

short mayaMath::sign(double v)  {  // return -1, 0 or 1
	const double epsilon = 1e-6;
	if (v < -(epsilon) ) { return -1; }
	if (v > epsilon) { return 1; }
	return 0;
}

bool mayaMath::inRange(const double &v)  {  // number is between 0 and 1
	if (v < 0.0) { return false; }
	if (v > 1.0) { return false; }
	return true;
}

MFloatPoint mayaMath::closestPointOnLine(
  const MFloatPoint &p,
  const MFloatPoint &a,
  const MFloatPoint &b,
  float &t
) {
	MFloatVector ab = b - a;
	t = ((p - a) * ab) / (ab * ab);
	if (t < 0.0f) { t = 0.0f; }
	if (t > 1.0f) { t = 1.0f; }
	return a + (t * ab);
}

MVector mayaMath::closestPointOnLine(
  const MVector &p,
  const MVector &a,
  const MVector &b,
  double &t
) {
	MVector ab = b - a;
	t = ((p - a) * ab) / (ab * ab);
	if (t < 0) { t = 0; }
	if (t > 1) { t = 1; }
	return a + (t * ab);
}

MVector mayaMath::closestPointOnLine(
  const MVector &A,
  const MVector &B,
  const MVector &P
)  {
	MVector BA = B - A;

	MVector PA = P - A;
	if ((PA * BA) < 0) { return A; }

	MVector PB = P - B;
	if ((PB * (-BA)) < 0) { return B; }

	BA.normalize();
	double dot = PA * BA;
	return A + (BA * dot);
}

MVector mayaMath::closestPointOnInfiniteLine(
  const MVector &start,
  const MVector &direction,
  const MVector &toPoint
)  {
	MVector PA = toPoint - start;
	double dot = PA * direction;
	return start + (direction * dot);
}



bool mayaMath::validTriangle( const MFloatPoint &A, const MFloatPoint &B,
                              const MFloatPoint &C )  {
	if (A.isEquivalent(B)) { return false; }
	if (C.isEquivalent(B)) { return false; }
	if (A.isEquivalent(C)) { return false; }
	return (true);
}

bool mayaMath::validTriangle( const MFloatVector &A, const MFloatVector &B,
                              const MFloatVector &C )  {
	if (A.isEquivalent(B)) { return false; }
	if (C.isEquivalent(B)) { return false; }
	if (A.isEquivalent(C)) { return false; }
	return (true);
}

bool mayaMath::spherePlaneIntersection(const MVector &pointOnPlane ,
                                       const MVector &normal , const MVector &center, double radius)  {
	return ( fabs((center - pointOnPlane) * normal) <= radius);
}

bool mayaMath::boxBoxIntersection(const MBoundingBox &b1, const MBoundingBox &b2 )  {

	if (b1.max()[0] <= b2.min()[0]) { return false; }
	if (b1.min()[0] >= b2.max()[0]) { return false; }

	if (b1.max()[1] <= b2.min()[1]) { return false; }
	if (b1.min()[1] >= b2.max()[1]) { return false; }

	if (b1.max()[2] <= b2.min()[2]) { return false; }
	if (b1.min()[2] >= b2.max()[2]) { return false; }

	return  true;
}


bool mayaMath::sphereBoxIntersection(const MBoundingBox &box, const MFloatPoint &center,
                                     float radius)
{
	return  mayaMath::sphereBoxIntersection(box, MVector(center.x, center.y, center.z),
	                                        double(radius));
}

bool mayaMath::sphereBoxIntersection(const MBoundingBox &box, const MVector &center,
                                     double radius)
{
	double s, d = 0;

	if (center.x < box.min().x) { s = center.x - box.min().x; d += s * s;}
	else if (center.x > box.max().x) {s = center.x - box.max().x; d += s * s;}

	if (center.y < box.min().y) { s = center.y - box.min().y; d += s * s; }
	else if (center.y > box.max().y) {s = center.y - box.max().y; d += s * s;}

	if (center.z < box.min().z) { s = center.z - box.min().z; d += s * s; }
	else if (center.z > box.max().z) { s = center.z - box.max().z; d += s * s; }

	return d <= radius * radius;
}

MFloatMatrix mayaMath::matFromAxes(const MVector &P, const MVector &X, const MVector &Y,
                                   const MVector &Z )    {
	MFloatMatrix mat;

	mat[0][0] = float(X.x);	mat[0][1] = float(X.y);	mat[0][2] = float(X.z);	mat[0][3] = 0;
	mat[1][0] = float(Y.x);	mat[1][1] = float(Y.y);	mat[1][2] = float(Y.z);	mat[1][3] = 0;
	mat[2][0] = float(Z.x);	mat[2][1] = float(Z.y);	mat[2][2] = float(Z.z);	mat[2][3] = 0;
	mat[3][0] = float(P.x);	mat[3][1] = float(P.y);	mat[3][2] = float(P.z);	mat[3][3] = 1.0;

	return mat;
}

// MMatrix mayaMath::matFromAxes(const MVector &P, const MVector &X, const MVector &Y,
//                               const MVector &Z )    {
// 	MMatrix mat;

// 	mat[0][0] = X.x;	mat[0][1] = X.y;	mat[0][2] = X.z;	mat[0][3] = 0;
// 	mat[1][0] = Y.x;	mat[1][1] = Y.y;	mat[1][2] = Y.z;	mat[1][3] = 0;
// 	mat[2][0] = Z.x;	mat[2][1] = Z.y;	mat[2][2] = Z.z;	mat[2][3] = 0;
// 	mat[3][0] = P.x;	mat[3][1] = P.y;	mat[3][2] = P.z;	mat[3][3] = 1.0;

// 	return mat;
// }


double mayaMath::attenuate(const double &distance, const  double &maxDistance,
                           const  double &decay) {
	if ( distance >=  maxDistance) { return 0.0; }

	double result = 1.0;
	if (decay > 0) {
		double closeness = (1.0 - (distance / maxDistance));
		if (decay == 1.0) {//if linear
			result *= closeness;
		}
		else if (decay == 2.0) {
			result *= (closeness * closeness) ; // ^2
		}
		else {
			result *= pow(closeness, decay);
		}
	}
	return result;
}


MStatus mayaMath::quadInterp( const MFloatVector &x, const MFloatVector &y,
                              const MFloatArray &t, MFloatArray &result ) {


	// make sure data is in good order
	if ((x[1] < x[0]) || (x[1] > x[2])) { return MS::kFailure; }
	if (! t.length()) { return MS::kFailure; }
	// do some pre calcs outside the loop


	//float dif1 = ( y[1] - y[0] ) / ( x[1] - x[0]);
	//float dif2 = (y[2] - y[1]) / (x[2] - x[1]);

	float xrange1 = x[0] - x[1];
	float xrange2 = x[2] - x[1];
	float yrange1 = y[0] - y[1];
	float yrange2 = y[2] - y[1];

	// loop over t
	result.clear();
	for (unsigned i = 0; i < t.length(); i++) {
		if ( t[i] <= x[0]) {
			result.append(y[0]);
		}
		else if  ( t[i] >= x[2]) {
			result.append(y[2]);
		}
		else if (t[i] == x[1] ) {
			result.append( y[1]);
		}
		else if  ( t[i] < x[1]) {   // t is between between pt0 and pt1
			float frac = (t[i] - x[1]) / xrange1 ;
			frac = (frac * frac * yrange1) + y[1];
			result.append(frac);
		}
		else {   // t must be between between pt1 and pt2
			float frac = (t[i] - x[1]) / xrange2 ;
			frac = (frac * frac * yrange2) + y[1];
			result.append(frac);
		}
	}
	return MS::kSuccess;
}



MFloatMatrix mayaMath::matFromAim(
  const MFloatVector &trans,
  const MFloatVector &front,
  const MFloatVector &up,
  mayaMath::axis frontAxis,
  mayaMath::axis upAxis
)  {
	// deal with inversions
	////////////////////////////////////////////////////
	bool invertFront = (frontAxis > 2);
	frontAxis = mayaMath::axis(short(frontAxis) % 3);

	bool invertUp = (upAxis > 2);
	upAxis =  mayaMath::axis(short(upAxis) % 3);

	MFloatVector fAxis = front.normal();
	if (invertFront) { fAxis = -fAxis; }

	MFloatVector uAxis(up);
	if (invertUp) { uAxis = -uAxis; }
	////////////////////////////////////////////////////

	// make sure vectors are valid and prepare orthonormal xyz
	////////////////////////////////////////////////////
	if (fAxis.isEquivalent(MFloatVector::zero)) {
		fAxis = (MVector::xAxis);
	}
	if (uAxis.isEquivalent(MFloatVector::zero)) {
		uAxis = (MFloatVector::yAxis);
	}
	if  ( fAxis.isParallel(uAxis) ) { // gotta do something,
		if ( fAxis.isParallel(MFloatVector::yAxis) ) {
			uAxis = (MFloatVector::xAxis) ;
		}
		else {
			uAxis = (MFloatVector::yAxis);
		}
	}
	MFloatVector sAxis = (fAxis ^ uAxis).normal();
	uAxis = (sAxis ^ fAxis).normal();
	////////////////////////////////////////////////////



	MFloatVector xa, ya, za;


	if (frontAxis == mayaMath::xAxis)	{
		xa = fAxis;
		if (upAxis == mayaMath::yAxis ) {
			ya = uAxis;
			za = sAxis;
		}
		else {
			za = uAxis;
			ya = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::yAxis)	{
		ya = fAxis;
		if (upAxis == mayaMath::zAxis ) {
			za = uAxis;
			xa = sAxis;
		}
		else {
			xa = uAxis;
			za = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::zAxis)	{
		za = fAxis;
		if (upAxis == mayaMath::xAxis ) {
			xa = uAxis;
			ya = sAxis;
		}
		else {
			ya = uAxis;
			xa = -sAxis;
		}
	}

	float mat[4][4] ;
	// fill in the rot matrix
	////////////////////////////////////////////////////
	mat[0][0] = xa.x;	mat[0][1] = xa.y;	mat[0][2] = xa.z;	mat[0][3] = 0;
	mat[1][0] = ya.x;	mat[1][1] = ya.y;	mat[1][2] = ya.z;	mat[1][3] = 0;
	mat[2][0] = za.x;	mat[2][1] = za.y;	mat[2][2] = za.z;	mat[2][3] = 0;
	mat[3][0] = trans.x;	mat[3][1] = trans.y;	mat[3][2] = trans.z;	mat[3][3] = 1.0;

	return MFloatMatrix(mat);



	//  MTransformationMatrix mtMat(mmat);
	//  mtMat.setScale(dScale, MSpace::kTransform);
	//  mtMat.setTranslation(trans, MSpace::kTransform)	;

	// return mtMat.asMatrix();




}
MFloatMatrix mayaMath::matFromAim(
  const MFloatVector &trans,
  const MFloatVector &front,
  const MFloatVector &up,
  const float &scale,
  mayaMath::axis frontAxis,
  mayaMath::axis upAxis
)  {
	// deal with inversions
	////////////////////////////////////////////////////
	float mat[4][4] ;


	if (
	  (front.isEquivalent(MFloatVector::zero)) ||
	  (up.isEquivalent(MFloatVector::zero))	||
	  (front.isParallel(up))
	) {

		mat[0][0] = 0.0f;	mat[0][1] = 0.0f;	mat[0][2] = 0.0f;	mat[0][3] = 0.0f;
		mat[1][0] = 0.0f;	mat[1][1] = 0.0f;	mat[1][2] = 0.0f;	mat[1][3] = 0.0f;
		mat[2][0] = 0.0f;	mat[2][1] = 0.0f;	mat[2][2] = 0.0f;	mat[2][3] = 0.0f;
		mat[3][0] = trans.x;	mat[3][1] = trans.y;	mat[3][2] = trans.z;	mat[3][3] = 1.0;
		return MFloatMatrix(mat);
	}



	bool invertFront = (frontAxis > 2);
	frontAxis = mayaMath::axis(short(frontAxis) % 3);

	bool invertUp = (upAxis > 2);
	upAxis =  mayaMath::axis(short(upAxis) % 3);

	MFloatVector fAxis = front.normal();
	if (invertFront) { fAxis = -fAxis; }

	MFloatVector uAxis(up);
	if (invertUp) { uAxis = -uAxis; }
	////////////////////////////////////////////////////

	// make sure vectors are valid and prepare orthonormal xyz
	////////////////////////////////////////////////////

	MFloatVector sAxis = (fAxis ^ uAxis).normal();
	uAxis = (sAxis ^ fAxis).normal();
	////////////////////////////////////////////////////



	MFloatVector xa, ya, za;


	if (frontAxis == mayaMath::xAxis)	{
		xa = fAxis;
		if (upAxis == mayaMath::yAxis ) {
			ya = uAxis;
			za = sAxis;
		}
		else {
			za = uAxis;
			ya = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::yAxis)	{
		ya = fAxis;
		if (upAxis == mayaMath::zAxis ) {
			za = uAxis;
			xa = sAxis;
		}
		else {
			xa = uAxis;
			za = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::zAxis)	{
		za = fAxis;
		if (upAxis == mayaMath::xAxis ) {
			xa = uAxis;
			ya = sAxis;
		}
		else {
			ya = uAxis;
			xa = -sAxis;
		}
	}

	xa *= scale;
	ya *= scale;
	za *= scale;


	// fill in the rot matrix
	////////////////////////////////////////////////////
	mat[0][0] = xa.x;	mat[0][1] = xa.y;	mat[0][2] = xa.z;	mat[0][3] = 0;
	mat[1][0] = ya.x;	mat[1][1] = ya.y;	mat[1][2] = ya.z;	mat[1][3] = 0;
	mat[2][0] = za.x;	mat[2][1] = za.y;	mat[2][2] = za.z;	mat[2][3] = 0;
	mat[3][0] = trans.x;	mat[3][1] = trans.y;	mat[3][2] = trans.z;	mat[3][3] = 1.0;

	return MFloatMatrix(mat);



	//  MTransformationMatrix mtMat(mmat);
	//  mtMat.setScale(dScale, MSpace::kTransform);
	//  mtMat.setTranslation(trans, MSpace::kTransform)	;

	// return mtMat.asMatrix();




}



MMatrix mayaMath::matFromAim(
  const MVector &trans,
  const MVector &front,
  const MVector &up,
  const MVector &scale,
  mayaMath::axis frontAxis,
  mayaMath::axis upAxis
)  {


	// deal with inversions
	////////////////////////////////////////////////////
	bool invertFront = (frontAxis > 2);
	frontAxis = mayaMath::axis(short(frontAxis) % 3);

	bool invertUp = (upAxis > 2);
	upAxis =  mayaMath::axis(short(upAxis) % 3);

	if (front.isEquivalent(MVector::zero) ) { return MMatrix::identity; }
	MVector fAxis = front.normal();
	if (invertFront) { fAxis = -fAxis; }

	MVector uAxis(up);
	if (invertUp) { uAxis = -uAxis; }
	////////////////////////////////////////////////////



	// make sure vectors are valid and prepare orthonormal xyz
	////////////////////////////////////////////////////
	if (fAxis.isEquivalent(MVector::zero)) {
		fAxis = (MVector::xAxis);
	}
	if (uAxis.isEquivalent(MVector::zero)) {
		uAxis = (MVector::yAxis);
	}
	if  ( fAxis.isParallel(uAxis) ) { // gotta do something,
		if ( fAxis.isParallel(MVector::yAxis) ) {
			uAxis = (MVector::xAxis) ;
		}
		else {
			uAxis = (MVector::yAxis);
		}
	}
	MVector sAxis = (fAxis ^ uAxis).normal();
	uAxis = (sAxis ^ fAxis).normal();
	////////////////////////////////////////////////////

	MVector xa, ya, za;


	if (frontAxis == mayaMath::xAxis)	{
		xa = fAxis;
		if (upAxis == mayaMath::yAxis ) {
			ya = uAxis;
			za = sAxis;
		}
		else {
			za = uAxis;
			ya = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::yAxis)	{
		ya = fAxis;
		if (upAxis == mayaMath::zAxis ) {
			za = uAxis;
			xa = sAxis;
		}
		else {
			xa = uAxis;
			za = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::zAxis)	{
		za = fAxis;
		if (upAxis == mayaMath::xAxis ) {
			xa = uAxis;
			ya = sAxis;
		}
		else {
			ya = uAxis;
			xa = -sAxis;
		}
	}

	double mat[4][4] ;
	// fill in the rot matrix
	////////////////////////////////////////////////////
	mat[0][0] = xa.x;	mat[0][1] = xa.y;	mat[0][2] = xa.z;	mat[0][3] = 0;
	mat[1][0] = ya.x;	mat[1][1] = ya.y;	mat[1][2] = ya.z;	mat[1][3] = 0;
	mat[2][0] = za.x;	mat[2][1] = za.y;	mat[2][2] = za.z;	mat[2][3] = 0;
	mat[3][0] = 0.0;	mat[3][1] = 0.0;	mat[3][2] = 0.0;	mat[3][3] = 1.0;

	MMatrix mmat(mat);


	const double dScale[3] = {scale.x, scale.y, scale.z};

	MTransformationMatrix mtMat(mmat);
	mtMat.setScale(dScale, MSpace::kTransform);
	mtMat.setTranslation(trans, MSpace::kTransform)	;

	return mtMat.asMatrix();

}



MMatrix mayaMath::matFromAim(
  const MVector &trans,
  const MVector &front,
  const MVector &up,
  mayaMath::axis frontAxis,
  mayaMath::axis upAxis
)  {


	// deal with inversions
	////////////////////////////////////////////////////
	bool invertFront = (frontAxis > 2);
	frontAxis = mayaMath::axis(short(frontAxis) % 3);

	bool invertUp = (upAxis > 2);
	upAxis =  mayaMath::axis(short(upAxis) % 3);

	if (front.isEquivalent(MVector::zero) ) { return MMatrix::identity; }
	MVector fAxis = front.normal();
	if (invertFront) { fAxis = -fAxis; }

	MVector uAxis(up);
	if (invertUp) { uAxis = -uAxis; }
	////////////////////////////////////////////////////



	// make sure vectors are valid and prepare orthonormal xyz
	////////////////////////////////////////////////////
	if (fAxis.isEquivalent(MVector::zero)) {
		fAxis = (MVector::xAxis);
	}
	if (uAxis.isEquivalent(MVector::zero)) {
		uAxis = (MVector::yAxis);
	}
	if  ( fAxis.isParallel(uAxis) ) { // gotta do something,
		if ( fAxis.isParallel(MVector::yAxis) ) {
			uAxis = (MVector::xAxis) ;
		}
		else {
			uAxis = (MVector::yAxis);
		}
	}
	MVector sAxis = (fAxis ^ uAxis).normal();
	uAxis = (sAxis ^ fAxis).normal();
	////////////////////////////////////////////////////

	MVector xa, ya, za;


	if (frontAxis == mayaMath::xAxis)	{
		xa = fAxis;
		if (upAxis == mayaMath::yAxis ) {
			ya = uAxis;
			za = sAxis;
		}
		else {
			za = uAxis;
			ya = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::yAxis)	{
		ya = fAxis;
		if (upAxis == mayaMath::zAxis ) {
			za = uAxis;
			xa = sAxis;
		}
		else {
			xa = uAxis;
			za = -sAxis;
		}
	}
	else if (frontAxis == mayaMath::zAxis)	{
		za = fAxis;
		if (upAxis == mayaMath::xAxis ) {
			xa = uAxis;
			ya = sAxis;
		}
		else {
			ya = uAxis;
			xa = -sAxis;
		}
	}

	double mat[4][4] ;
	// fill in the rot matrix
	////////////////////////////////////////////////////
	mat[0][0] = xa.x;	mat[0][1] = xa.y;	mat[0][2] = xa.z;	mat[0][3] = 0;
	mat[1][0] = ya.x;	mat[1][1] = ya.y;	mat[1][2] = ya.z;	mat[1][3] = 0;
	mat[2][0] = za.x;	mat[2][1] = za.y;	mat[2][2] = za.z;	mat[2][3] = 0;
	mat[3][0] = trans.x;	mat[3][1] = trans.y;	mat[3][2] = trans.z;	mat[3][3] = 1.0;

	MMatrix mmat(mat);

	return mmat;
}





MMatrix mayaMath::matFromPhi(const MVector &trans, const MVector &phi,
                             const MVector &scale) {
	// phi

	MVector ax(phi);
	double theta = ax.length();
	if (theta == 0.0)  {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}

	MQuaternion q(theta, ax);

	MTransformationMatrix mtMat(MMatrix::identity);
	mtMat.setRotationQuaternion(q.x, q.y, q.z, q.w);


	const double dScale[3] = {scale.x, scale.y, scale.z};

	mtMat.setScale(dScale , MSpace::kTransform);
	mtMat.setTranslation(trans, MSpace::kTransform)	;
	MMatrix mmat;

	return mtMat.asMatrix();

}

MFloatMatrix mayaMath::matFromPhi(const MVector &trans, const MVector &phi) {

	MVector ax(phi);
	double theta = ax.length();
	if (theta == 0.0)  {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}

	MQuaternion q(theta, ax);
	MMatrix mat = q.asMatrix();
	float fm[4][4];
	mat.get(fm);
	fm[3][0] = float(trans.x);
	fm[3][1] = float(trans.y);
	fm[3][2] = float(trans.z);
	return MFloatMatrix(fm);
}


MMatrix mayaMath::matFromPhi(const MVector &phi) {
	// phi

	MVector ax(phi);
	double theta = ax.length();
	if (theta == 0.0)  {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}

	MQuaternion q(theta, ax);
	return	q.asMatrix();

	//  MTransformationMatrix mtMat(MMatrix::identity);
	//  mtMat.setRotationQuaternion(q.x,q.y,q.z,q.w);
	//
	//
	//  const double dScale[3] = {scale.x,scale.y,scale.z};
	//
	//  mtMat.setScale(dScale , MSpace::kTransform);
	//  mtMat.setTranslation(trans, MSpace::kTransform)	;
	//  MMatrix mmat;
	//
	//  return mtMat.asMatrix();

}

void mayaMath::axesFromPhi(const MVector &phi, MFloatVector &x,  MFloatVector &y,
                           MFloatVector &z) {
	// phi

	MVector ax(phi);
	double theta = ax.length();
	if (theta == 0.0)  {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}

	MQuaternion q(theta, ax);
	MMatrix m =	q.asMatrix();

	x = MFloatVector(m[0][0], m[0][1], m[0][2]);
	y = MFloatVector(m[1][0], m[1][1], m[1][2]);
	z = MFloatVector(m[2][0], m[2][1], m[2][2]);


	//  MTransformationMatrix mtMat(MMatrix::identity);
	//  mtMat.setRotationQuaternion(q.x,q.y,q.z,q.w);
	//
	//
	//  const double dScale[3] = {scale.x,scale.y,scale.z};
	//
	//  mtMat.setScale(dScale , MSpace::kTransform);
	//  mtMat.setTranslation(trans, MSpace::kTransform)	;
	//  MMatrix mmat;
	//
	//  return mtMat.asMatrix();

}



MVector mayaMath::phiFromMat(const MMatrix &mat) {
	MQuaternion q;
	q = mat;
	MVector ax;
	double theta;
	q.getAxisAngle(ax, theta) ;

	if (theta > mayaMath::single_pi)  { theta = theta - mayaMath::double_pi; }
	return ax * theta;
}

void mayaMath::posPhiFromMat(const MMatrix &mat, MVector &pos, MVector &phi) {
	// MQuaternion q;
	// q = mat;
	// MVector ax;
	// double theta;
	// q.getAxisAngle(ax,theta) ;

	// if (theta > mayaMath::single_pi)  theta = theta - mayaMath::double_pi;
	// phi = ax*theta;
	phi = mayaMath::phiFromMat(mat);
	pos = MVector(mat[3][0], mat[3][1], mat[3][2]);
}


MVector mayaMath::axisVector(mayaMath::axis a) {
	if (a == mayaMath::xAxis) { return MVector::xAxis; }
	if (a == mayaMath::yAxis) { return MVector::yAxis; }
	if (a == mayaMath::zAxis) { return MVector::zAxis; }
	if (a == mayaMath::xAxisNeg) { return -(MVector::xAxis); }
	if (a == mayaMath::yAxisNeg) { return -(MVector::yAxis); }
	if (a == mayaMath::zAxisNeg) { return -(MVector::zAxis); }
	return (MVector::zero);
}



MVector mayaMath::phiToEuler(
  const MVector &phi,
  MEulerRotation::RotationOrder ord,
  mayaMath::RotateUnit outUnit)
{
	MVector ax = phi;
	double len = phi.length();

	MQuaternion q;
	if (len < epsilon) {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}
	q.setAxisAngle(ax, len);
	MEulerRotation euler(MVector::zero, ord);
	euler = q;
	if (outUnit == mayaMath::kDegrees) {
		return (euler.asVector() *  57.295779524);
	}
	return euler.asVector() ;
}

MEulerRotation mayaMath::phiToEuler(
  const MVector &phi, MEulerRotation::RotationOrder ord)
{
	MVector ax = phi;
	double len = phi.length();

	MQuaternion q;
	if (len < epsilon) {
		ax = MVector::yAxis;
	}
	else {
		ax.normalize();
	}
	q.setAxisAngle(ax, len);
	MEulerRotation euler(MVector::zero, ord);
	euler = q;
	return euler ;
}


// void mayaMath::getPosition(const MMatrix &mat, MFloatPoint &result) {
// 	result = MFloatPoint(mat[3][0], mat[3][1], mat[3][2]);
// }

// void mayaMath::getPosition(const MMatrix &mat, MPoint &result) {
// 	result = MPoint(mat[3][0], mat[3][1], mat[3][2]);
// }

// void mayaMath::getRotation(const MMatrix &mat,
//                            MTransformationMatrix::RotationOrder order,
//                            MAngle::Unit unit, MVector &result)
// {
// 	double rotValue[3];
// 	MTransformationMatrix tMat(mat);
// 	tMat.reorderRotation(order);

// 	MTransformationMatrix::RotationOrder throwAway;
// 	tMat.getRotation( rotValue, throwAway );
// 	if (unit == MAngle::kDegrees) {
// 		rotValue[0] *= rad_to_deg;
// 		rotValue[1] *= rad_to_deg;
// 		rotValue[2] *= rad_to_deg;
// 	}
// 	result =  MVector(rotValue[0], rotValue[1], rotValue[2]);
// }


/////////////////////////////////////////////////


double mayaMath::eval(const double a[4], const double x) {
	return ((a[0] * x + a[1]) * x + a[2]) * x + a[3];
}

bool mayaMath::sign_change(const double a, const double b) {
	return (a >= 0 && b <= 0) || (a <= 0 && b >= 0);
}

bool mayaMath::equal(const double x, const double y, const double eps) {
	return (fabs(x - y) < eps);
}
bool mayaMath::isZero(const double x, const double eps) {
	return (fabs(x) < eps);
}


double mayaMath::find_root(
  const double a[4],
  double x0, double v0,
  double x1, double v1
) {
	const double eps = 1e-6;
	double x;
	const bool neg = v0 < 0;
	for (int i = 0; i < 10; ++i) {
		const double denom = v1 - v0;
		if (fabs(denom) < eps) {
			return x0;
		}
		const double alpha = v1 / denom;
		x = alpha * x0 + (1 - alpha) * x1;
		const double v = eval(a, x);
		if (fabs(v) < eps) {
			break;
		}
		else if (neg ? (v > 0) : (v < 0)) {
			x1 = x;
			v1 = v;
		}
		else {
			x0 = x;
			v0 = v;
		}
	}
	return x;
}

void mayaMath::output_root(double out[], int &i, const double x, const double lim ) {
	const double eps = 1e-6;
	if (x >= 0 && x <= lim) {
		if (i == 0) {
			out[i++] = x;
		}
		else if (!mayaMath::equal(out[i - 1], x, eps)) {
			out[i++] = x;
		}
	}
}

/// Find the roots of a cubic in the interval [0,lim]. Additionally treat any
/// point on the curve that evaluates close to zero as a root, even if it
/// doesn't actually exhibit a sign change (this means we might get more
/// than 3 outputs). Returns the number of values added to the output array
int mayaMath::cubic_roots(
  const double a[4],	// a, b, c, d
  double out[5],
  const double lim
) {
	int i = 0;
	const double eps = 1e-7;
	// CUBIC
	if (!mayaMath::isZero(a[0], eps)) {
		const double v0 = eval(a, 0);
		const double v1 = eval(a, lim);
		const double A = 3 * a[0];
		const double B = 2 * a[1];
		double disc = B * B - 4 * A * a[2];
		if (mayaMath::isZero(disc, eps)) {
			// one minima/maxima, two sub-intervals
			const double x = -B / (2 * A);
			const double v = eval(a, x);
			if (mayaMath::sign_change(v0, v)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, 0, v0, x, v), lim);
			}
			if (mayaMath::isZero(v, eps)) {
				mayaMath::output_root(out, i, x, lim);
			}
			if (mayaMath::sign_change(v, v1)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, x, v, lim, v1), lim);
			}
		}
		else if (disc < 0) {
			// no minima/maxima, one interval
			if (mayaMath::sign_change(v0, v1)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, 0, v0, lim, v1), lim);
			}
		}
		else {
			// two minima/maxima, three sub-intervals
			disc = sqrt(disc);
			double x0 = (-B - disc) / (2 * A);
			double x1 = (-B + disc) / (2 * A);
			if (x0 > x1) {
				std::swap(x0, x1);
			}
			const double m0 = mayaMath::eval(a, x0);
			const double m1 = mayaMath::eval(a, x1);
			if (mayaMath::sign_change(v0, m0)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, 0, v0, x0, m0), lim);
			}
			if (mayaMath::isZero(m0, eps)) {
				mayaMath::output_root(out, i, x0, lim);
			}
			if (mayaMath::sign_change(m0, m1)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, x0, m0, x1, m1), lim);
			}
			if (mayaMath::isZero(m1, eps)) {
				mayaMath::output_root(out, i, x1, lim);
			}
			if (mayaMath::sign_change(m1, v1)) {
				mayaMath::output_root(out, i,
				                      mayaMath::find_root(a, x1, m1, lim, v1), lim);
			}
		}
		// QUADRATIC
	}
	else if (!mayaMath::isZero(a[1], eps)) {
		double disc = a[2] * a[2] - 4 * a[1] * a[3];
		if (mayaMath::isZero(disc, eps)) {
			const double x = -a[2] / (2 * a[1]);
			const double v = mayaMath::eval(a, x);
			if (mayaMath::isZero(v, eps)) {
				mayaMath::output_root(out, i, x, lim);
			}
		}
		else if (disc > 0) {
			disc = sqrt(disc);
			double x0 = (-a[2] - disc) / (2 * a[1]);
			double x1 = (-a[2] + disc) / (2 * a[1]);
			if (x0 > x1) {
				std::swap(x0, x1);
			}
			mayaMath::output_root(out, i, x0, lim);
			mayaMath::output_root(out, i, x1, lim);
		}
		// LINEAR
	}
	else if (!mayaMath::isZero(a[2], eps)) {
		mayaMath::output_root(out, i, -a[3] / a[2], lim);
	}
	return i;
}



// static const double  double_pi  ;
// static const double  single_pi  ;
// static const double  half_pi    ;
// static const double  quarter_pi;

MVector mayaMath::rgb2hcl(const MColor &color )
{

	MFloatVector result;
	float maxColor =  std::max(color.r, std::max(color.g, color.b)) ;
	float minColor =  std::min(color.r, std::min(color.g, color.b)) ;

	float hue;
	if (maxColor == color.r)
	{
		hue = (color.g - color.b) / (maxColor - minColor);
	}
	else if (maxColor == color.g)
	{
		hue = 2.0 + (color.b - color.r) / (maxColor - minColor);
	}
	else // blue is max
	{
		hue = 4.0 + (color.r - color.g) / (maxColor - minColor);
	}
	hue = (hue / 3.0) *  single_pi;

	float chroma = maxColor - minColor;

	float luminance = (0.2126 * color.r) + (0.7152 * color.g) + (0.0722 * color.b);

	result.x = chroma * cos(hue) ;
	result.y = chroma * sin(hue);
	result.z = luminance;

	return result;
}



float mayaMath::interp(
	const MFloatArray &values,const float &param)
{
	int len = values.length();
	int last = (len - 1);
	if (param >= 1.0f)
	{
		return values[last];
	}
	else if (param <= 0.0f)
	{
		return values[0];
	}
	float t = param * last;
	float r = t - floor(t);
	int lindex = int(t);
	return (values[lindex] * (1 - r)) +
		(values[(lindex + 1)] * (r));
}



MFloatMatrix mayaMath::rotationOnly(const MFloatMatrix & rhs){
	MMatrix mat(rhs.matrix);
	mat =  rotationOnly(mat);
	return MFloatMatrix(mat.matrix);
}

MMatrix mayaMath::rotationOnly(const MMatrix & rhs){
	const double scale[3] = {1.0,1.0,1.0};
	const double shear[3] = {0.0,0.0,0.0};
	MTransformationMatrix mat(rhs);
	mat.setScale(scale, MSpace::kTransform);
	mat.setShear(shear, MSpace::kTransform);
	mat.setTranslation(MVector::zero, MSpace::kTransform);
	return mat.asMatrix();
}







void mayaMath::rgbToHsv(
    const MColor &rgb,
    MColor &result)
{

    
    const float &red = rgb[0];
    const float &green = rgb[1];
    const float &blue = rgb[2];

    float maxval = std::max(red, std::max(green, blue));
    float minval = std::min(red, std::min(green, blue));

    float difference = maxval - minval;

    float hue, sat;
    if (difference == 0)
        hue = 0;
    else if (red == maxval)
        hue = fmod(((60 * ((green - blue) / difference)) + 360), 360.0);
    else if (green == maxval)
        hue = fmod(((60 * ((blue - red) / difference)) + 120), 360.0);
    else if (blue == maxval)
        hue = fmod(((60 * ((red - green) / difference)) + 240), 360.0);

    result[0] = hue;

    if (maxval == 0)
        sat = 0;
    else
        sat = (difference / maxval);

    result[1] = (sat);

    result[2] = (maxval);
    result[3] = rgb[3];

}

void mayaMath::hsvToRgb(
    const MColor &hsv,
    MColor &result)
{

 
    float hue = std::min(std::max(hsv[0], 0.0f), 360.0f);
    float sat = std::min(std::max(hsv[1], 0.0f), 1.0f);
    float val = std::min(std::max(hsv[2], 0.0f), 1.0f);
    
    float C = sat*val;
    float X = C*(1-abs(fmod(hue/60.0, 2)-1));
    float m = val-C;
    float r,g,b;

    if(hue >= 0 && hue < 60){
        r = C,g = X,b = 0;
    }
    else if(hue >= 60 && hue < 120){
        r = X,g = C,b = 0;
    }
    else if(hue >= 120 && hue < 180){
        r = 0,g = C,b = X;
    }
    else if(hue >= 180 && hue < 240){
        r = 0,g = X,b = C;
    }
    else if(hue >= 240 && hue < 300){
        r = X,g = 0,b = C;
    }
    else{
        r = C,g = 0,b = X;
    }
    r+=m;
    g+=m;
    b+=m;
    result = MColor(r,g,b,hsv[3]);

}









