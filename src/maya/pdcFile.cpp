#include <maya/MBoundingBox.h>
#include "errorMacros.h"
#include "pdcAttr.h"
#include "pdcFile.h"



/////////////////////////////////////////////////////////////////////////////
pdcFile::pdcFile ( const MString &fn, MStatus &st ):
	m_numParticles(0), m_numAttr(0)
{


	m_filename = fn;
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);

	if (is.fail())  {
		st = MS::kFailure;
		return;
	}


	MString fType = "PDC ";
	const char *fTypeChar = fType.asChar();
	is.read((char *)fTypeChar , ( 4 * (sizeof(char))));
	if (MString(fTypeChar, 4) != fType) {st = MS::kFailure; return;}

	int numAttr;


	st = mayaIoUtil::readValue(m_version, is) ;
	if (st.error()) { return; }

	st = mayaIoUtil::readValue(m_endian, is) ;
	if (st.error()) { return; }

	st = mayaIoUtil::readValue(m_bitInfo1, is) ;
	if (st.error()) { return; }

	st = mayaIoUtil::readValue(m_bitInfo2, is) ;
	if (st.error()) { return; }

	st = mayaIoUtil::readValue(m_numParticles, is) ;
	if (st.error()) { return; }

	st = mayaIoUtil::readValue(numAttr, is) ;
	if (st.error()) { return; }



	//cerr << "pdcFile - reading data points" << endl;
	for (int i = 0; i < numAttr; i++) {


		int nameLength;
		st = mayaIoUtil::readValue(nameLength, is) ;
		if (st.error()) { return; }
		MString attrName("X", nameLength);
		st = mayaIoUtil::readValue(attrName, is) ;
		if (st.error()) { return; }
		if (attrName == "ghostFrames")  {
			//cerr << "what the hell is ghostFrames? " << endl;
			st = MS::kSuccess; return;
		}


		int attrType;
		st = mayaIoUtil::readValue(attrType, is) ; mser;

		//cerr << "attrType:" << attrType << " - attrName" << attrName << " " << i << " of " << numAttr << " attrs" << endl;


		m_attrMap[attrName] = pdcAttr(  attrName,  attrType,  int(is.tellg()) , i);
		//cerr << "made an attrmap " <<   int(is.tellg()) << endl;
		if (i < (numAttr - 1)) {
			switch (attrType) {
				case pdcAttr::kIntAttr: 			is.seekg(sizeof(int) , ios::cur); 							break;
				case pdcAttr::kIntArrayAttr:		is.seekg(sizeof(int) * m_numParticles , ios::cur);			break;
				case pdcAttr::kDoubleAttr:			is.seekg(sizeof(double) , ios::cur); 							break;
				case pdcAttr::kDoubleArrayAttr:		is.seekg(sizeof(double) * m_numParticles , ios::cur);
					break;
				case pdcAttr::kVectorAttr:			is.seekg(sizeof(double) * 3 , ios::cur); 						break;
				case pdcAttr::kVectorArrayAttr:		is.seekg(sizeof(double) * 3 * m_numParticles, ios::cur);
					break;
			}
		}
	}
	//cerr << "pdcFile - done reading data points" << endl;
	is.close();
}


pdcFile::~pdcFile() {
}

void pdcFile::getHeaderInfo(int &version, int &endian, int &bitinfo1, int &bitinfo2) const
{
	version = m_version;
	endian = m_endian;
	bitinfo1 = m_bitInfo1;
	bitinfo2 = m_bitInfo2;
}

int pdcFile::fileNumber(MStatus *st) const {
	MStringArray sa;
	m_filename.split('.', sa);
	unsigned sl = sa.length();
	if ( sl < 2) {
		*st = MS::kFailure;
		return 0;
	}
	return (sa[(sl - 2)]).asInt();
}

int pdcFile::particleCount() const {
	return m_numParticles;
}

int pdcFile::attributeCount() const {
	return int(m_attrMap.size());
}

MStringArray pdcFile::getNames() const {
	MStringArray names;
	pdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter != m_attrMap.end()) {
		if (iter->second.isValid()) { names.append(iter->first); }
		iter++;
	}
	return names;
}

MStringArray pdcFile::getSortedNames() const {

	MStringArray names(m_attrMap.size(), "X");
	pdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter != m_attrMap.end()) {
		if (iter->second.isValid()) { names[(iter->second.m_dataIndex)] = iter->first; }
		iter++;
	}
	return names;
}

MStringArray pdcFile::getTypes() const {
	MStringArray types;
	pdcAttrMap::const_iterator iter = m_attrMap.begin();
	while (iter != m_attrMap.end()) {
		if (iter->second.isValid()) { types.append(iter->second.m_attrTypeStr); }
		iter++;
	}
	return types;
}


bool pdcFile::attributeExists(const MString &name) const {
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	return (iter != m_attrMap.end());
}

pdcAttr::PdcAttrType pdcFile::typeFromName (const MString &name) const {

	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) {
		return pdcAttr::kInvalid;
	}
	return iter->second.m_attrType;
}

MString pdcFile::typeStringFromName (const MString &name) const {

	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) {
		return "not found";
	}
	return iter->second.m_attrTypeStr;
}


/////////// GET DATA BY NAME /////////////////////////////////////
MStatus pdcFile::getData(int &data, const MString &name)  const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kIntAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}

MStatus pdcFile::getData(MIntArray &data, const MString &name) const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kIntArrayAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	data.setLength(m_numParticles);
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}

MStatus pdcFile::getData(double &data, const MString &name)  const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kDoubleAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}

MStatus pdcFile::getData(MDoubleArray &data, const MString &name)  const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kDoubleArrayAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	data.setLength(m_numParticles);
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}

MStatus pdcFile::getData(MVector &data, const MString &name) const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kVectorAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}

MStatus pdcFile::getData(MVectorArray &data, const MString &name)  const {
	std::ifstream		is;
	is.open(m_filename.asChar(),  ios::in | ios::binary);
	pdcAttrMap::const_iterator iter = m_attrMap.find(name);
	if (iter == m_attrMap.end()) { return MS::kUnknownParameter; }
	if ( iter->second.m_attrType != pdcAttr::kVectorArrayAttr)  { return MS::kUnknownParameter; }
	is.seekg( iter->second.m_dataPos , ios::beg);
	if (is.fail()) { return MS::kFailure; }
	data.setLength(m_numParticles);
	MStatus st = mayaIoUtil::readValue(data, is) ; msert;
	is.close();
	return MS::kSuccess;
}


MStatus pdcFile::boundingBox(JBoundingBox &bb)  {

	MStatus st = MS::kSuccess;
	MBoundingBox mbb;
	st =  pdcFile::boundingBox(mbb); msert;
	JFloatVector fmin(float(mbb.min().x) , float(mbb.min().y) , float(mbb.min().z) );
	JFloatVector fmax(float(mbb.max().x) , float(mbb.max().y) , float(mbb.max().z) );
	bb = JBoundingBox(fmin, fmax);
	return MS::kSuccess;

}

MStatus pdcFile::boundingBox(MBoundingBox &bb)  {
	MVectorArray points;
	MDoubleArray rads;
	MStatus st;

	st = getData(points, MString("position")); msert;
	unsigned plen = points.length() ;
	if (!plen) { return MS::kFailure; }

	bb =  MBoundingBox(MPoint(points[0]) , MPoint(points[0]) );

	st = getData(rads, MString("radusPP"));
	if (rads.length() == plen) {
		for (unsigned i = 0; i < plen; i++) {
			MPoint pt(points[i]);
			MVector radVec(rads[i], rads[i], rads[i] );
			bb.expand( pt - radVec );
			bb.expand( pt + radVec );
		}
	}
	else {
		for (unsigned i = 0; i < plen; i++) {
			MPoint pt(points[i]);
			bb.expand( pt );
		}
	}
	return MS::kSuccess;
}
