/*
 *  pdcAttr.cpp
 *  jtools
 *
 *  Created by Julian Mann on 15/02/2007.
 *  Copyright 2007 hoolyMama. All rights reserved.
 *
 */

#include "pdcAttr.h"



pdcAttr::pdcAttr()
:m_attrName(""),
m_attrTypeStr("invalid"),
m_attrType(pdcAttr::kInvalid),
m_dataPos(0)
{
}

pdcAttr::pdcAttr(const MString& name, int type, int pos , int i)
	:m_attrName(name), m_dataPos(pos), m_dataIndex(i)
 {
	if (( type >= 0 ) && ( type <= 5)) {
		m_attrType = (PdcAttrType(type));
	} else {
		m_attrType = pdcAttr::kInvalid;
	}
	
	
	enum PdcAttrType{kIntAttr, kIntArrayAttr, kDoubleAttr, kDoubleArrayAttr, kVectorAttr, kVectorArrayAttr, kInvalid };
	if ( m_attrType == pdcAttr::kIntAttr) m_attrTypeStr = "int";
	else if ( m_attrType == pdcAttr::kIntArrayAttr) m_attrTypeStr = "intArray";
	else if ( m_attrType == pdcAttr::kDoubleAttr) m_attrTypeStr = "double";
	else if ( m_attrType == pdcAttr::kDoubleArrayAttr) m_attrTypeStr = "doubleArray";
	else if ( m_attrType == pdcAttr::kVectorAttr) m_attrTypeStr = "vector";
	else if ( m_attrType == pdcAttr::kVectorArrayAttr) m_attrTypeStr = "vectorArray";
	else m_attrTypeStr = "invalid"; // this can't happen - I hope
	 
}
	
pdcAttr::~pdcAttr() {}

