

#ifndef errorMacros_h
#define errorMacros_h



#include <maya/MStatus.h>

// print error details
#define JPMDBG cerr << "DBG LINE " <<  __LINE__  << " File: " << __FILE__  << endl;

// print error details
#define mser                                                            \
    if (MS::kSuccess != st) {                                       \
        cerr << "    in File: " << __FILE__                         \
             << ", at Line: " << __LINE__ << endl;                  \
        st.perror("Error");                                 \
    }
// print error details and return status
#define msert                                                           \
    if (MS::kSuccess != st) {                                       \
        cerr << "    in File: " << __FILE__                         \
             << ", at Line: " << __LINE__ << endl;                  \
        st.perror("Error");                                 \
        return st;                                                  \
    }

// #define erv                                                          \
//     if (MS::kSuccess != st) {                                        \
//         cerr << "    in File: " << __FILE__                          \
//           << ", at Line: " << __LINE__ << endl;                  \
//         st.perror("Error");                                          \
//         return ;                                                 \
//     }

// #define er1                                                          \
//     if (MS::kSuccess != st) {                                        \
//         cerr << "    in File: " << __FILE__                          \
//           << ", at Line: " << __LINE__ << endl;                  \
//         st.perror("Error");                                      \
//         return 1;                                                    \
//     }


#define tracer \
    cerr << "Line: " <<  __LINE__ <<  "File: " << __FILE__   << endl;


/*
#define mser  ;

#define msert     if (MS::kSuccess != st)   return st;
*/

#endif
