
#ifndef mayaFX_H
#define mayaFX_H


#include <maya/MDataBlock.h>
#include <maya/MObject.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MFloatArray.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MTime.h>

class mayaFX {

public:


	static bool sampleTexture(
		const MObject &node,
		const MObject &attr,
		MVectorArray &samplePoints,
		MFloatArray &resColors
		);

	static bool sampleTexture(
		const MObject &node,
		const MObject &attr,
		MFloatPointArray &samplePoints,
		MFloatArray &resColors
		) ;

	static bool sampleTexture(
		const MObject &node,
		const MObject &attr,
		MFloatArray &uCoords,
		MFloatArray &vCoords,
		MFloatPointArray &samplePoints,
		MFloatVectorArray &sampleNormals,
		MFloatArray &resValues
		);

	static bool sampleTexture(
		const MObject &node,
		const MObject &attr,
		MFloatArray &uCoords,
		MFloatArray &vCoords,
		MFloatPointArray &samplePoints,
		MFloatVectorArray &sampleNormals,
		MFloatVectorArray &resColors
		) ;

	static MStatus collectExternalForces(
		const MObject &node,
		MDataBlock &data,
		const MVectorArray &positions,
		const MVectorArray &velocities,
		const MDoubleArray &masses,
		const MTime &dT, 
		MVectorArray & force
		) ;

	static bool getDynTriangleArrays(
		double dtRecip,
		const MVectorArray &lastVelocity,
		 unsigned lastTriangleCount,
		MObject &sweptData,
		MVectorArray &centers,
		MVectorArray &velocities,
		MVectorArray &accelerations,
		MDoubleArray &areas,
		double &totalArea,
		MFloatArray &uCoords,
		MFloatArray &vCoords,
		MFloatPointArray &samplePoints,
		MFloatVectorArray &sampleNormals
		) ;

	static bool getDynTriangleArrays(
		double dtRecip,
		const MVectorArray &lastVelocity,
		 unsigned lastTriangleCount,
		MObject &sweptData,
		MVectorArray &centers,
		MVectorArray &velocities,
		MVectorArray &accelerations,
		MDoubleArray &areas,
		double &totalArea
		);

	static bool getDynTriangleArrays(
		double dtRecip,
		const MVectorArray &lastVelocity,
		 unsigned lastTriangleCount,
		MObject &sweptData,
	MVectorArray &centers0,
	MVectorArray &centers1,
		MVectorArray &velocities,
		MVectorArray &accelerations,
		MFloatArray &uCoords,
		MFloatArray &vCoords,
		MFloatPointArray &samplePoints,
		MFloatVectorArray &sampleNormals
		) ;

	static bool getDynTriangleArrays(
		double dtRecip,
		const MVectorArray &lastVelocity,
		 unsigned lastTriangleCount,
		MObject &sweptData,
	MVectorArray &centers0,
	MVectorArray &centers1,
		MVectorArray &velocities,
		MVectorArray &accelerations
		);
};


#endif



