/***************************************************************************
lineTreeData.cpp  -  description
-------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    this is the maya data object containing a lineKdTree so that it can be passed
    through the dependency graph.
    It also contains some methods to make calls to the lineKdTree methods
	***************************************************************************/

// Copyright (C) 2000 hoolyMama

// Author: Julian Mann
#include <maya/MDataHandle.h>

#include "lineKdTree.h"
#include "lineData.h"
#include "lineTreeData.h"
#include "errorMacros.h"
// #include "splineVectors.h"
#include "jMayaIds.h"

#define EPSILON 0.0000001

const double bigNum = 99999999999.0  ;

MTypeId lineTreeData::id( k_lineTreeData );
MString lineTreeData::typeName( "lineTreeData" );

lineTreeData::lineTreeData()
{
	m_pTree = 0;

}

lineTreeData::~lineTreeData()
{
}

void *lineTreeData::creator()
{
	return new lineTreeData;
}

// the ptKdTree
lineKdTree	*lineTreeData::tree() const {return m_pTree;}

// clean up
void	lineTreeData::clear() {
	if (m_pTree) {delete m_pTree; m_pTree = 0;}
}

// create new tree
void lineTreeData::create(MDataHandle &h) {
	//cerr << "hello" << endl;
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::create" );


	clear();
	m_pTree = new lineKdTree();

	if (m_pTree)  {
		MObject d = h.data();
		MFnDynSweptGeometryData fnG(d, &st);
		if (!(st.error())) {
			st = m_pTree->populate(fnG, 0);
			if (!(st.error())) {
				m_pTree->build();
			}
		}
	}
	// NOTE: there is now a lineKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree
}


void lineTreeData::create(MArrayDataHandle &ah) {
	// create new tree from an array of curves

	MStatus st = MS::kSuccess;
	MString method("lineTreeData::create" );

	clear();

	unsigned n = ah.elementCount();

	// make a new tree
	m_pTree = new lineKdTree();
	if (m_pTree)  {

		for (unsigned i = 0; i < n; i++, ah.next()) {
			int el = ah.elementIndex(&st);
			MDataHandle h = ah.inputValue();
			MObject  d = h.data();
			MFnDynSweptGeometryData fnG(d, &st);
			if (!(st.error())) {
				m_pTree->populate(fnG, el);
			}
		}
		m_pTree->build();
	}
	// NOTE: there is now a lineKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree
}



void lineTreeData::create(
  MArrayDataHandle &ah,
  const MObject &childGeomPlug,
  const MObject &childActivePlug,
  const MObject &childTangentPlug,
  const MObject &childNormalPlug,
  const MObject &childCircularPlug,
  MVectorArray &forces
) {
	// create new tree from an array of curves
	// the curves in thi case are children of a compound array
	// also - the ids will  be their physical index
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::create" );

	clear();

	unsigned n = ah.elementCount();
	unsigned curveIndex = 0;
	// make a new tree
	m_pTree = new lineKdTree();
	if (m_pTree)  {

		for (unsigned i = 0; i < n; i++, ah.next()) {
			//int el = ah.elementIndex(&st);
			MDataHandle hCmp = ah.inputValue();
			MDataHandle hGeo = hCmp.child(childGeomPlug);
			bool active = hCmp.child(childActivePlug).asBool();
			if (active) {
				MObject  d = hGeo.data();
				MFnDynSweptGeometryData fnG(d, &st);
				if (!(st.error())) {
					m_pTree->populate(fnG, curveIndex);
					curveIndex++;
					double tan = hCmp.child(childTangentPlug).asDouble();
					double nor = hCmp.child(childNormalPlug).asDouble();
					double cir = hCmp.child(childCircularPlug).asDouble();
					MVector v(tan, nor, cir);

					if (!(v.isEquivalent(MVector::zero))) {
						double t = fabs(v.x) + fabs(v.y) + fabs(v.z);
						t = 1.0 / t;

						forces.append(v * t);
					}
					else {
						forces.append(MVector::xAxis);
					}
				}
			}
		}
		m_pTree->build();
	}
	// NOTE: there is now a lineKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree
}



int lineTreeData::size() {
	if (m_pTree)
	{ return m_pTree->size(); }
	else
	{ return 0; }
}



bool lineTreeData::closestPointOnLineInRadius(
  const MPoint &searchPoint,
  double rad,
  lineData &result
)  const {
	MPoint c = searchPoint;
	double searchRad = rad;
	m_pTree->closestLine( m_pTree->root(), c , searchRad,  result);
	return (searchRad < rad) ;
}


MStatus lineTreeData::closestPointOnLineInRadius(
  const MPoint &searchPoint,
  double radius,
  MPoint &resultPoint,
  MVector &resultTangent,
  MVector &resultVelocity,
  double &resultU,
  double &resultDist
)  const {

	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointOnLine" );

	MPoint c = MPoint(searchPoint);
	double searchRad = radius;
	lineData lineBox;
	m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
	if (searchRad < radius) {  // we found a point
		resultU = lineBox.calcU();
		resultPoint =  lineBox.cachedPoint() ;
		resultDist = searchRad ;
		resultVelocity = lineBox.calcVelocity();
		resultTangent = lineBox.tangent() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point
		st = MS::kFailure;
	}
	return st;
}




MStatus lineTreeData::closestPointOnLine(
  const MPoint &searchPoint,
  MPoint &resultPoint,
  MVector &resultTangent,
  MVector &resultVelocity,
  double &resultU,
  double &resultParam
) const {

	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointOnLine" );

	double searchRad = bigNum;
	lineData lineBox;
	//cerr << "about to find closest line" << endl;

	m_pTree->closestLine( m_pTree->root(), searchPoint , searchRad,  lineBox);

	//cerr << "done search" << endl;
	if (searchRad < bigNum) {  // we found a point
		resultU = lineBox.calcU();
		resultPoint = MPoint(lineBox.cachedPoint()) ;
		resultParam = lineBox.cachedParam() ;
		resultTangent = lineBox.tangent() ;
		resultVelocity = lineBox.calcVelocity();

		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}


MStatus lineTreeData::closestPointOnLine(
  const MVector &searchPoint,
  MVector &resultPoint,
  double &resultU,
  double &resultParam
) const {

	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointOnLine" );

	MPoint c = MPoint(searchPoint);
	double searchRad = bigNum;
	lineData lineBox;
	m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
	if (searchRad < bigNum) {  // we found a point
		resultU = lineBox.calcU();
		resultPoint =  lineBox.cachedPoint() ;
		resultParam = lineBox.cachedParam() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

MStatus lineTreeData::closestPointOnLine(
  const MPoint &searchPoint,
  MVector &resultPoint,
  double &resultU,
  double &resultParam
) const {

	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointOnLine" );

	double searchRad = bigNum;
	lineData lineBox;
	m_pTree->closestLine( m_pTree->root(), searchPoint , searchRad,  lineBox);
	if (searchRad < bigNum) {  // we found a point
		resultU = lineBox.calcU();
		resultPoint = lineBox.cachedPoint() ;
		resultParam = lineBox.cachedParam() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}




MStatus lineTreeData::closestPointOnLine(
  const MVector &searchPoint,
  MVector &resultPoint
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointOnLine" );

	MPoint c = MPoint(searchPoint);
	double searchRad = bigNum;
	lineData lineBox;
	m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
	if (searchRad < bigNum) {  // we found a point
		resultPoint =  lineBox.cachedPoint() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

MStatus lineTreeData::closestPointsOnLine(
  const MVectorArray &searchPoints,
  MVectorArray &resultPoints,
  MDoubleArray &resultUs,
  //MIntArray &resultIds,
  MDoubleArray &resultParams
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointsOnLine" );

	unsigned len = searchPoints.length();
	resultPoints.setLength(len) ;
	//resultIds.setLength(len) ;
	resultUs.setLength(len) ;
	resultParams.setLength(len) ;


	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		lineData lineBox;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < bigNum) {  // we found a point
			resultPoints.set( lineBox.cachedPoint() , i);
			resultUs.set(lineBox.calcU(), i);
			//resultIds.set(lineBox.id() ,i);
			resultParams.set( lineBox.cachedParam() , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}

MStatus lineTreeData::closestPointsOnLine(
  const MVectorArray &searchPoints,
  MVectorArray &resultPoints
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestPointsOnTri" );

	unsigned len = searchPoints.length();
	resultPoints.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		lineData lineBox;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < bigNum) {  // we found a point
			resultPoints.set( lineBox.cachedPoint() , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}



MStatus lineTreeData::blendClosestTangentsOnLine(
  const MVectorArray &searchPoints,
  double distance,
  double attenuation,
  MVectorArray &resultTangents
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::blendClosestTangentsOnLine" );

	unsigned len = searchPoints.length();
	resultTangents.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = distance;
		lineData lineBox;
		double blendVal;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < distance) {  // we found a point
			if (attenuation == 0.0) {
				resultTangents.set( lineBox.tangent() , i);
			}
			else if (attenuation == 1.0) {
				blendVal = 1.0 - (searchRad / distance);
				MVector newVec = (lineBox.tangent() * blendVal) + (resultTangents[i] * (1.0 - blendVal));
				resultTangents.set( newVec , i);
			}
			else {
				blendVal = pow(1.0 - (searchRad / distance), attenuation);
				MVector newVec = (lineBox.tangent() * blendVal) + (resultTangents[i] * (1.0 - blendVal));
				resultTangents.set( newVec , i);
			}
		}
	}
	return st;
}





MStatus lineTreeData::blendClosestTangentsOnLine(
  const MVectorArray &searchPoints,
  const MVectorArray &forces,
  double distance,
  double attenuation,
  MVectorArray &resultTangents
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::blendClosestTangentsOnLine" );

	unsigned len = searchPoints.length();
	unsigned forcesLen = forces.length();
	//cerr << "forcesLen" << forcesLen << endl;
	//cerr << forces << endl;
	resultTangents.setLength(len) ;

	MPoint c   ; // the center of the search sphere


	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = distance;
		lineData lineBox;
		double blendVal;
		MVector v;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < distance) {  // we found a point
			// get the force components
			//cerr << "id " << lineBox.id() << endl;
			v = MVector::xAxis;
			if (unsigned(lineBox.id()) < forcesLen) {
				v = forces[lineBox.id()];
			}
			const MVector &tan = lineBox.tangent();
			MVector theVector = tan;
			if (!(v.isEquivalent(MVector::xAxis))) { // if anything but tangent only

				const MVector &hit =  lineBox.cachedPoint();

				MVector norm = (c - hit);
				norm.normalize();
				MVector circular = (tan ^ (c - hit)).normal();

				// alternative that will act differently at the end of the line
				// MVector norm = circular^tan;

				theVector = ((tan * v.x) + (norm * v.y) + (circular * v.z)).normal();

			}
			if (attenuation == 0.0) {
				resultTangents.set( theVector, i);
			}
			else if (attenuation == 1.0) {
				blendVal = 1.0 - (searchRad / distance);
				MVector newVec = (theVector * blendVal) + (resultTangents[i] * (1.0 - blendVal));
				resultTangents.set( newVec , i);
			}
			else {
				blendVal = pow(1.0 - (searchRad / distance), attenuation);
				MVector newVec = (theVector * blendVal) + (resultTangents[i] * (1.0 - blendVal));
				resultTangents.set( newVec , i);
			}
		}
	}
	return st;
}


MStatus lineTreeData::closestTangentsOnLine(
  const MFloatVectorArray &searchPoints,
  const MVectorArray &forces,
  double distance,
  double attenuation,
  MFloatPointArray &resultTangents,
  MIntArray &affected
) const {


	/*
	 This method is optimized for animated guides.
	 The resulting MFloatPoints have the attenuated weight stored in the w component
	 */


	MStatus st = MS::kSuccess;
	MString method("lineTreeData::blendClosestTangentsOnLine" );

	unsigned len = searchPoints.length();
	unsigned forcesLen = forces.length();
	//cerr << "forcesLen" << forcesLen << endl;
	//cerr << forces << endl;
	resultTangents.clear() ;
	affected.clear() ;

	MPoint c   ; // the center of the search sphere

	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = distance;
		lineData lineBox;
		MVector v;
		//double blendVal;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < distance) {  // we found a point
			// get the force components
			//cerr << "id " << lineBox.id() << endl;

			/*  In the forces array, remember - tangent issignified by xAxis vector, normal = yAxis, circular = zAxis  */

			v = MVector::xAxis;
			if (unsigned(lineBox.id()) < forcesLen) {
				v = forces[lineBox.id()];
			}
			const MVector &tan = lineBox.tangent();
			MVector theVector = tan;

			if (!(v.isEquivalent(MVector::xAxis))) { // if anything but tangent only

				const MVector &hit =  lineBox.cachedPoint();

				MVector norm = (c - hit);
				norm.normalize();
				MVector circular = (tan ^ (c - hit)).normal();

				// alternative that will act differently at the end of the line
				// MVector norm = circular^tan;

				theVector = ((tan * v.x) + (norm * v.y) + (circular * v.z)).normal();

			}

			MFloatPoint p(float(theVector.x), float(theVector.y), float(theVector.z) );

			if (attenuation == 1.0) {
				p.w = 1.0f - float(searchRad / distance);
			}
			else {
				p.w = pow(1.0f - float(searchRad / distance), float(attenuation));
			}

			resultTangents.append(p);
			affected.append(i);
		}
	}
	return st;
}








MStatus lineTreeData::closestTangentsOnLine(
  const MVectorArray &searchPoints,
  const MVectorArray &forces,
  MVectorArray &resultTangents
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestTangentsOnLine" );

	unsigned len = searchPoints.length();
	unsigned forcesLen = forces.length();
	resultTangents.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		lineData lineBox;
		MVector v;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < bigNum) {  // we found a point
			v = MVector::xAxis;
			if (unsigned(lineBox.id()) < forcesLen) {
				v = forces[lineBox.id()];
			}
			const MVector &tan = lineBox.tangent();
			MVector theVector = tan;
			if (!(v.isEquivalent(MVector::xAxis))) { // if anything but tangent only

				const MVector &hit =  lineBox.cachedPoint();

				MVector norm = (c - hit);
				norm.normalize();
				MVector circular = (tan ^ (c - hit)).normal();

				// alternative that will act differently at the end of the line
				// MVector norm = circular^tan;

				theVector = ((tan * v.x) + (norm * v.y) + (circular * v.z)).normal();

			}

			resultTangents.set(theVector , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}


MStatus lineTreeData::closestTangentsOnLine(
  const MVectorArray &searchPoints,
  MVectorArray &resultTangents
) const {
	MStatus st = MS::kSuccess;
	MString method("lineTreeData::closestTangentsOnLine" );

	unsigned len = searchPoints.length();
	resultTangents.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		lineData lineBox;
		m_pTree->closestLine( m_pTree->root(), c , searchRad,  lineBox);
		if (searchRad < bigNum) {  // we found a point
			resultTangents.set( lineBox.tangent() , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}

void lineTreeData::copy(const MPxData &otherData)
{
	m_pTree = ((const lineTreeData &)otherData).tree();
}


lineTreeData &lineTreeData::operator=(const lineTreeData &otherData ) {
	if (this != &otherData ) {
		m_pTree = otherData.tree();
	}
	return *this;
}


