/***************************************************************************
ptTreeData.h  -  description
-------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
	***************************************************************************/

#ifndef _ptTreeData
#define _ptTreeData

//
// Copyright (C) 2006 hoolyMama
//
// Author: Julian Mann

#include <maya/MIOStream.h>

#include <vector>
#include <math.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include "ptKdTree.h"

class ptTreeData : public MPxData
{
public:
	// constructor
	ptTreeData();
	
	// destructor
	virtual	~ptTreeData();
	
	// creator
	static	void*		creator();
	
	// copy constructor
	virtual	void		copy(const MPxData&);
	
	// operator =
	ptTreeData& operator=(const ptTreeData &);
	
	// type
	virtual	MTypeId		typeId() const {return id;}
	static	MTypeId		id;
	
	// name
	virtual	MString		name() const {return typeName;}
	static	MString		typeName;
	
	
	// create some new member data
	void create(MDataHandle &h, int splitFunc=0); // points array
	void create(MDataHandle &hP,MDataHandle &hV,MDataHandle &hN, int splitFunc=0); //  arrays
	
	// clear member data
	void	clear();
	
	// accessor
	ptKdTree*	tree() const;
	
	// accessor
	// SOUP * soup() const ;
	

MStatus closestDist(
    const MVector &searchPoint,
    double &result
) const ;

// This is useful for creating a cellular shader
MStatus closest2Dists(
								const MVector &searchPoint,
								MDoubleArray &result
								) const;

 MStatus closest2Pts(
								const MVector &searchPoint,
								MVectorArray &result
								) const;
/*
void closestNPoints(const MVector &searchPoint,
								int numPts,
								MVectorArray &pts,
								MDoubleArray &dists)  const   ;
*/						
	int size();
	
private:
		
		ptKdTree 	*	m_pTree; 			// the ptKdTree
	
};

#endif


