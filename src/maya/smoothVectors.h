/*
 *  smoothVectors.h
 *  feather
 *
 *  Created by Julian Mann on 07/11/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#ifndef smoothVectors_H
#define smoothVectors_H

#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>



#include "ptKdTree.h"






inline MStatus smoothVectors	( const MIntArray & neighborIds, 
							 const MDoubleArray & power,						
							 int steps, 
							 MVectorArray &vectors) {
	
	// if there are say 5 neighbors per point, the neighborIds array will have numPoints X 5 elements

	
	
	MStatus st;
	MString method("geom::smoothVectors");

	
	unsigned vl = vectors.length();
	
	//if  (neighborIds.length() != (vl*neighbors)) return MS::kUnknownParameter;
	
	if (neighborIds.length() <  vl) return MS::kUnknownParameter;
	if ( neighborIds.length() % vl  ) return MS::kUnknownParameter;
	
	unsigned neighbors = neighborIds.length() / vl;
	
	if (!neighbors) return MS::kUnknownParameter;

	double oneOverNeighbors = 1.0 / double(neighbors);
	MVectorArray buffer(vl);
	
	
	for (int s = 0;s<steps;s++){
		
		for (unsigned i = 0; i<vl; i++ ) {
			
			buffer[i] = MVector::zero;
			unsigned neighborIdsIndex = i*neighbors;
			for (unsigned j=0;j<neighbors;j++){
				int nid = neighborIds[ (neighborIdsIndex+j) ];
				if (nid >= 0) {
					buffer[i] += vectors[nid];
				}
			}
			
			//cerr << "power[i] = " << power[i] << "  1/n = " << oneOverNeighbors << " buffer[i] = " << 	buffer[i] << endl;
			buffer[i] = buffer[i] * power[i] * oneOverNeighbors;
		}
		
		
		for (unsigned i = 0; i<vl; i++ ) {	
			vectors[i] = (vectors[i].normal() +  buffer[i]).normal();
		}
	}

	return MS::kSuccess;
}


inline MStatus smoothVectors(
							  const MVectorArray & points, 
							  unsigned neighbors,
							  const MVectorArray & normals,
							  const MDoubleArray & power,
							  double thresh,							
							  int steps, 
							  MVectorArray &vectors) {
	 
	 
	 
	 // thresh is the threshold 
	 
	 
	 //MStatus st;
	 
	const double epsilon = 1e-12;
	 const double bigNum = 10e+37 ;

 
	 MStatus st;
	 MString method("geom::smoothVectors");
	 //cerr << method << "neighbors" << endl;
	 
	 unsigned pl = points.length();
	 if (normals.length() != pl) return MS::kFailure;
	 if (vectors.length() != pl) return MS::kFailure;
	 if (!neighbors) return MS::kUnknownParameter;
	 //if (triangleIds.length() != pl) return MS::kFailure;
	 double oneOverNeighbors = 1.0 / double(neighbors);
	 ptKdTree * pTree = new ptKdTree();
	 if (pTree) { 
		 //cerr << "smoothVectors 1" << endl;
		 pTree->populate(points, vectors, normals);
		 pTree->build(); 
		 //cerr << "smoothVectors 2" << endl;
		 
		 
		 PD_CLUSTERS * neighborsArray = new PD_CLUSTERS;
		 if (neighborsArray) {
			 //cerr << "smoothVectors 3" << endl;
			 // now get a neighbors cluster for every point
			 
			 for (unsigned i = 0; i<pl; i++ ) {
				 // In this loop, we first make a priority_queue
				 // container and fill it with the n closest neighbors 
				 // to each point.
				 // We will need to retain the data in the queue for the
				 // smoothing steps, however the queue makes it difficult
				 // to access the values without popping the data off.
				 
				 // For this reason, as soon as we have the data, we will "convert" it
				 // to a PT_LIST (a vector of ptData) - then each of these vectors of ptData will
				 // be appended to an array, which we call neighbors.
				 ////////////////////////////////////////////
				 PT_LIST * cluster = 0;
				 if (power[i] > epsilon) {
					 //cerr << "pos power" << endl;
					 KNN_PD_QUEUE * q = new KNN_PD_QUEUE;
					 
					 for (unsigned p = 0;p<neighbors;p++) {
						 knnSearchPdData k;
						 k.dist = bigNum;
						 k.pd=0;
						 q->push(k);
					 }
					 // do search here - fill q with real pd neighbors
					 
					 // (currently this is failing for small point sets)
					 //pTree->closestNPts(pTree->root(),points[i],q );
					 pTree->closestNPtsWithinNormalThresh(pTree->root(),points[i],q , normals[i], thresh);
					 
					 // we have a queue - now convert it to a PT_LIST,
					 // append to the array and clean up leftovers
					 cluster = new PT_LIST;
					 while (!q->empty()) {
						 if (q->top().pd) {
							 cluster->push_back(q->top().pd);
						 }
						 q->pop();
					 }
					 delete q;					
				 }
				 neighborsArray->push_back(cluster);
				 ////////////////////////////////////////////
			 }
			 //cerr << "smoothVectors 4" << endl;
			 
			 // now the neighbors array is full
			 PT_LIST * perm = pTree->perm();
			 //cerr << "smoothVectors 5" << endl;
			 
			 for (int s = 0;s<steps;s++){
				 for (unsigned i = 0; i<pl; i++ ) {	
					 //cerr << "smoothVectors 5.1."<< s <<"."<<i<<endl;;
					 // this is where we do the averaging
					 
					 if ((*neighborsArray)[i]) { // if cluster not null
						 MVector & t = vectors[i];
						 //cerr << "t = " << t <<endl;;
						 PT_LIST::iterator iter = ( (*neighborsArray)[i])->begin();
						 //cerr << "h 2 " <<endl;;
						 t=MVector::zero;
						 //cerr <<  "((*neighborsArray)[i])->size()" << ((*neighborsArray)[i])->size() << endl;;
						 while (iter != ((*neighborsArray)[i])->end()) {
							 // iter is a ptData in  the current cluster
							 //cerr << "(*iter) " << (*iter) << endl;
							 //cerr << "getting vel " <<  endl;;
							 
							 t += (*iter)->velocity();	
							 
							 
							 //cerr << "(*iter)->velocity() " << (*iter)->velocity() << endl;;
							 
							 iter++;
						 }
					 }
					 // t is now a vector representing the average direction of neighbor vectors.
					 // t.normalize();
					 
				 }
				 //cerr << "smoothVectors 5.5" << endl;
				 
				 // now we blend the new directions back into the ptData - ready for the next step.
				 PT_LIST::iterator iter = perm->begin();
				 while (iter != perm->end()) {
					 //MVector newVelocity =  (tangents[(*iter)->id()]*power) + ((*iter)->velocity()*(1.0 - power)) ;
					 if ( power[(*iter)->id()] ) { // otherwise leave it as is
						 MVector newVelocity =  ((  (vectors[(*iter)->id()] * power[(*iter)->id()]) * oneOverNeighbors) + (*iter)->velocity()).normal() ;
						 (*iter)->setVelocity(newVelocity);
					 }
					 iter++;
				 }	
				 
				 
			 }	
			 
			 // now put the smoothed values back into the out array
			 /////////////////////////////////////////////////
			 PT_LIST::iterator iter = perm->begin();
			 while (iter != perm->end()) {
				 vectors[(*iter)->id()] = (*iter)->velocity();
				 iter++;
			 }
			 /////////////////////////////////////////////////
			 //	cerr << "smoothVectors 7" << endl;
			 
			 // delete the individual clusters (PT_LISTS)
			 /////////////////////////////////////////////////
			 for (unsigned i = 0; i<pl; i++ ) {
				 if ((*neighborsArray)[i]) { // if cluster not null
					 delete (*neighborsArray)[i];
					 (*neighborsArray)[i]=0; 
				 }
			 }
			 /////////////////////////////////////////////////
			 
			 // delete the array of clusters
			// if (neighborsArray) {
				 delete neighborsArray; 
				 neighborsArray = 0;
			// }
			 
		 }
		 
		 delete pTree; pTree = 0;
		 /////////////////////////////////////////////////
		 //	cerr << "smoothVectors 10" << endl;
		 
	 }
	 return MS::kSuccess;
 }





inline MStatus neighborIds(
							 const MVectorArray & points, 
							 unsigned neighbors,
							 const MVectorArray & normals,
							 double thresh,							
							 MIntArray & neighborIds) {
	
	//MStatus st;
	
	//const double epsilon = 1e-12;
	const double bigNum = 10e+37 ;
	
	unsigned index = 0;
	MStatus st;
	MString method("geom::smoothVectors");
	//cerr << method << "neighbors" << endl;
	
	unsigned pl = points.length();
	if (normals.length() != pl) return MS::kFailure;
	if (!neighbors) return MS::kUnknownParameter;
	neighborIds.setLength(pl*neighbors);
	ptKdTree * pTree = new ptKdTree();
	if (pTree) { 

		
		pTree->populate(points, normals, normals);
		pTree->build(); 


		for (unsigned i = 0; i<pl; i++ ) {
			// PT_LIST * cluster = 0;

			KNN_PD_QUEUE * q = new KNN_PD_QUEUE;
				
			for (unsigned p = 0;p<neighbors;p++) {
				knnSearchPdData k;
				k.dist = bigNum;
				k.pd=0;
				q->push(k);
			}

			pTree->closestNPtsWithinNormalThresh(pTree->root(),points[i],q , normals[i], thresh);
				
			// we have a queue - now get the ids,
			
			while (!q->empty()) {
				if (q->top().pd) {

					neighborIds.set(q->top().pd->id() , index);
					//cluster->push_back(q->top().pd);
				} else {
					neighborIds.set( -1 , index);
				}
				q->pop();
				index++;
			}
			delete q;					

			////////////////////////////////////////////
		}

		
		delete pTree; pTree = 0;
		/////////////////////////////////////////////////
		//	cerr << "smoothVectors 10" << endl;
		
	}
	return MS::kSuccess;
}

 
#endif



