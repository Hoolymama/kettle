#include "texUtils.h"

#include <maya/MString.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MRampAttribute.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>

#include "errorMacros.h"
#include "math.h"

int TexUtils::getA()
{
  return 1;
}

bool TexUtils::hasTexture(const MObject &node, const MObject &attribute)
{
  MString name;
  MStatus st = getTextureName(node, attribute, name);
  if (st.error())
  {
    return false;
  }
  return true;
}

MStatus TexUtils::getTextureName(const MObject &node, const MObject &attribute,
                                 MString &name)
{
  MStatus st;

  MPlugArray plugArray;
  MPlug plug(node, attribute);
  bool hasConnection = plug.connectedTo(plugArray, 1, 0, &st);
  if (st.error() or (!hasConnection))
  {
    return MS::kUnknownParameter;
  }
  name = plugArray[0].name(&st);
  return MS::kSuccess;
}

MStatus TexUtils::sampleSolidTexture(
    const MObject &node,
    const MObject &attribute,
    float scale,
    MFloatPointArray &points,
    MFloatVectorArray &result)
{

  MStatus st;
  MString plugName;
  st = getTextureName(node, attribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }
  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;

  int n = points.length();

  st = MRenderUtil::sampleShadingNetwork(
      plugName, n, false, false, cameraMat,
      &points, 0, 0, 0, &points, 0, 0, 0, result, transparencies);

  if (st.error())
  {
    return MS::kUnknownParameter;
  }
  if (scale != 1.0)
  {
    for (int i = 0; i < n; i++)
    {
      result[i] = MFloatVector(result[i].x * scale, result[i].y * scale, result[i].z * scale);
    }
  }

  return MS::kSuccess;
}

MStatus TexUtils::sampleSolidTexture(
    const MObject &node,
    const MObject &attribute,
    float scale,
    MFloatPointArray &points,
    MFloatArray &result)
{

  MStatus st;
  MString plugName;
  st = getTextureName(node, attribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }
  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  int n = points.length();

  st = MRenderUtil::sampleShadingNetwork(
      plugName, n, false, false, cameraMat,
      &points, 0, 0, 0, &points, 0, 0, 0, colors, transparencies);

  if (st.error())
  {
    return MS::kUnknownParameter;
  }
  result.setLength(n);
  for (int i = 0; i < n; i++)
  {
    result[i] = float(colors[i].x * scale);
  }

  return MS::kSuccess;
}



void TexUtils::sampleSolidTexture(
    const MObject &node,
    const MObject &attribute,
    int quantizeLevels,
    MFloatPointArray &points,
    MIntArray &result,
    bool clamp)
{

  MStatus st;
  MString plugName;
  st = getTextureName(node, attribute, plugName);
  int lastValue = quantizeLevels - 1;
  int n = points.length();
  // default
  MPlug plug(node, attribute);
  float value;;
  plug.getValue(value);
  int intvalue = clamp ?  fmax(0, fmin(roundf(value * lastValue), lastValue)) : roundf(value * lastValue);

  if (st.error())
  {
    result = MIntArray(n, intvalue);
    return;
  }

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  st = MRenderUtil::sampleShadingNetwork(
      plugName, n, false, false, cameraMat,
      &points, 0, 0, 0, &points, 0, 0, 0, colors, transparencies);

  if (st.error())
  {
    result = MIntArray(n, intvalue);
    return;
  }

  result.setLength(n);
  if (clamp)
  {
    for (int i = 0; i < n; i++)
    {
      result[i] = fmax(0, fmin(roundf(colors[i].x * lastValue), lastValue));
    }
  }
  else
  {
    for (int i = 0; i < n; i++)
    {
      result[i] = roundf(colors[i].x * lastValue);
    }
  }
  return;
}


void TexUtils::defaultedSampleSolidTexture(
    const MObject &node,
    const MObject &attribute,
    MFloatPointArray &points,
    MFloatArray &result)
{
  MStatus st;
  MPlug plug(node, attribute);
  bool found = false;
  unsigned num = points.length();
  if (!num)
  {
    return;
  }
  if (hasTexture(node, attribute))
  {
    found = true;
  }

  if (found)
  {
    st = sampleSolidTexture(node, attribute, 1.0f, points, result);
    if (st.error())
    {
      found = false;
    }
  }
  if (!found)
  {
    float value;
    plug.getValue(value);
    result = MFloatArray(num, value);
  }
}



// MStatus TexUtils::sampleSolidTexture() { return MS::kSuccess; }

MStatus TexUtils::sampleUVTexture(const MObject &node, const MObject &attribute,
                                  MFloatArray &uVals,
                                  MFloatArray &vVals, MFloatVectorArray &result)
{

  MStatus st;
  MString plugName;
  st = getTextureName(node, attribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }
  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  // MFloatVectorArray colors;

  int n = uVals.length();

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat,
                                         0, &uVals, &vVals, 0, 0, 0, 0, 0, result, transparencies);
  if (st.error())
  {
    return MS::kUnknownParameter;
  }
  return MS::kSuccess;
}

MStatus TexUtils::sampleUVTexture(
    const MObject &node,
    const MObject &textureAttribute,
    MFloatArray &uVals,
    MFloatArray &vVals,
    const MObject &rampAttribute,
    int low, int high,
    MIntArray &result)
{

  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }

  MRampAttribute remapRamp(node, rampAttribute);
  if (st.error())
  {
    return MS::kFailure;
  }

  int span = high - low;

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  int n = uVals.length();

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat,
                                         0, &uVals, &vVals, 0, 0, 0, 0, 0, colors, transparencies);
  if (st.error())
  {
    return MS::kUnknownParameter;
  }

  result.setLength(n);
  for (int i = 0; i < n; ++i)
  {
    float remappedVal;
    remapRamp.getValueAtPosition(colors[i].x, remappedVal);
    result.set(int((remappedVal * span) + low), i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sampleUVTexture(
    const MObject &node,
    const MObject &textureAttribute,
    MFloatArray &uVals,
    MFloatArray &vVals,
    int range,
    MIntArray &result)
{
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  int n = uVals.length();

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat,
                                         0, &uVals, &vVals, 0, 0, 0, 0, 0, colors, transparencies);
  if (st.error())
  {
    return MS::kUnknownParameter;
  }

  result.setLength(n);
  for (int i = 0; i < n; ++i)
  {
    result.set(int(colors[i].x * range), i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sampleUVTexture(
    const MObject &node,
    const MObject &textureAttribute,
    MFloatArray &uVals,
    MFloatArray &vVals,
    MFloatArray &result)
{
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  int n = uVals.length();

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat,
                                         0, &uVals, &vVals, 0, 0, 0, 0, 0, colors, transparencies);
  if (st.error())
  {
    return MS::kUnknownParameter;
  }

  result.setLength(n);
  for (int i = 0; i < n; ++i)
  {
    result.set(colors[i].x, i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sampleUVTexture(
    const MObject &node,
    const MObject &textureAttribute,
    MFloatArray &uVals,
    MFloatArray &vVals,
    float low,
    float high,
    MFloatArray &result)
{
  float range = high - low;
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;
  MFloatVectorArray colors;

  int n = uVals.length();

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat,
                                         0, &uVals, &vVals, 0, 0, 0, 0, 0, colors, transparencies);
  if (st.error())
  {
    return MS::kUnknownParameter;
  }

  result.setLength(n);
  for (int i = 0; i < n; ++i)
  {
    result.set((colors[i].x * range) + low, i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sampleUVGradient(
    const MObject &node,
    const MObject &textureAttribute,
    float sampleDistance,
    float scale,
    MFloatArray &uVals,
    MFloatArray &vVals,
    MFloatVectorArray &result,
    float boundaryWidth)
{
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);
  if (st.error())
  {
    return MS::kFailure;
  }

  unsigned n = uVals.length();
  result.setLength(n);

  if (sampleDistance < 0.00001)
  {
    sampleDistance = 0.00001;
  }
  scale = scale / sampleDistance;

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;

  MFloatArray uOffsetVals(n);
  MFloatArray vOffsetVals(n);

  // clamp to max boundary

  float maxBoundary = 1.0 - boundaryWidth;
  for (unsigned i = 0; i < n; i++)
  {
    if (uVals[i] > maxBoundary)
    {
      uVals[i] = maxBoundary;
    }
    if (vVals[i] > maxBoundary)
    {
      vVals[i] = maxBoundary;
    }
  }

  for (unsigned i = 0; i < n; i++)
  {
    uOffsetVals.set((uVals[i] + sampleDistance), i);
    vOffsetVals.set((vVals[i] + sampleDistance), i);
  }

  MFloatVectorArray colors;
  MFloatVectorArray uOffsetColors;
  MFloatVectorArray vOffsetColors;

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, 0, &uVals,
                                         &vVals, 0, 0, 0, 0, 0, colors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, 0,
                                         &uOffsetVals, &vVals, 0, 0, 0, 0, 0, uOffsetColors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, 0, &uVals,
                                         &vOffsetVals, 0, 0, 0, 0, 0, vOffsetColors, transparencies);
  msert;

  for (unsigned i = 0; i < n; i++)
  {
    MFloatVector grad(
        (uOffsetColors[i].x - colors[i].x),
        (vOffsetColors[i].x - colors[i].x),
        0.0);

    result.set(grad * scale, i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sample3dGradient(
    const MObject &node,
    const MObject &textureAttribute,
    float sampleDistance,
    float scale,
    MFloatPointArray &points,
    MFloatVectorArray &result)
{
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);

  if (st.error())
  {
    return MS::kFailure;
  }

  unsigned n = points.length();
  result.setLength(n);

  if (sampleDistance < 0.00001)
  {
    sampleDistance = 0.00001;
  }
  scale = scale / sampleDistance;

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;

  MFloatPointArray xOffsetPoints(n);
  MFloatPointArray yOffsetPoints(n);
  MFloatPointArray zOffsetPoints(n);

  // clamp to max boundary

  for (unsigned i = 0; i < n; i++)
  {
    xOffsetPoints.set((points[i] + MFloatVector(sampleDistance, 0.0f, 0.0f)), i);
    yOffsetPoints.set((points[i] + MFloatVector(0.0f, sampleDistance, 0.0f)), i);
    zOffsetPoints.set((points[i] + MFloatVector(0.0f, 0.0f, sampleDistance)), i);
  }

  MFloatVectorArray colors;
  MFloatVectorArray xOffsetColors;
  MFloatVectorArray yOffsetColors;
  MFloatVectorArray zOffsetColors;

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &points, 0, 0, 0, &points, 0, 0, 0, colors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &xOffsetPoints, 0, 0, 0, &xOffsetPoints, 0, 0, 0, xOffsetColors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &yOffsetPoints, 0, 0, 0, &yOffsetPoints, 0, 0, 0, yOffsetColors, transparencies);

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &zOffsetPoints, 0, 0, 0, &zOffsetPoints, 0, 0, 0, zOffsetColors, transparencies);
  msert;

  for (unsigned i = 0; i < n; i++)
  {
    MFloatVector grad(
        (xOffsetColors[i].x - colors[i].x),
        (yOffsetColors[i].x - colors[i].x),
        (zOffsetColors[i].x - colors[i].x));

    result.set(grad * scale, i);
  }
  return MS::kSuccess;
}

MStatus TexUtils::sample3dGradient(
    const MObject &node,
    const MObject &textureAttribute,
    MFloatPointArray &points,
    MFloatArray sampleDistances,
    MFloatVectorArray &result)
{
  MStatus st;
  MString plugName;
  st = getTextureName(node, textureAttribute, plugName);

  if (st.error())
  {
    return MS::kFailure;
  }

  unsigned n = points.length();
  if (n != sampleDistances.length())
  {
    return MS::kFailure;
  }

  MFloatMatrix cameraMat;
  cameraMat.setToIdentity();
  MFloatVectorArray transparencies;

  MFloatPointArray xOffsetPoints(n);
  MFloatPointArray yOffsetPoints(n);
  MFloatPointArray zOffsetPoints(n);
  // JPMDBG;
  for (unsigned i = 0; i < n; i++)
  {
    xOffsetPoints.set((points[i] + MFloatVector(sampleDistances[i], 0.0f, 0.0f)), i);
    yOffsetPoints.set((points[i] + MFloatVector(0.0f, sampleDistances[i], 0.0f)), i);
    zOffsetPoints.set((points[i] + MFloatVector(0.0f, 0.0f, sampleDistances[i])), i);
  }
  // JPMDBG;
  MFloatVectorArray colors;
  MFloatVectorArray xOffsetColors;
  MFloatVectorArray yOffsetColors;
  MFloatVectorArray zOffsetColors;

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &points, 0, 0, 0, &points, 0, 0, 0, colors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &xOffsetPoints, 0, 0, 0, &xOffsetPoints, 0, 0, 0, xOffsetColors, transparencies);
  msert;
  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &yOffsetPoints, 0, 0, 0, &yOffsetPoints, 0, 0, 0, yOffsetColors, transparencies);

  st = MRenderUtil::sampleShadingNetwork(plugName, n, false, false, cameraMat, &zOffsetPoints, 0, 0, 0, &zOffsetPoints, 0, 0, 0, zOffsetColors, transparencies);
  msert;
  // JPMDBG;
  result.setLength(n);
  for (unsigned i = 0; i < n; i++)
  {
    result.set(MFloatVector(
                   (xOffsetColors[i].x - colors[i].x),
                   (yOffsetColors[i].x - colors[i].x),
                   (zOffsetColors[i].x - colors[i].x)),
               i);
  }
  // JPMDBG;
  return MS::kSuccess;
}
