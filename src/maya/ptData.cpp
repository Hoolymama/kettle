/***************************************************************************
ptData.cpp  -  description
-------------------
    begin                : Wed Mar 29 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

***************************************************************************/

#include "ptData.h"

ptData::ptData(
			   const MVector &p,
			   unsigned int id
			   
			   )	
:m_position(p),
m_velocity(MVector::zero),
m_normal(MVector::zero),
m_id(id)
{
}

ptData::ptData(
			   const MVector &p,
			   const MVector &v,
			   unsigned int id
			   
			   )	
:m_position(p),
m_velocity(v),
m_normal(MVector::zero),
m_id(id)
{
}


ptData::ptData(
			   const MVector &p,
			   const MVector &v,
			   const MVector &n,
			   unsigned int id
			   
			   )	
:m_position(p),
m_velocity(v),
m_normal(n),
m_id(id)
{
}


ptData::~ptData() {}



const MVector & ptData::position()  const {return m_position; } 
const MVector & ptData::velocity()  const {return m_velocity; } 
const MVector & ptData::normal()  const {return m_normal; } 
const double ptData::position(axis a) const {return (m_position)[a];}
MVector  ptData::getPosition()  const {return m_position; }
MVector  ptData::getVelocity()  const {return m_velocity; }
//void ptData::setDist(double d) { m_dist = d; }
//const double & ptData::dist() const {return m_dist; } 
//double ptData::getDist() const{return m_dist; } 
void ptData::setVelocity(MVector &v) {m_velocity = v;}
const unsigned int & ptData::id() const {return m_id;}

MVector ptData::forceAt(const MVector & searchPoint, 
						  const MVector & searchVelocity,
						  const double & radius,
						  const lookup * radialFactorD,
						  const lookup * normalFactorD,
						  const lookup * gyrateFactorD,
						  const lookup * inheritFactorD,
						  int & count
						  ) const {
	
	MVector force(0.0,0.0,0.0);
	
	MVector diff = (searchPoint - m_position  );


	double dist =  diff.length(); // normalize dist
	if (dist < 0.001) return(force); // hack to avoid self test

	MVector diffN = diff.normal();

	//cerr << "pos and search "<< m_position << " " << searchPoint << endl;

	if (dist > radius ) return(force);

	double atten = dist / radius;
	count ++;
	MVector componentForce; 
	if (normalFactorD) {
		componentForce = (m_normal * normalFactorD->evaluate(atten));
		//cerr << "nForce" << componentForce << endl;
		force += componentForce;
	} 

	// velocity is relative
	if (inheritFactorD) {
		componentForce = ((m_velocity - searchVelocity) * inheritFactorD->evaluate(atten));
		//cerr << "inherForce" << componentForce << endl;
		force += componentForce;
	}
	

	if (radialFactorD) {
		// cerr << "diffN=" << diffN << endl;
		// cerr << "dist=" << dist << endl;
		double val = radialFactorD->evaluate(atten);
		// cerr << "val=" << val << endl;
		componentForce = diffN * val ;

		// cerr << "radForce" << componentForce << endl;
		force += componentForce;
		
	}
	
	if (gyrateFactorD) {
		if (!m_normal.isParallel(diff)) {
			// we can't calculate the gyrate component if the point is on the normal vector

			MVector cross = (m_normal^diff).normal();
			componentForce =(cross  *  gyrateFactorD->evaluate(atten) );
			//cerr << "gyForce" << componentForce << endl;
			force += componentForce;
		}
	}

	
	//force *= m_length;
	// cerr << "force " << force << endl;
	// cerr << "--------------------" << endl;

	return(force);
 }


