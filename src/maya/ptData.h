
#ifndef _ptData
#define _ptData


#include <maya/MDoubleArray.h>
#include <maya/MVectorArray.h>
#include "lookup.h"
#include "mayaMath.h"

typedef mayaMath::axis axis;

class ptData {
public:
	
	/// 
	ptData(
		   const MVector &p,
		   unsigned int id
		   );
	
	ptData(
		   const MVector &p,
		   const MVector &v,
		   unsigned int id
		   );
	
	ptData(
		   const MVector &p,
		   const MVector &v,
		   const MVector &n,
		   unsigned int id
		   );
	
	~ptData();
	
	const MVector & position()  const  ; 
	const MVector & velocity()  const ; 
	const MVector & normal()  const ; 
	const double position(axis a) const; 
	MVector  getPosition()  const ;
	MVector  getVelocity()  const ;
	void setVelocity(MVector &v);
	const unsigned int & id() const;
	
	//bool operator< (const ptData &other) const ; 
	
	MVector forceAt(const MVector & searchPoint, 
							const MVector & searchVelocity,
							const double & radius,
							const lookup * radialFactorD,							
							const lookup * normalFactorD,
							const lookup * gyrateFactorD,
							const lookup * inheritFactorD,
							int & count
							) const;
	
	
	
private:
	MVector m_position;	// where the point is
	MVector m_velocity; // an associated direction / velocity
	MVector m_normal; // an associated normal - useful if pnts are on a surface
	unsigned int  m_id;	// an id, just in case
};
#endif

