 #ifndef _pdcFile
#define _pdcFile

 
#include <maya/MIOStream.h>
#include <string.h> 
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <map>

//#include <netinet/in.h>
#include <maya/MStatus.h>
#include <maya/MVector.h>
#include <maya/MString.h>
#include <maya/MIntArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MVectorArray.h>
#include <maya/MStringArray.h>
#include <maya/MBoundingBox.h>
#include "JBoundingBox.h"
#include "byteSwap.h"
#include "mayaIoUtils.h"

#include "pdcAttr.h"


struct MStrCmp
{
	bool operator()(const MString & m1, const MString & m2) const
	{
		return strcmp(m1.asChar(), m2.asChar()) < 0;
	}
};


typedef std::map<MString, pdcAttr, MStrCmp>  pdcAttrMap;


class pdcFile {
public:

	// open the pdc file 
	pdcFile ( const MString& fn, MStatus &st );
	
	// close the file and clean up
	~pdcFile() ;
	
	// attribute names
	MStringArray getNames() const ;

	MStringArray getSortedNames() const ;
	
	bool attributeExists(const MString& name) const ;

	MStringArray getTypes() const;
	
	pdcAttr::PdcAttrType typeFromName (const MString& name) const;
	
	MString typeStringFromName (const MString& name) const ;

	int fileNumber(MStatus *st=NULL) const;
	// number of particles
	int	particleCount() const ;
	
	// number of artributes
	int	attributeCount() const ;

	// get data by attribute name
	MStatus	getData(int& data, const MString& name)  const;
	MStatus	getData(MIntArray& data, const MString& name) const;
	MStatus	getData(double& data, const MString& name)  const;
	MStatus	getData(MDoubleArray& data, const MString& name) const;
	MStatus	getData(MVector& data, const MString& name)  const;
	MStatus	getData(MVectorArray& data, const MString& name)  const;

	MStatus boundingBox(JBoundingBox &bb) ;
	MStatus boundingBox(MBoundingBox &bb) ;


	void getHeaderInfo(int& version, int& endian, int& bitinfo1, int& bitinfo2) const;


	friend ostream& operator<<(ostream &os, const pdcFile &file){


		os << "** " << file.m_filename << " **" << endl;
		os << "version " << file.m_version << endl;
		os << "endian " << file.m_endian << endl;
		os << "bitInfo1 " << file.m_bitInfo1 << endl;
		os << "bitInfo2 " << file.m_bitInfo2 << endl;
		os << "number of Particles " <<file.m_numParticles  << endl;
		os << "number of Attributes " << file.attributeCount() << endl;
		os << "-----------------------------------"<<endl;

		pdcAttrMap::const_iterator iter = file.m_attrMap.begin();
		while (iter !=file.m_attrMap.end()) {
			os <<  iter->second << endl;
			iter++;
		}
		return os ;
	}


private:

//	int					indexFromName(const MString& name) const; 
	//int					swapDouble(double* n);
	//int					swapInt(int* n);
	
	
	
	/*
	 MStatus readValue(MIntArray & val, std::istream &is);
	 MStatus readValue(MDoubleArray & val, std::istream &is) ;
	 MStatus readValue(MVectorArray & val, std::istream &is) ;
	 MStatus readValue(int & val, std::istream &is) ;
	 MStatus readValue(double & val, std::istream &is) ;
	 MStatus readValue(MVector & val, std::istream &is) ;
	 MStatus readValue(MString & val, std::istream &is);
	*/
	//  MString				generatePDCFileName(const MString & prefix, const MTime & cT);

	int					m_version;
	int					m_endian;
	int					m_bitInfo1;
	int					m_bitInfo2;
	int					m_numParticles;
	int					m_numAttr;
	

	pdcAttrMap				m_attrMap;

	// pdcAttrArray				m_attrData;
	
	/*
	MIntArray				m_attrTypes;
	MStringArray				m_attrNames;
	MIntArray				m_dataPos;
	*/
	
	MString				m_filename; 
	// std::ifstream		m_is;

};

#endif

/*
inline MStatus pdcFile::readValue(MIntArray & val, std::istream &is) {	
	unsigned len = val.length();
	for (unsigned i= 0;i<len;i++){
		int & f = val[i];
		is.read((char*)&f , sizeof(int) );
		swapInt(&f );
		if (is.fail())  return MS::kFailure; 
	}
	return MS::kSuccess;
}

inline MStatus pdcFile::readValue(MDoubleArray & val, std::istream &is) {	
	unsigned len = val.length();
	for (unsigned i= 0;i<len;i++){
		double & f = val[i];
		is.read((char*)&f , sizeof(double) );
		swapDouble(&f );
		if (is.fail())  return MS::kFailure; 
	}
	return MS::kSuccess;
}

inline MStatus pdcFile::readValue(MVectorArray & val, std::istream &is) {	
	unsigned len = val.length();
	for (unsigned i= 0;i<len;i++){
		MVector & v = val[i];
		for (unsigned j = 0;j<3;j++){
			double &f =v[j]; 
			is.read((char*)&f , sizeof(double) );
			swapDouble(&f );

			if (is.fail())  return MS::kFailure; 
		}
	}
	return MS::kSuccess;
}

inline MStatus pdcFile::readValue(int & val, std::istream &is) {
	is.read((char*)&val , sizeof(int) );
	swapInt(&val );
	if (is.fail()) return MS::kFailure; 
	return MS::kSuccess;
}

inline MStatus pdcFile::readValue(double & val, std::istream &is) {
	is.read((char*)&val , sizeof(double) );
	swapDouble(&val );
	if (is.fail()) return MS::kFailure; 
	return MS::kSuccess;
}

inline MStatus pdcFile::readValue(MVector & val, std::istream &is) {
	for (unsigned i = 0;i<3;i++){
		double & f = val[i];
		is.read((char*)&f , sizeof(double) );
		swapDouble(&f );
		if (is.fail()) return MS::kFailure; 
	}
	return MS::kSuccess;
}



inline MStatus pdcFile::readValue(MString & val, std::istream &is) {
	MStatus st = MS::kFailure; 
	unsigned len = val.length();
	char * buffer = new char[len];
	is.read(buffer,len);
	if (!(is.fail())) {
		val = MString(buffer,len) ;
		st = MS::kSuccess;
	} 
	delete[] buffer;
	return st; 
}
*/


