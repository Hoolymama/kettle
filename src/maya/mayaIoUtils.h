/*
 *  mayaIoUtils.h
 *  feather
 *
 *  Created by Julian Mann on 04/10/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#ifndef mayaioutils_h
#define mayaioutils_h


#include <fstream>


#include <maya/MFloatMatrix.h>
#include <maya/MFloatVector.h>
#include <maya/MDoubleArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVectorArray.h>
 
#include <maya/MIntArray.h>
#include <maya/MTime.h>
#include <maya/MString.h>
#include "byteSwap.h"

///////////////////////////////////// [ W R I T E ] ////////////////////////////////

namespace mayaIoUtil {

	inline MStatus writeValue(const MIntArray & val, std::ostream &os) {
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			int n = val[i];
			swapInt(&n );
			os.write( (char*)&n  , sizeof(int) );
			if (os.fail())  return MS::kFailure; 
		}
		return MS::kSuccess;
	}

	inline MStatus writeValue(const MDoubleArray & val, std::ostream &os) {
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			double d = val[i];
			swapDouble(&d );
			os.write( (char*)&d  , sizeof(double) );
			if (os.fail())  return MS::kFailure; 
		}
		return MS::kSuccess;
	}

	inline MStatus writeValue(const MVectorArray & val, std::ostream &os) {
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			const MVector & v = val[i];
			for (unsigned j = 0;j<3;j++){
				double d = v[j];
				swapDouble(&d);
				os.write( (char*)&d  , sizeof(double) );
				if (os.fail())  return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}


	inline MStatus writeValue(const MFloatMatrix & val, std::ostream &os) {
		for (unsigned j= 0;j<4;j++){
			for (unsigned i = 0;i<4;i++){
				float n = (val(i,j))  ;
				swapFloat(&n );
				os.write( (char*)&n  , sizeof(float) );
				if (os.fail())  return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}


	inline MStatus writeValue(const MFloatVector & val, std::ostream &os) {
		for (unsigned i = 0;i<3;i++){
			float n = val[i];
			swapFloat(&n );
			os.write((char*)&n , sizeof(float) );
			if (os.fail()) return MS::kFailure; 
		}
		return MS::kSuccess;
	}



	inline MStatus writeValue(const MVector & val, std::ostream &os) {
		for (unsigned i = 0;i<3;i++){
			double n = val[i];
			swapDouble(&n );
			os.write((char*)&n , sizeof(double) );
			if (os.fail()) { 
				//cerr << "problem " << endl;
				return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}


	inline MStatus writeValue(const float & val, std::ostream &os) {
		float n = val;
		swapFloat(&n );
		os.write((char*)&n , sizeof(float) );
		if (os.fail())  return MS::kFailure; 
		return MS::kSuccess;
	}


	inline MStatus writeValue(const double & val, std::ostream &os) {
		double n = val;
		swapDouble(&n );
		os.write((char*)&n , sizeof(double) );
		if (os.fail())  return MS::kFailure; 
		return MS::kSuccess;
	}

	inline MStatus writeValue(const int & val, std::ostream &os) {
		int n = val;
		swapInt(&n);
		os.write((char*)&n , sizeof(int) );
		if (os.fail())  return MS::kFailure; 
		return MS::kSuccess;
	}


	inline MStatus writeValue(const unsigned & val, std::ostream &os) {
		unsigned n = val;
		swapInt( (int * )(&n));
		os.write((char*)&n , sizeof(unsigned) );
		if (os.fail())  return MS::kFailure; 
		return MS::kSuccess;
	}


	inline MStatus writeValue(const MString & val, std::ostream &os) {
		unsigned len = val.length();
		const char * c = val.asChar();
		os.write(c, (len * sizeof(char)));
		if (os.fail())  return MS::kFailure; 
		return MS::kSuccess;
	}

	///////////////////////////////////// [ R E A D ] /////////////////////////////////

	inline MStatus readValue(MIntArray & val, std::istream &is) {	
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			int & n = val[i];
			is.read((char*)&n , sizeof(int) );
			swapInt(&n);
			if (is.fail())  return MS::kFailure; 
		}
		return MS::kSuccess;
	}

	inline MStatus readValue(MDoubleArray & val, std::istream &is) {	
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			double & n = val[i];
			is.read((char*)&n , sizeof(double) );
			swapDouble(&n);
			if (is.fail())  return MS::kFailure; 
		}
		return MS::kSuccess;
	}

	inline MStatus readValue(MVectorArray & val, std::istream &is) {	
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			MVector & v = val[i];
			for (unsigned j = 0;j<3;j++){
				double &n =v[j]; 
				is.read((char*)&n , sizeof(double) );
				swapDouble(&n);
				if (is.fail())  return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}

	inline MStatus readValue(MFloatVectorArray & val, std::istream &is) {	
		unsigned len = val.length();
		for (unsigned i= 0;i<len;i++){
			MFloatVector & v = val[i];
			for (unsigned j = 0;j<3;j++){
				float &n =v[j]; 
				is.read((char*)&n , sizeof(float) );
				swapFloat(&n);
				if (is.fail())  return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}


	inline MStatus readValue(MFloatMatrix & val, std::istream &is) {
		for (unsigned j= 0;j<4;j++){
			for (unsigned i = 0;i<4;i++){
				float & n = val(i,j)   ;
				is.read( (char*)&n , sizeof(float) );
				swapFloat(&n);

				if (is.fail()) return MS::kFailure; 
			}
		}
		return MS::kSuccess;
	}

	inline MStatus readValue(MVector & val, std::istream &is) {
		for (unsigned i = 0;i<3;i++){
			double & n = val[i];
			is.read((char*)&n , sizeof(double) );
			swapDouble(&n);
			if (is.fail()) return MS::kFailure; 
		}
		return MS::kSuccess;
	}

	inline MStatus readValue(MFloatVector & val, std::istream &is) {
		for (unsigned i = 0;i<3;i++){
			 float & n = val[i];
			is.read((char*)&n , sizeof(float) );
			swapFloat(&n);
			if (is.fail()) return MS::kFailure; 
		}
		return MS::kSuccess;
	}


	inline MStatus readValue(float & val, std::istream &is) {
		is.read((char*)&val , sizeof(float) );
		swapFloat(&val);
		if (is.fail()) return MS::kFailure; 
		return MS::kSuccess;
	}

	inline MStatus readValue(double & val, std::istream &is) {
		is.read((char*)&val , sizeof(double) );
		swapDouble(&val);
		if (is.fail()) return MS::kFailure; 
		return MS::kSuccess;
	}

	inline MStatus readValue(int & val, std::istream &is) {
		is.read((char*)&val , sizeof(int) );
		swapInt(&val);
		if (is.fail()) return MS::kFailure; 
		return MS::kSuccess;
	}

	inline MStatus readValue(unsigned & val, std::istream &is) {
		is.read((char*)&val , sizeof(unsigned) );
		swapInt( (int*)&val);
		if (is.fail()) return MS::kFailure; 
		return MS::kSuccess;
	}

	inline MStatus readValue(MString & val, std::istream &is) {
		MStatus st = MS::kFailure; 
		unsigned len = val.length();
		char * buffer = new char[len];
		is.read(buffer,len);
		if (!(is.fail())) {
			val = MString(buffer,len) ;
			st = MS::kSuccess;
		} 
		delete[] buffer;
		return st; 
	}

	inline MString generateFileName(const MString & dir, const MString & nodeName, const MString & ext)  {
		
		MString fn = dir;
		int last = (fn.length() - 1);
		if (  fn.substring(last,last)  != "/")  fn += "/";
		fn += nodeName;
		fn +=".";
		fn +=ext;
		return fn;
	}

	inline MString generateFileName(const MString & dir, const MString & nodeName, const MString & ext, const MTime & t)  {
		
		MString fn = dir;
		int last = (fn.length() - 1);
		if (  fn.substring(last,last)  != "/")  fn += "/";
		int num = int(t.as(MTime::uiUnit()));
		num += 10000;
		MString numStr;
		numStr += num;
		fn += nodeName;
		fn +=".";
		fn += numStr.substring(1,4);
		fn +=".";
		fn +=ext;
		return fn;
	}

	inline MString generateFileName(const MString & prefix, const MString & ext, const MTime & t)  {
		
		MString fn = prefix;
		int num = int(t.as(MTime::uiUnit()));
		num += 10000;
		MString numStr;
		numStr += num;
		fn +=".";
		fn += numStr.substring(1,4);
		fn +=".";
		fn +=ext;
		return fn;
	}

	
	
  // 
  // inline MStatus directoryEntries(const MString & path,  MStringArray & result)  {
  // 	
  // 	const char * c_path = path.asChar();
  // 	DIR * dirp = opendir(c_path);
  // 	struct dirent *dp;
  // 	
  // 	if (dirp) {	
  // 		// return all files in the directory
  // 		while ((dp = readdir(dirp)) != NULL) {
  // 			const std::string s = dp->d_name; 
  // 			result.append(MString(s.c_str()));
  // 		}
  // 		closedir(dirp);	
  // 		return MS::kSuccess;
  // 	} 
  //
  //
  // 	int lastSlash = path.rindexW('/');
  // 	int pathLen = path.length();
  // 	// if the slash is embedded then the last bit might be a filePrefix
  // 	if (( lastSlash < (pathLen-1)  ) && (lastSlash > -1)) {
  // 		MString filePrefix = path.substringW((lastSlash+1), (pathLen-1));
  // 		MString pathRoot = path.substringW(0, lastSlash);
  // 		
  // 		cerr << "pathRoot " << pathRoot << endl;
  // 		cerr << "filePrefix " << filePrefix <<  endl;
  // 		
  // 		const char * dirStr = pathRoot.asChar();
  // 		dirp = opendir(dirStr);
  // 		if (dirp) {	
  // 			// return all files in the directory matching filePrefix
  // 			while ((dp = readdir(dirp)) != NULL) {
  // 				const std::string s = dp->d_name; 
  // 				MString fname(s.c_str());
  // 				MStringArray
  // 				
  // 				
  // 				result.append(MString(s.c_str()));
  // 			}
  // 			closedir(dirp);	
  // 			return MS::kSuccess;
  // 		} else {
  // 			cerr << "path" << "path" << endl;
  // 			return MS::kFailure;
  // 		}
  // 	} else {
  // 		cerr << "path" << "path" << endl;
  // 		return MS::kFailure;
  // 	}
  //
  //
  //
  //
  // }
  // 
	
	
	
	
	
	
}


#endif

