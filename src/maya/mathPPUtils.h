#include <iostream>

#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MPlug.h>
#include "errorMacros.h"



inline MStatus cleanUp(MDataBlock &d, const  MPlug &p) {
	MStatus st;
	MDataHandle h = d.outputValue(att, &st); msert;
	h.set(out);
	d.setClean(p);
	return MS::kSuccess;
}
