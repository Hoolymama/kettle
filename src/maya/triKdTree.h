#ifndef _triKdTree
#define _triKdTree

#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MTime.h>
#include <maya/MString.h>
#include <maya/MVector.h>
#include <map>
#include <maya/MIntArray.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MItMeshPolygon.h>

#include "triData.h"
#include "pointOnTriangleInfo.h"

#include "errorMacros.h"


class     triData ;

typedef vector<triData*> TRI_LIST;


struct triDataKdNode  {
	bool bucket;							/// is this node a bucket
	axis cutAxis;							/// if not, this axis will divide it 
	double cutVal;							/// at this value
	triDataKdNode *loChild, *hiChild;		/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	TRI_LIST * overlapList;					/// list of triDatas whose boumding boxes intersect this bucket

	// if we get anymore crashes we should try to make the overlap list a pointer
};


class triKdTree 
{
public:

	triKdTree();			

	~triKdTree();  
	const triDataKdNode * root();
	triKdTree& operator=(const triKdTree & otherData );

	void 		build(); 							/// build root from m_perm

	triDataKdNode * 	build( int low,  int high ); 	/// build children recursively
	int size() ;
		//MStatus 	getSize(int &siz);  			/// size of m_perm and _prismVector - returns failure if they are different

	void  	setMaxPointsPerBucket( int b) {if (b < 1) {m_maxPointsPerBucket = 1;} else {m_maxPointsPerBucket = b;}}

	MStatus 	init();

	//	MStatus 	populate(const MFnDynSweptGeometryData &g) ;

	MStatus		populate(const MFnDynSweptGeometryData  &g, unsigned id);
	
	MStatus		populate(const MFnDynSweptGeometryData  &g, unsigned id, const MBoundingBox &box);

	MStatus 	populate(const MDagPath  &meshPath) ;

	MStatus populate(	
		const MPointArray & lastPoints,
		const MPointArray & points,
		const MIntArray & triangleVertices
		) ;

	void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;

	void makeEmpty() ; 

	void makeEmpty(triDataKdNode * p) ; 

	void  setOverlapList(triDataKdNode * p,  triData * tb   );

	TRI_LIST & triangleList();
	
	const TRI_LIST & orderedTriangleList() const ;

	void  boxSearch(const triDataKdNode * p, const MBoundingBox& searchBox, TRI_LIST * resultBoxes, int ignoreMin=-1,int ignoreMax=-1) const;

	void closestTri(const triDataKdNode * p,const MPoint &searchPoint,  double & radius,  triData &  result , int ignoreMin=-1,int ignoreMax=-1)  const     ;

	// fill data in given pointOnTriangleInfo
	void closestTri(
		const triDataKdNode * p,
		const MPoint &searchPoint,  
		double & radius,  
		pointOnTriangleInfo &  result , 
		int ignoreMin=-1,int ignoreMax=-1)  const;

private:	
	void  makeResultBoxes(const TRI_LIST * overlapList, const MBoundingBox& searchBox , TRI_LIST *resultBoxes, int ignoreMin=-1,int ignoreMax=-1) const  ;

	axis findMaxAxis(const  int low,const  int  high) const;
	
	void  searchList(const TRI_LIST * overlapList, const MPoint &searchPoint, double & radius, triData & result, int ignoreMin=-1,int ignoreMax=-1) const   ;

void  searchList( const TRI_LIST * overlapList, const MPoint &searchPoint, double & radius, pointOnTriangleInfo & pInfo,int ignoreMin,int ignoreMax) const;

	TRI_LIST m_ordered_triangles;
	
	TRI_LIST * m_perm;						// pointer to (permutations) - a list of pointers to elements of _triDataVector
	
	triDataKdNode * m_pRoot;				// pointer to root of tree
	
	int m_maxPointsPerBucket;				// at build time keep recursing until no bucket holds more than this

} ;

typedef std::map< MTime, triKdTree * > triKdTreeCacheMap ;	

#endif
