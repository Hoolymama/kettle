/***************************************************************************
lineData.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

	a class to store lines in a bounding box for use in a lineKdTree.
	***************************************************************************/

#include "lineData.h"
#include "errorMacros.h"

const double TIMESTEP = 1.0f / 24.0f;
const double EPSILON = 0.0000001;
//const double NEG_EPSILON = -0.0001;

lineData::lineData(){}

lineData::lineData( MDynSweptLine &l,double uStart, double uEnd, int id, double dt):
m_p(2),
m_v(2),
m_uCoords(2),
m_cachedParam(0.0),
m_cachedPt(MPoint::origin),
m_id(id)
{	
	
	m_p.set(l.vertex(0),0);
	m_p.set(l.vertex(1),1);
	
	if (fabs(dt) > EPSILON) {
		double dt_recip = 1.0 / dt;
		m_v.set( (l.vertex(0,1) - l.vertex(0,0)) * dt_recip ,0);
		m_v.set( (l.vertex(1,1) - l.vertex(1,0)) * dt_recip ,1);
	}
	
	m_uCoords.set(uStart,0);
	m_uCoords.set(uEnd,1);

	computeBoundingBox();
	
	m_length = l.length();
	//cerr << "l.length() " << l.length() << endl;
	m_tangent = l.tangent();
	//cerr << "l.tangent() " << l.tangent() << endl;
}

lineData::~lineData(){}

lineData& lineData::operator=(const lineData& other) {
	if (this != &other) {
		m_p = other.m_p;
		m_v = other.m_v;
		m_box = other.m_box;
		m_uCoords = other.m_uCoords;
		m_cachedParam = other.m_cachedParam;
		m_cachedDist = other.m_cachedDist;
		m_cachedPt = other.m_cachedPt;
		m_id = other.m_id;
		m_length = other.m_length;
		m_tangent = other.m_tangent; 
	}
	return *this;
}

void lineData::computeBoundingBox(){
	m_box = MBoundingBox(m_p[0],m_p[1]);
	m_center = m_box.center();
	if (
		(m_box.width() < EPSILON) ||
		(m_box.height() < EPSILON) ||
		(m_box.depth() < EPSILON) 
		){
		m_box.expand(m_box.max() + MVector(0.0001,0.0001,0.0001));
		m_box.expand(m_box.min() - MVector(0.0001,0.0001,0.0001));
	}	
}

const MVector & lineData::center() const {return m_center;}

double  lineData::center(axis a) const {return  m_center[a];}

double  lineData::min(axis a) const {return m_box.min()[a];}

double  lineData::max(axis a) const {return m_box.max()[a];}

int lineData::id() const { return m_id;}

const MVector & lineData::vertex(int i) const {return m_p[i];}

const MVector & lineData::velocity(int i) const {return m_v[i];}

const MBoundingBox & lineData::box() const {return m_box;}

const MDoubleArray & lineData::uCoords() const {return m_uCoords;}

// determine if the given spere intersects this bounding box
bool lineData::sphereIntersectsBB(const MVector &c, double r) const  {
	// params are center and radius
	return mayaMath::sphereBoxIntersection( m_box, c, r);
}

// determine if the given spere intersects the line.
// If so, store the point and bary coords of the closest point to the sphere center
// and send back the distance in dist
bool lineData::sphereIntersectsLine(const MVector &c, double r, double &dist) {
	double param;
	MVector pt  = mayaMath::closestPointOnLine(c, m_p[0], m_p[1], param ) ;
	  
	MVector v = pt - c;
	double sqDist = (v*v);
	if ((sqDist) <= (r*r)) {  // bingo
		dist = sqrt(sqDist) ;

		//cerr << " in lineData - found a point and setting cached Dist to  " << dist << endl;
    	m_cachedParam = param;
    	m_cachedDist = dist;
		//cerr << "  m_cachedDist " << m_cachedDist << endl;
		m_cachedPt = pt;
		return true;
	}
	return false;
}

const MVector & lineData::cachedPoint() const  {
	return m_cachedPt;
}
const MVector & lineData::tangent() const{
	return m_tangent;
}
const double & lineData::length() const{
	return m_length;
}
const double & lineData::cachedParam() const  {
	return m_cachedParam;
}
const double & lineData::cachedDist() const  {
	return m_cachedDist;
}
double lineData::calcU() const {
	return (m_uCoords[0] + (m_cachedParam * (m_uCoords[1] - m_uCoords[0])));
}
MVector lineData::calcVelocity() const {
	return (m_v[0] + (m_cachedParam * (m_v[1] - m_v[0])));
}

