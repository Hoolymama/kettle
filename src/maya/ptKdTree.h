
#ifndef _ptKdTree
#define _ptKdTree

//
// Copyright (C) 2000 hoolyMama 
// 
// File: ptKdTree.h
//
// Author: Julian Mann

                                                                                                                                                                                                            
#include <maya/MVectorArray.h>
#include <maya/MIntArray.h>

#include <vector>
#include <queue>

#include "ptData.h"





struct ptDataKdNode  {
	bool bucket;					// is this node a bucket
	axis cutAxis;					// if not, this axis will divide it 
	float cutVal;					// at this value
	ptDataKdNode *loChild, *hiChild;		// and these pointers point to the children
	unsigned int loPoint, hiPoint;	// Indices into the permutations array
};


class ptData;
struct knnSearchData;
struct knnSearchPdData;

typedef std::vector<ptData*> PT_LIST;

typedef std::priority_queue<knnSearchData> KNN_QUEUE;
typedef std::priority_queue<knnSearchPdData> KNN_PD_QUEUE;

typedef std::vector<PT_LIST *> PD_CLUSTERS;

// this struct is used as an element in a priority queue
struct knnSearchData {
     double dist;
     MVector point;
     MVector velocity;
     bool operator< (const knnSearchData &other) const {return dist < other.dist;}
}  ;


struct knnSearchPdData {
	double dist;
	ptData * pd;
	bool operator< (const knnSearchPdData &other) const {return dist < other.dist;}
}  ;




class ptKdTree
{

public:

	//  constructor 
	ptKdTree();	

	// destructor
	~ptKdTree();  
	
	// return the size
	const int size() const;
	
	// set bucket capacity


	void  setMaxPointsPerBucket( int b);


	// add points to the permutations array through a mask   
	void populate(const MVectorArray &p, unsigned id);
	
	void populate(const MVectorArray &p);
	void populate(const MVectorArray &p, const MVectorArray &v);
	void populate(const MVectorArray &p, const MVectorArray &v, const MVectorArray &n);

	// when all poins are added use this method to build the tree 
	MStatus build( int * pForceCutAxis=0);

	// return a pointer to the root
	const ptDataKdNode * root() const;

	// return a pointer to the perm
	//const PT_LIST * perm() const;

	
	// This was const PT_LIST * perm() const;
	// I changed it because there is a need to average the velocities 
	// based on the velocities of nearby neighbors
	
	PT_LIST * perm();
	
	// do search and repulsion calcs on the fly
	/*
	void calculateOffset(

		const ptDataKdNode * p,
		const MVector & searchPoint,
		const double & radius,	 
		MVector & result
	) const ;
*/
	// do search only
	void  fixedRadiusSearch(
		const ptDataKdNode * p,
		const MVector & searchPoint,
		const double & radius, 
		PT_LIST & result
	)  const ;
void  fixedRadiusSearch(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	const double & radius, 
	MIntArray & result
)  const ;

  void closestDist(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	double & radius
)  const  ;

void  closest2Dists(          // find the two nearest neighbors
	const ptDataKdNode * p,
	const MVector & searchPoint,
	MDoubleArray &radii
)  const ;

void  closest2Pts(
							 const ptDataKdNode * p,
							 const MVector & searchPoint,
							 MDoubleArray &dists,
							 MVectorArray &pts
							 )  const;

void  closestNPts(
  		 const ptDataKdNode * p,
  		 const MVector & searchPoint,
       KNN_QUEUE &q
)  const ;


void  closestNPts(
				  const ptDataKdNode * p,
				  const MVector & searchPoint,
				  KNN_PD_QUEUE *q
				  )  const;


void  closestNPtsWithinNormalThresh(
	  const ptDataKdNode * p,
	  const MVector & searchPoint,
	  KNN_PD_QUEUE *q,
	  const MVector & normal,
	  const double &thresh
)  const  ;

double calculateOffsetAndReturnRadius(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	int neighbors,
	MVector & result
) const;

double calculateOffsetAndReturnRadius(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	const MVector & normal,
	double thresh,
	int neighbors,											
	MVector & result
) const ;
/*
double calculateOffsetAndReturnRadiusEdgeAtten
(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	int neighbors,
	MVector & result
) const ;
*/
void  gatherForces(
							 const ptDataKdNode * p, 
							 const MVector & searchPoint, 
							 const MVector & searchVelocity, 
							 const double &  radius,
							 const lookup * rfl,
							 const lookup * nfl,
							 const lookup * gfl,
							 const lookup * ifl,
							 MVector & result ,
							 int & count
							 )  const ;


private:

	void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;
	const axis findMaxAxis(int low, int high) const;
	void makeEmpty(ptDataKdNode * p) ; 

	bool pointInRadius	(  int i , const MVector & searchPoint ,const double & radius) const ;
	
	MVector calcRepulsion(  int i ,const MVector & searchPoint ,const double & radius) const ;


	ptDataKdNode * build( int low, int high , int * pForceCutAxis=0);

	PT_LIST * m_pPerm;						// all points
	ptDataKdNode * m_pRoot;					// pointer to root of tree
	unsigned int m_maxPointsPerBucket;	// at build time keep recursing until no bucket holds more than this
	// double m_dRadius;
	//lookup * diLookup;
	//lookup * normalLookup;
	//lookup * normalLookup;
	//lookup * normalLookup;
	
	
	
	
};

#endif

