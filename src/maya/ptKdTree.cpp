 /***************************************************************************
                          ptKdTree.cpp  -  description
                             -------------------
    begin                : Wed Mar 29 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

  this kdTree stores ptData objects. It's meant to be pretty fast.



  I'm a little concerned about the constructor / destructor / makeEmpty methods
  If scenes start to crash in very odd ways - then its probably to do with stray
  or null pointers that are not correctly handled here

***************************************************************************/

#define P_TOLERANCE 0.00000001

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MIntArray.h>
#include "lookup.h"
#include "ptKdTree.h"

const double bigNum = 99999999999.0  ;

ptKdTree::ptKdTree() // default constructor
	:m_pPerm(0),
	m_pRoot(0),
m_maxPointsPerBucket(4)
{

	m_pPerm = new PT_LIST;
	m_pRoot = 0;
};


ptKdTree::~ptKdTree()
{
	// delete the elements in the permutations array
	if (m_pPerm) {
		PT_LIST::iterator p = m_pPerm->begin();
		while (p != m_pPerm->end()) {
			delete *p;
			*p =0;
			p++;
		}
		// then delete the array itself
		delete m_pPerm;
		m_pPerm = 0;
	}
	
	makeEmpty(m_pRoot);  // recursively delete tree 	
};

void ptKdTree::makeEmpty(ptDataKdNode * p) {
	// post order traversal to delete nodes
	if (p) { // if the pointer is not NULL
		if (!(p->bucket)) { // if its not a bucket node
			makeEmpty(p->loChild);	// recurse through child nodes
			makeEmpty(p->hiChild);
		}
		delete p;            // then delete this node
		p = 0;
	}
}


void ptKdTree::populate(const MVectorArray & p, unsigned id)
{
	unsigned int pl = p.length();
	for (unsigned int i = 0; i < pl; i++) {
		ptData * pp = new ptData( p[i], id);
		m_pPerm->push_back(pp);
	} 
}

void ptKdTree::populate(const MVectorArray & p)
{
	unsigned int pl = p.length();
	for (unsigned int i = 0; i < pl; i++) {
		ptData * pp = new ptData( p[i], i);
		m_pPerm->push_back(pp);
	} 
}

void ptKdTree::populate(const MVectorArray & p,const MVectorArray & v )
{
	unsigned int pl = p.length();
	unsigned int vl = v.length();
	if (vl == pl) {
		for (unsigned int i = 0; i < pl; i++) {
			ptData * pp = new ptData( p[i], v[i], i);
			m_pPerm->push_back(pp);
		} 
	}
}
void ptKdTree::populate(const MVectorArray & p,const MVectorArray & v ,const MVectorArray & n )
{
	unsigned int pl = p.length();
	unsigned int vl = v.length();
	unsigned int nl = n.length();
	if ((vl == pl) && (nl == pl)) {
		for (unsigned int i = 0; i < pl; i++) {
			ptData * pp = new ptData( p[i], v[i], n[i], i);
			m_pPerm->push_back(pp);
		} 
	}
}


const int  ptKdTree::size() const {
	return int(m_pPerm->size());
}



MStatus ptKdTree::build(int * pForceCutAxis){
	m_pRoot = build( 0, int(m_pPerm->size() -1) , pForceCutAxis );
	if (!m_pRoot) {
		return MS::kUnknownParameter;
	}
	return MS::kSuccess;
}


ptDataKdNode *  ptKdTree::build( int low,  int high	, int * pForceCutAxis){
	
	// recursively make a new ptDataKdNode and 

	// reArrange the entries in perm respectively
	
	ptDataKdNode * p = new ptDataKdNode;

 	if ( unsigned((high - low) + 1) <= m_maxPointsPerBucket) {   // stop recursing
		
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
		
	} else {   // split the space and recursively build
		p->bucket = false;
		
		// two possible methods to find the cut axis:
		// 1. test a subset of the points to find the longest dimension (really expensive).
		// 2. simply rotate through the axes each time we cut (really cheap). 
		if (!pForceCutAxis){ // if null
			p->cutAxis = findMaxAxis(low, high); 
		} else {
			p->cutAxis = axis(*pForceCutAxis); 
			*pForceCutAxis = (*pForceCutAxis+1)%3;
		}
		int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		p->cutVal = float(((*m_pPerm)[mid])->position(p->cutAxis));
		p->loChild = build(low, mid,pForceCutAxis);
		p->hiChild = build(mid+1, high,pForceCutAxis);
	}
	return p;
}


// accessor
const ptDataKdNode * ptKdTree::root() const {
	return m_pRoot;
};



// accessor
PT_LIST * ptKdTree::perm() {
	return m_pPerm;
};


// a selection algorithm written by someone called wirth. This is the
// only selection algo I could find with no bugs. Don't ask me
// how it works coz I aint got a 
void ptKdTree::wirthSelect(  int left,  int right,  int k, axis cutAxis )  
{
	int n = (right - left) + 1;
	if (n <= 1) return;
	register int i,j,l,m;
	ptData *x;
	ptData *tmp;
	
	l=left;
	m=right;
	while (l<m) {
		x = (*m_pPerm)[k];
		i=l;
		j=m;
		do {
			while (  ((*m_pPerm)[i])->position(cutAxis) <  x->position(cutAxis)  ) i++;
			while (  ((*m_pPerm)[j])->position(cutAxis) >  x->position(cutAxis)  ) j--;
			
			if (i<=j) {
				tmp = (*m_pPerm)[i];  
				(*m_pPerm)[i] =(*m_pPerm)[j] ;  
				(*m_pPerm)[j] =tmp ;
				i++; j--;
			}
		} while(i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
}



// The idea is just to find the axis containing the longest
// side of the bounding rectangle of the points
// From a list of N points we just take sqrtN samples
// so this keeps the calculation time down to O(N)
// which is usually ok
const axis ptKdTree::findMaxAxis(  int low,   int high) const {
	int  num = (high - low ) +1;
	int interval = int(sqrt(float(num)));
	int i;
	MVector p = ((*m_pPerm)[low])->position();
	
	double minx , miny,  minz , maxx , maxy , maxz, tmpVal;
	double sx, sy, sz;
	
	minx = p.x;
	maxx = minx;
	miny = p.y;
	maxy = miny;
	minz = p.z;
	maxz = minz;
	
	for (i= (low + interval); i<=high;i+=interval ) {
		p = ((*m_pPerm)[i])->position();
		tmpVal= p.x;
		if (tmpVal < minx) {
			minx = tmpVal;
		} else {
			if (tmpVal > maxx) {
				maxx = tmpVal;
			}
		}
		tmpVal= p.y;
		if (tmpVal < miny) {
			miny = tmpVal;
		} else {
			if (tmpVal > maxy) {
				maxy = tmpVal;
			}
		}
		tmpVal= p.z;
		if (tmpVal < minz) {
			minz = tmpVal;
		} else {
			if (tmpVal > maxz) {
				maxz = tmpVal;
			}
		}
	}
	
	sx = maxx - minx;
	sy = maxy - miny;
	sz = maxz - minz;
	
	if (sx > sy) {
		// y is not the greatest
		if (sx > sz) {
			return mayaMath::xAxis;
		} else {
			return mayaMath::zAxis;
		}
	} else {
		// x is not the greatest
		if (sy > sz) {
			return mayaMath::yAxis;
		} else {
			return mayaMath::zAxis;
		}
	}
}


void  ptKdTree::fixedRadiusSearch(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	const double & radius, 
	MIntArray & result
)  const{
	if (!p) return;
	if (p->bucket) {
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			if (pointInRadius(i , searchPoint , radius)) {
				result.append((*m_pPerm)[i]->id());
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			fixedRadiusSearch(p->loChild,searchPoint,radius,result);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				fixedRadiusSearch(p->hiChild,searchPoint,radius,result);
			}
		} else { // we are in the hi child so search it 
			fixedRadiusSearch(p->hiChild,searchPoint,radius,result);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				fixedRadiusSearch(p->loChild,searchPoint,radius,result);
			}
		}
	}
}

// do a fixed radius search and return the result points in a PT_LIST
void  ptKdTree::fixedRadiusSearch(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	const double & radius, 
	PT_LIST & result
)  const  {
	if (!p) return;
	if (p->bucket) {
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			if (pointInRadius(i , searchPoint , radius)) {
				result.push_back((*m_pPerm)[i]);
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			fixedRadiusSearch(p->loChild,searchPoint,radius,result);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				fixedRadiusSearch(p->hiChild,searchPoint,radius,result);
			}
		} else { // we are in the hi child so search it 
			fixedRadiusSearch(p->hiChild,searchPoint,radius,result);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				fixedRadiusSearch(p->loChild,searchPoint,radius,result);
			}
		}
	}
}

void  ptKdTree::closestNPts(
							const ptDataKdNode * p,
							const MVector & searchPoint,
							KNN_PD_QUEUE *q
							)  const  {
	// NOTE - The priority queue should already have the right number of elements in it
	// i.e. the number of nearest neighbors we are looking for
	if (!p) return;
	if (p->bucket) {
		double dl;
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			dl = (((*m_pPerm)[i])->position() - searchPoint).length();
			// the tolerance check ensures that we are not looking at ourself
			if ((P_TOLERANCE < dl) && (dl < q->top().dist)) {
				q->pop();
				knnSearchPdData ksd;
				
				ksd.dist = dl;
				ksd.pd =  ((*m_pPerm)[i]);
				q->push(ksd)  ;
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closestNPts(p->loChild,searchPoint,q);
			if (q->top().dist >= -diff) { // if radius overlaps the hi child then search that too
				closestNPts(p->hiChild,searchPoint,q);
			}
		} else { // we are in the hi child so search it
			closestNPts(p->hiChild,searchPoint,q);
			if (q->top().dist >= diff) { // if radius overlaps the lo child then search that too
				closestNPts(p->loChild,searchPoint,q);
			}
		}
	}
}


void  ptKdTree::closestNPtsWithinNormalThresh(
							const ptDataKdNode * p,
							const MVector & searchPoint,
							KNN_PD_QUEUE *q,
							const MVector & normal,
							const double &thresh
							)  const  {
	// this method finds n closest points - however a further test takes place
	// i.e. - that their associated normals are not facing too far away from each other.
	// This will stop points from affecting points who are close but on the other side of a
	// surface
	
	
	// NOTE - The priority queue should already have the right number of elements in it
	// i.e. the number of nearest neighbors we are looking for
	if (!p) return;
	if (p->bucket) {
		double dl;

		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			if ((((*m_pPerm)[i])->normal() * normal ) > thresh) { // test normals
				
				dl = (((*m_pPerm)[i])->position() - searchPoint).length();
				// the tolerance check ensures that we are not looking at ourself
				if ((P_TOLERANCE < dl) && (dl < q->top().dist)) {
					q->pop();
					knnSearchPdData ksd;
					
					ksd.dist = dl;
					ksd.pd =  ((*m_pPerm)[i]);
					q->push(ksd)  ;
				}
			}else {

			}
			
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closestNPtsWithinNormalThresh(p->loChild,searchPoint,q, normal, thresh);
			if (q->top().dist >= -diff) { // if radius overlaps the hi child then search that too
				closestNPtsWithinNormalThresh(p->hiChild,searchPoint,q, normal, thresh);
			}
		} else { // we are in the hi child so search it
			closestNPtsWithinNormalThresh(p->hiChild,searchPoint,q, normal, thresh);
			if (q->top().dist >= diff) { // if radius overlaps the lo child then search that too
				closestNPtsWithinNormalThresh(p->loChild,searchPoint,q, normal, thresh);
			}
		}
	}
}


/**/
// find the two nearest neighbors
// the MDoubleArray dists will always hold the closest point in element 0 and the
// other closest in elenment 1
// In effect element 1 is the search radius. It will be progressively shrunk as we
// find closer points.
// with this info we can do a voronoi kind of thing - i.e. value = shortest / longest
void  ptKdTree::closest2Dists(
  	 const ptDataKdNode * p,
  	 const MVector & searchPoint,
  	 MDoubleArray &dists
)  const  {
	if (!p) return;
	if (p->bucket) {
		// MVector diff;
		double dl;
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			dl = (((*m_pPerm)[i])->position() - searchPoint).length();
         //cerr << "dl = " << dl << endl;
			// here is where we make sure that the 2 lowest values out of
			// radius, dist[0] and dist[1] are in the array in ascending order
			// Whatever happens, we set radius to the larger of the two 
			if ((dl < dists[0]) ) {
				dists[1] = dists[0];
				dists[0] = dl;
			} else if (dl < dists[1]) {
				dists[1] = dl;	

			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closest2Dists(p->loChild,searchPoint,dists);
			if (dists[1] >= -diff) { // if radius overlaps the hi child then search that too
				closest2Dists(p->hiChild,searchPoint,dists);
			}
		} else { // we are in the hi child so search it
			closest2Dists(p->hiChild,searchPoint,dists);
			if (dists[1] >= diff) { // if radius overlaps the lo child then search that too
				closest2Dists(p->loChild,searchPoint,dists);
			}
		}
	}
 }


// this is the same as closest2Dists, except we return the points too

void  ptKdTree::closest2Pts(
  		 const ptDataKdNode * p,
  		 const MVector & searchPoint,
  		 MDoubleArray &dists,
  		 MVectorArray &pts
)  const  {
	if (!p) return;
	if (p->bucket) {
		double dl;
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			dl = (((*m_pPerm)[i])->position() - searchPoint).length();
			if ((dl < dists[0]) ) {
				dists[1] = dists[0];
				dists[0] = dl;
				pts[1] = pts[0];
				pts[0] = ((*m_pPerm)[i])->position();
			} else if (dl < dists[1]) {
				dists[1] = dl;
				pts[1] = ((*m_pPerm)[i])->position();
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closest2Pts(p->loChild,searchPoint,dists,pts);
			if (dists[1] >= -diff) { // if radius overlaps the hi child then search that too
				closest2Pts(p->hiChild,searchPoint,dists,pts);
			}
		} else { // we are in the hi child so search it
			closest2Pts(p->hiChild,searchPoint,dists,pts);
			if (dists[1] >= diff) { // if radius overlaps the lo child then search that too
				closest2Pts(p->loChild,searchPoint,dists,pts);
			}
		}
	}
 }



//The search radius is shrunk progressively to the closest point
// Therefore, the radius IS the distance to the closest point by the time we exit

void  ptKdTree::closestDist(
						   const ptDataKdNode * p,
						   const MVector & searchPoint,
						   double & radius)  const  {
	if (!p) return;
	if (p->bucket) {
		double dl;
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			dl = (((*m_pPerm)[i])->position() - searchPoint).length();
			if ((dl < radius) ) {
				radius = dl;    // as we are only interested in distance to the point, we can just measure the radius on exit
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closestDist(p->loChild,searchPoint,radius);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				closestDist(p->hiChild,searchPoint,radius);
			}
		} else { // we are in the hi child so search it
			closestDist(p->hiChild,searchPoint,radius);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				closestDist(p->loChild,searchPoint,radius);
			}
		}
	}
}

// this algo adds up all the repulsive forces of points within a given radius.

// It's basically a shortcult. You could use a fixedRadiusSearch and get
// the points back to the calling function, and then calculate the repulsion
// but we do it here because its theoretically a bit more efficient
/*
void  ptKdTree::calculateOffset(
							  const ptDataKdNode * p,
							  const MVector & searchPoint,
							  const double & radius,
							  MVector & result
							  )  const  {
	if (!p) return;
	if (p->bucket) {
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			result += calcRepulsion(i , searchPoint, radius );
		}

	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			calculateOffset(p->loChild,searchPoint, radius, result);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				calculateOffset(p->hiChild,searchPoint,radius,result);
			}
		} else { // we are in the hi child so search it
			calculateOffset(p->hiChild,searchPoint,radius,result);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				calculateOffset(p->loChild,searchPoint,radius,result);
			}
		}
	}
}
*/
double ptKdTree::calculateOffsetAndReturnRadius(
												const ptDataKdNode * p,
												const MVector & searchPoint,
												int neighbors,											
												MVector & result
												) const {
	
	double radius = 0;
	if (neighbors > 0 ) {
		KNN_PD_QUEUE * q = new KNN_PD_QUEUE;
		for (int n = 0;n<neighbors;n++) {
			knnSearchPdData k;
			k.dist = bigNum;
			k.pd=0;
			q->push(k);
		}
		closestNPts(p,searchPoint,q);
		// now we have the closest points in a priority queue - near to far.
		// the largest radius stored is the result radius
		
		
		MVectorArray pts;
		MDoubleArray dists ;
		bool foundAPoint = false;
		while (!q->empty()) {
			if (q->top().pd) {
				if (!foundAPoint) {
					radius = q->top().dist ;
					foundAPoint =true;
				}
				if (q->top().dist  > P_TOLERANCE)  {
					result += (searchPoint - q->top().pd->position() ).normal() *  (1.0 - (q->top().dist / radius));    // linear distance decay of repulsive force
				}
			}
			q->pop();
		}
		delete q;			
	}
	return radius;
}


double ptKdTree::calculateOffsetAndReturnRadius(
	const ptDataKdNode * p,
	const MVector & searchPoint,
	const MVector & normal,
	double thresh,
	int neighbors,											
	MVector & result
) const {

	double radius = 0;
	if (neighbors > 0 ) {
		KNN_PD_QUEUE * q = new KNN_PD_QUEUE;
		for (int n = 0;n<neighbors;n++) {
			knnSearchPdData k;
			k.dist = bigNum;
			k.pd=0;
			q->push(k);
		}
		closestNPtsWithinNormalThresh(p,searchPoint,q , normal, thresh);
	   // now we have the closest points in a priority queue - near to far.
	   // the largest radius stored is the result radius
		
		
	   MVectorArray pts;
		MDoubleArray dists ;
		bool foundAPoint = false;
		while (!q->empty()) {
			if (q->top().pd) {
				if (!foundAPoint) {
					radius = q->top().dist ;
					foundAPoint =true;
				}
				if (q->top().dist  > P_TOLERANCE)  {
					result += (searchPoint - q->top().pd->position() ).normal() *  (1.0 - (q->top().dist / radius));    // linear distance decay of repulsive force
				}
			}
			q->pop();
		}
		delete q;					
	}
	return radius;
}


void  ptKdTree::closestNPts(
  		 const ptDataKdNode * p,
  		 const MVector & searchPoint,
       KNN_QUEUE &q
)  const  {
	// NOTE - The priority queue should already have the right number of elements in it
	// i.e. the number of nearest neighbors we are looking for
	if (!p) return;
	if (p->bucket) {
		double dl;
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			dl = (((*m_pPerm)[i])->position() - searchPoint).length();
			// the tolerance check ensures that we are not looking at ourself
			if ((P_TOLERANCE < dl) && (dl < q.top().dist)) {
				q.pop();
				knnSearchData ksd;
				ksd.dist = dl;
				ksd.point =  ((*m_pPerm)[i])->position() ;
				ksd.velocity =  ((*m_pPerm)[i])->velocity() ;
				q.push(ksd)  ;
			}
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			closestNPts(p->loChild,searchPoint,q);
			if (q.top().dist >= -diff) { // if radius overlaps the hi child then search that too
				closestNPts(p->hiChild,searchPoint,q);
			}
		} else { // we are in the hi child so search it
			closestNPts(p->hiChild,searchPoint,q);
			if (q.top().dist >= diff) { // if radius overlaps the lo child then search that too
				closestNPts(p->loChild,searchPoint,q);
			}
		}
	}
}



// return a repulsive force on the search point
MVector ptKdTree::calcRepulsion(  int i , const MVector & searchPoint, const double &radius ) const {
	MVector result(MVector::zero);
	MVector   diff = (searchPoint - ((*m_pPerm)[i])->position()  );
	double dl = diff.length() ;
	if ((dl < radius) ) { 
		if ( dl > P_TOLERANCE)  { // this checks that the point we found is not the search point itself
			result = diff.normal() *  (1.0 - (dl / radius));    // linear distance decay of repulsive force
		}
	}
	return result;
}


// is the point within the radius
bool  ptKdTree::pointInRadius(  int i , const MVector & searchPoint ,const double & radius) const {
	MVector  diff = (((*m_pPerm)[i])->position() - searchPoint);
	double dl =  diff.length() ;
	if ((dl < radius) ) { 
		if ( dl > P_TOLERANCE) return 1;
	} 
	return 0;
}


void  ptKdTree::gatherForces(
						   const ptDataKdNode * p, 
						   const MVector & searchPoint, 
						   const MVector & searchVelocity, 
						   const double &  radius,
							 const lookup * rfl,
							 const lookup * nfl,
							 const lookup * gfl,
							 const lookup * ifl,
						   MVector & result ,
						   int & count
						   )  const  {
	if (!p) return;
	// if (p->empty) return;
	if (p->bucket) {
		for (unsigned int i = p->loPoint; i <= p->hiPoint;i++ ) {
			
			result+= ((*m_pPerm)[i])->forceAt(searchPoint,searchVelocity,radius,rfl,nfl,gfl,ifl,count) ;
			
		}
	} else {
		double diff = searchPoint[(p->cutAxis)] - p->cutVal; // distance to the cut wall for this bucket
		if (diff<0) { // we are in the lo child so search it
			gatherForces(p->loChild,searchPoint,searchVelocity,radius,rfl,nfl,gfl,ifl,result,count);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				gatherForces(p->hiChild,searchPoint,searchVelocity,radius,rfl,nfl,gfl,ifl,result,count);
			}
		} else { // we are in the hi child so search it 
			gatherForces(p->hiChild,searchPoint,searchVelocity,radius,rfl,nfl,gfl,ifl,result,count);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				gatherForces(p->loChild,searchPoint,searchVelocity,radius,rfl,nfl,gfl,ifl,result,count);
			}
		}
	}
}

