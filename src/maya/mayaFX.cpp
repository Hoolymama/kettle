#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPointArray.h>

#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>
#include <maya/MString.h>
#include <maya/MPlug.h>
#include <maya/MRenderUtil.h>

#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFloatPoint.h>
#include <maya/MTime.h>
#include "errorMacros.h"
#include "mayaFX.h"



bool mayaFX::sampleTexture(
  const MObject &node,
  const MObject &attr,
  MFloatArray &uCoords,
  MFloatArray &vCoords,
  MFloatPointArray &samplePoints,
  MFloatVectorArray &sampleNormals,
  MFloatArray &resValues
)  {
	const double oneThird = 0.3333333333333333333333333;

	MStatus st;
	MPlugArray plugArray;
	MPlug plug(node, attr);

	bool doSampling = plug.connectedTo(plugArray, 1, 0, &st); mser;
	if ((! doSampling) || (st.error()) ) { return false; }
	unsigned count = uCoords.length();

	MString name;
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	name = plugArray[0].name(&st);
	if ( st.error() ) { return false; }
	resValues.setLength(count);
	MFloatVectorArray resColors;
	MFloatVectorArray resTransparencies;
	st =  MRenderUtil::sampleShadingNetwork (
	        name,			   	// shadingNodeName Name of the shading node/shading engine
	        count,	   			// numSamples Number of samples to be calculated
	        false,  		   	// useShadowMaps Whether to calculate shadows
	        false,  		   	// reuseMaps If calculating shadows, whether to reuse shadowmaps
	        cameraMat,		   	// cameraMatrix The eyeToWorld matrix to be used for conversion
	        &samplePoints,		// points Locations to be sampled in world space
	        &uCoords,		   	// uCoords U coordinates of the samples
	        &vCoords,		   	// vCoords V coordinates of the samples
	        &sampleNormals,		// normals Normals at the sample points in world space
	        &samplePoints,		// refPoints RefPoints to be used for 3D texture in world space
	        NULL, 			   	// tangentUs U tangents at the sample points in world space
	        NULL,			   	// tangentVs V tangents at the sample points in world space
	        NULL, 			   	// filterSizes Filter sizes to be used for 2D/3D textures
	        resColors,		   	// resultColors Storage for result colors
	        resTransparencies  	// resultTransparencies storage for result transparencies
	      ); mser;

	if ( st.error() ) {
		return false;
	}
	else {
		for (unsigned i = 0; i < count; ++i)
		{
			resValues.set(resColors[i].x, i);
		}
	}

	return true;
}

bool mayaFX::sampleTexture(
  const MObject &node,
  const MObject &attr,
  MVectorArray &samplePoints,
  MFloatArray &resColors
)  {
	unsigned pl = samplePoints.length();
	MFloatPointArray fSamplePoints(pl);
	for (int i = 0; i < samplePoints.length(); ++i)
	{
		fSamplePoints.set(MFloatPoint(samplePoints[i].x, samplePoints[i].y, samplePoints[i].z),
		                  i);
	}
	return sampleTexture(node, attr, fSamplePoints, resColors);
}


bool mayaFX::sampleTexture(
  const MObject &node,
  const MObject &attr,
  MFloatPointArray &samplePoints,
  MFloatArray &resValues
)  {

	MStatus st;
	MPlugArray plugArray;
	MPlug plug(node, attr);

	bool doSampling = plug.connectedTo(plugArray, 1, 0, &st); mser;
	if ((! doSampling) || (st.error()) ) { return false; }
	unsigned count = samplePoints.length();
	MFloatVectorArray resColors;
	MString name;
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	name = plugArray[0].name(&st);
	if ( st.error() ) { return false; }

	MFloatVectorArray resTransparencies;
	st =  MRenderUtil::sampleShadingNetwork (
	        name,			   	// shadingNodeName Name of the shading node/shading engine
	        count,	   			// numSamples Number of samples to be calculated
	        false,  		   	// useShadowMaps Whether to calculate shadows
	        false,  		   	// reuseMaps If calculating shadows, whether to reuse shadowmaps
	        cameraMat,		   	// cameraMatrix The eyeToWorld matrix to be used for conversion
	        &samplePoints,		// points Locations to be sampled in world space
	        NULL,		   	// uCoords U coordinates of the samples
	        NULL,		   	// vCoords V coordinates of the samples
	        NULL,		// normals Normals at the sample points in world space
	        &samplePoints,		// refPoints RefPoints to be used for 3D texture in world space
	        NULL, 			   	// tangentUs U tangents at the sample points in world space
	        NULL,			   	// tangentVs V tangents at the sample points in world space
	        NULL, 			   	// filterSizes Filter sizes to be used for 2D/3D textures
	        resColors,		   	// resultColors Storage for result colors
	        resTransparencies  	// resultTransparencies storage for result transparencies
	      );


	if ( st.error() ) {
		return false;
	}
	else {
		resValues.setLength(count);
		for (unsigned i = 0; i < count; ++i)
		{
			resValues.set(resColors[i].x, i);
		}
	}
	return true;
}

bool mayaFX::sampleTexture(
  const MObject &node,
  const MObject &attr,
  MFloatArray &uCoords,
  MFloatArray &vCoords,
  MFloatPointArray &samplePoints,
  MFloatVectorArray &sampleNormals,
  MFloatVectorArray &resColors
)  {

	MStatus st;
	MPlugArray plugArray;
	MPlug plug(node, attr);

	bool doSampling = plug.connectedTo(plugArray, 1, 0, &st); mser;
	if ((! doSampling) || (st.error()) ) { return false; }
	unsigned count = uCoords.length();

	MString name;
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	name = plugArray[0].name(&st);
	if ( st.error() ) { return false; }

	MFloatVectorArray resTransparencies;
	st =  MRenderUtil::sampleShadingNetwork (
	        name,			   	// shadingNodeName Name of the shading node/shading engine
	        count,	   			// numSamples Number of samples to be calculated
	        false,  		   	// useShadowMaps Whether to calculate shadows
	        false,  		   	// reuseMaps If calculating shadows, whether to reuse shadowmaps
	        cameraMat,		   	// cameraMatrix The eyeToWorld matrix to be used for conversion
	        &samplePoints,		// points Locations to be sampled in world space
	        &uCoords,		   	// uCoords U coordinates of the samples
	        &vCoords,		   	// vCoords V coordinates of the samples
	        &sampleNormals,		// normals Normals at the sample points in world space
	        &samplePoints,		// refPoints RefPoints to be used for 3D texture in world space
	        NULL, 			   	// tangentUs U tangents at the sample points in world space
	        NULL,			   	// tangentVs V tangents at the sample points in world space
	        NULL, 			   	// filterSizes Filter sizes to be used for 2D/3D textures
	        resColors,		   	// resultColors Storage for result colors
	        resTransparencies  	// resultTransparencies storage for result transparencies
	      );

	if ( st.error() ) { return false; }

	return true;

}

// Force Accumulator procedure for gathering maya's force fields
MStatus mayaFX::collectExternalForces(
  const MObject &node,
  MDataBlock &data,
  const MVectorArray &positions,
  const MVectorArray &velocities,
  const MDoubleArray &masses,
  const MTime &dT,
  MVectorArray &force)
{

	MStatus st;
	MString method("mayaFX::collectExternalForces");

	MFnDependencyNode depFn(node, &st);
	//	cerr << "collectExternalForces 1" << endl;
	MObject forcesAttr = depFn.attribute("forces", &st); mser;
	MObject aSampleFieldData = depFn.attribute("sampleFieldData", &st); mser;
	MObject aSamplePoints = depFn.attribute("samplePoints", &st); mser;
	MObject aSampleVelocities = depFn.attribute("sampleVelocities", &st); mser;
	MObject aSampleMasses = depFn.attribute("sampleMasses", &st); mser;
	MObject aSampleDeltaTime = depFn.attribute("sampleDeltaTime", &st); mser;
	//	cerr << "collectExternalForces 2" << endl;

	MPlug forcesPlug(node, forcesAttr);
	unsigned numForces = forcesPlug.numElements();
	//	cerr << "collectExternalForces 3" << endl;

	// send the sample data out to the forces?
	////////////////////////////////////////////////////////////////
	MDataHandle hSampleFieldData = data.outputValue(aSampleFieldData, &st ); mser;
	MDataHandle hSamplePoints = hSampleFieldData.child(aSamplePoints);
	MDataHandle hSampleVelocities = hSampleFieldData.child(aSampleVelocities );
	MDataHandle hSampleMasses = hSampleFieldData.child(aSampleMasses );
	MDataHandle hSampleDeltaTime = hSampleFieldData.child(aSampleDeltaTime );
	//cerr << "collectExternalForces 4" << endl;

	MFnVectorArrayData fnSamplePoints;
	MObject dSamplePoints = fnSamplePoints.create( positions, &st ); mser;
	MFnVectorArrayData fnSampleVelocities;
	MObject dSampleVelocities = fnSampleVelocities.create( velocities, &st ); mser;
	MFnDoubleArrayData fnSampleMasses;
	MObject dSampleMasses = fnSampleMasses.create( masses, &st ); mser;
	//	cerr << "collectExternalForces 5" << endl;

	hSamplePoints.set(dSamplePoints);
	//	cerr << "collectExternalForces 5.1" << endl;
	hSampleVelocities.set(dSampleVelocities);
	//	cerr << "collectExternalForces 5.2" << endl;
	hSampleMasses.set(dSampleMasses);
	//	cerr << "collectExternalForces 5.3" << endl;
	// cerr << "dT: " << dT << endl;
	// cerr << "hSampleDeltaTime isNumeric" << hSampleDeltaTime.isNumeric() << endl;
	// cerr << "hSampleDeltaTime type" << hSampleDeltaTime.numericType() << endl;

	hSampleDeltaTime.set(dT);
	//	cerr << "collectExternalForces 6" << endl;

	data.setClean(aSamplePoints);
	data.setClean(aSampleVelocities);
	data.setClean(aSampleMasses);
	data.setClean(aSampleDeltaTime);
	data.setClean(aSampleFieldData);
	////////////////////////////////////////////////////////////////
	//	cerr << "collectExternalForces 7" << endl;

	// accumulate the forces
	////////////////////////////////////////////////////////////////
	unsigned len = positions.length();
	force.setLength(len);
	for (unsigned t = 0; t < len; t++ ) { force[t] = MVector::zero; }
	//	cerr << "collectExternalForces 8" << endl;

	for (unsigned i = 0; i < numForces; i++) {
		MPlug tmpForcePlug = forcesPlug.elementByPhysicalIndex(i);
		if (tmpForcePlug.isConnected()) {
			MObject tmpForceObject;
			st = tmpForcePlug.getValue(tmpForceObject); mser;
			if (st == MS::kSuccess) {
				MFnVectorArrayData tmpForceFn(tmpForceObject);
				MVectorArray tmpForce = tmpForceFn.array(&st);
				if (st == MS::kSuccess) {
					if (tmpForce.length() == unsigned(len)) {
						for (unsigned t = 0; t < len; t++ ) {
							force[t] += tmpForce[t];
						}
					}
				}
			}
		}
	}
	//	cerr << "collectExternalForces 9" << endl;

	return MS::kSuccess;
}







bool mayaFX::getDynTriangleArrays(
  double dtRecip,
  const MVectorArray &lastVelocity,
  unsigned lastTriangleCount,
  MObject &sweptData,
  MVectorArray &centers,
  MVectorArray &velocities,
  MVectorArray &accelerations,
  MDoubleArray &areas,
  double &totalArea,
  MFloatArray &uCoords,
  MFloatArray &vCoords,
  MFloatPointArray &samplePoints,
  MFloatVectorArray &sampleNormals
)
{
	const double oneThird = 0.3333333333333333333333333;
	MFnDynSweptGeometryData fnSweptData( sweptData );
	unsigned int numTriangles = fnSweptData.triangleCount();
	bool triangleCountChanged =  (numTriangles != lastTriangleCount) ;
	if (! numTriangles)  { return MS::kSuccess; }


	centers.setLength(numTriangles);
	velocities.setLength(numTriangles);
	accelerations.setLength(numTriangles);
	areas.setLength(numTriangles);
	MVector uvVec;
	uCoords.setLength(numTriangles);
	vCoords.setLength(numTriangles);
	samplePoints.setLength(numTriangles);
	sampleNormals.setLength(numTriangles);

	MFloatPoint fp;

	for (unsigned i = 0; i < numTriangles; i++ )
	{
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( i );
		MVector center0 = ((( tri.vertex( 0 , 0.0) + tri.vertex( 1 , 0.0) + tri.vertex( 2 ,
		                      0.0)) * oneThird));
		MVector &c = centers[i];
		c = (( tri.vertex( 0 , 1.0) + tri.vertex( 1 , 1.0) + tri.vertex( 2 , 1.0)) * oneThird);
		velocities.set(((centers[i] - center0) * dtRecip), i);
		areas.set( tri.area(), i);
		totalArea += areas[i];

		fp.setCast(c);
		samplePoints.set( fp, i);

		uvVec =  tri.uvPoint(0);
		uvVec += tri.uvPoint(1);
		uvVec += tri.uvPoint(2);
		uvVec *= oneThird; //  UV value  at center of the triangle
		uCoords[i] = float(uvVec.x);
		vCoords[i] = float(uvVec.y);

		sampleNormals.set(MFloatVector(tri.normal()), i);
	}

	// if lastVelocity array is valid, calc accelerations , otherwise set accelerations to zero
	if (! triangleCountChanged) {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( ((velocities[i] - lastVelocity[i]) * dtRecip), i);
		}
	}
	else {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( MVector::zero, i);
		}
	}
	return triangleCountChanged;
}

bool mayaFX::getDynTriangleArrays(
  double dtRecip,
  const MVectorArray &lastVelocity,
  unsigned lastTriangleCount,
  MObject &sweptData,
  MVectorArray &centers,
  MVectorArray &velocities,
  MVectorArray &accelerations,
  MDoubleArray &areas,
  double &totalArea
)
{
	const double oneThird = 0.3333333333333333333333333;
	MFnDynSweptGeometryData fnSweptData( sweptData );
	unsigned int numTriangles = fnSweptData.triangleCount();
	bool triangleCountChanged =  (numTriangles != lastTriangleCount) ;
	if (! numTriangles)  { return MS::kSuccess; }


	centers.setLength(numTriangles);
	velocities.setLength(numTriangles);
	accelerations.setLength(numTriangles);
	areas.setLength(numTriangles);

	for (unsigned i = 0; i < numTriangles; i++ )
	{
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( i );
		MVector center0 = ((( tri.vertex( 0 , 0.0) + tri.vertex( 1 , 0.0) + tri.vertex( 2 ,
		                      0.0)) * oneThird));
		centers.set((( tri.vertex( 0 , 1.0) + tri.vertex( 1 , 1.0) + tri.vertex( 2 ,
		               1.0)) * oneThird),   i);
		velocities.set(((centers[i] - center0) * dtRecip), i);
		areas.set( tri.area(), i);
		totalArea += areas[i];
	}

	// if lastVelocity array is valid, calc accelerations , otherwise set accelerations to zero
	if (! triangleCountChanged) {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( ((velocities[i] - lastVelocity[i]) * dtRecip), i);
		}
		//	cerr << accelerations << endl;
	}
	else {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( MVector::zero, i);
		}
	}

	return triangleCountChanged;
}

bool mayaFX::getDynTriangleArrays(
  double dtRecip,
  const MVectorArray &lastVelocity,
  unsigned lastTriangleCount,
  MObject &sweptData,
  MVectorArray &centers0,
  MVectorArray &centers1,
  MVectorArray &velocities,
  MVectorArray &accelerations,
  MFloatArray &uCoords,
  MFloatArray &vCoords,
  MFloatPointArray &samplePoints,
  MFloatVectorArray &sampleNormals
)
{
	const double oneThird = 0.3333333333333333333333333;
	MFnDynSweptGeometryData fnSweptData( sweptData );
	unsigned int numTriangles = fnSweptData.triangleCount();
	bool triangleCountChanged =  (numTriangles != lastTriangleCount) ;
	if (! numTriangles)  { return MS::kSuccess; }


	centers0.setLength(numTriangles);
	centers1.setLength(numTriangles);
	velocities.setLength(numTriangles);
	accelerations.setLength(numTriangles);
	// areas.setLength(numTriangles);
	MVector uvVec;
	uCoords.setLength(numTriangles);
	vCoords.setLength(numTriangles);
	samplePoints.setLength(numTriangles);
	sampleNormals.setLength(numTriangles);

	MFloatPoint fp;

	for (unsigned i = 0; i < numTriangles; i++ )
	{
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( i );
		centers0.set((( tri.vertex( 0 , 0.0) + tri.vertex( 1 , 0.0) + tri.vertex( 2 ,
		                0.0)) * oneThird),   i);
		centers1.set((( tri.vertex( 0 , 1.0) + tri.vertex( 1 , 1.0) + tri.vertex( 2 ,
		                1.0)) * oneThird),   i);
		velocities.set(((centers1[i] - centers0[i]) * dtRecip), i);
		// areas.set( tri.area(),i);
		// totalArea += areas[i];

		fp.setCast(centers1[i]);
		samplePoints.set( fp, i);

		uvVec =  tri.uvPoint(0);
		uvVec += tri.uvPoint(1);
		uvVec += tri.uvPoint(2);
		uvVec *= oneThird; //  UV value  at center of the triangle
		uCoords[i] = float(uvVec.x);
		vCoords[i] = float(uvVec.y);

		sampleNormals.set(MFloatVector(tri.normal()), i);
	}

	// if lastVelocity array is valid, calc accelerations , otherwise set accelerations to zero
	if (! triangleCountChanged) {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( ((velocities[i] - lastVelocity[i]) * dtRecip), i);
		}
	}
	else {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( MVector::zero, i);
		}
	}
	return triangleCountChanged;
}

bool mayaFX::getDynTriangleArrays(
  double dtRecip,
  const MVectorArray &lastVelocity,
  unsigned lastTriangleCount,
  MObject &sweptData,
  MVectorArray &centers0,
  MVectorArray &centers1,
  MVectorArray &velocities,
  MVectorArray &accelerations
)
{
	const double oneThird = 0.3333333333333333333333333;
	MFnDynSweptGeometryData fnSweptData( sweptData );
	unsigned int numTriangles = fnSweptData.triangleCount();
	bool triangleCountChanged =  (numTriangles != lastTriangleCount) ;
	if (! numTriangles)  { return MS::kSuccess; }


	centers0.setLength(numTriangles);
	centers1.setLength(numTriangles);
	velocities.setLength(numTriangles);
	accelerations.setLength(numTriangles);
	// areas.setLength(numTriangles);

	for (unsigned i = 0; i < numTriangles; i++ )
	{
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( i );
		// MVector center0 = ((( tri.vertex( 0 ,0.0)+ tri.vertex( 1 ,0.0) + tri.vertex( 2 ,0.0)) * oneThird));
		centers0.set((( tri.vertex( 0 , 0.0) + tri.vertex( 1 , 0.0) + tri.vertex( 2 ,
		                0.0)) * oneThird),   i);
		centers1.set((( tri.vertex( 0 , 1.0) + tri.vertex( 1 , 1.0) + tri.vertex( 2 ,
		                1.0)) * oneThird),   i);
		velocities.set(((centers1[i] - centers0[i]) * dtRecip), i);
		// areas.set( tri.area(),i);
		// totalArea += areas[i];
	}

	// if lastVelocity array is valid, calc accelerations , otherwise set accelerations to zero
	if (! triangleCountChanged) {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( ((velocities[i] - lastVelocity[i]) * dtRecip), i);
		}
		//	cerr << accelerations << endl;
	}
	else {
		for  (unsigned i = 0; i < numTriangles; i++ )
		{
			accelerations.set( MVector::zero, i);
		}
	}

	return triangleCountChanged;
}




















