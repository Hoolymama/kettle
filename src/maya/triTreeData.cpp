/***************************************************************************
triTreeData.cpp  -  description
-------------------
begin                : Fri Mar 31 2006
copyright            : (C) 2006 by Julian Mann
email                : julian.mann@gmail.com

this is the maya data object containing a triKdTree so that it can be passed
through the dependency graph.
It also contains some methods to make calls to the triKdTree methods
***************************************************************************/




//
// Copyright (C) 2000 hoolyMama

//
// Author: Julian Mann
//
#include <maya/MGlobal.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MDataHandle.h>

#include "triKdTree.h"
#include "triData.h"
#include "triTreeData.h"
#include "errorMacros.h"

#include "jMayaIds.h"

#define EPSILON 0.0000001

const double bigNum = 99999999999.0  ;

MTypeId triTreeData::id( k_triTreeData );
MString triTreeData::typeName( "triTreeData" );

triTreeData::triTreeData(): m_pTree(0)
{
	//	m_pTree = 0;

}

triTreeData::~triTreeData()
{
	//   triKdTreeCacheMap::iterator it  = m_treeCacheMap.begin();
	//   for (;it !=m_treeCacheMap.end();it++) {
	//   	if (it->second) {
	//   		delete it->second;
	//   		it->second = 0;
	//   	}
	//   	m_treeCacheMap.erase(it);
	//   }
}

void *triTreeData::creator()
{
	return new triTreeData;
}

// the ptKdTree
triKdTree	*triTreeData::tree() const {return m_pTree;}

//triKdTreeCacheMap	triTreeData::treeMap() const {return m_treeCacheMap;}

// clean up
//void	triTreeData::clear() {
////	if (m_pTree) {delete m_pTree;m_pTree = 0;}
//}


void triTreeData::clear() {
	if (m_pTree) {delete m_pTree; m_pTree = 0;}

	// if  (tIter !=  m_treeCacheMap.end()) {

	// 	triKdTree * t = tIter->second;

	// //	if (t != m_pTree){
	// 		if (t) {
	// 			delete t;t = 0;
	// 		}
	// 		m_treeCacheMap.erase(tIter);
	// //	}
	// }
}


// void triTreeData::removeCachesOutsideRange(
// 	MTime cacheStartTime,
// 	MTime cacheEndTime
// ) {

// //	cerr << "in removeCachesOutsideRange" << endl;

// //	cerr << "cacheStartTime " << cacheStartTime << endl;
// //	cerr << "cacheStartTime " << cacheStartTime << endl;

// 	triKdTreeCacheMap::iterator itlow, itup, itbegin, itend;

// 	itlow=m_treeCacheMap.lower_bound(cacheStartTime);
// 	itup=m_treeCacheMap.upper_bound(cacheEndTime);
// 	itbegin=m_treeCacheMap.begin();
// 	itend=m_treeCacheMap.end();


// 	triKdTreeCacheMap::iterator tIter = itbegin;

// 	while ((tIter  !=  itlow) && (tIter != itend))	{
// 		clear(tIter);
// 		++tIter;
// 	}

// 	tIter = itup;
// 	while (tIter != itend)	{clear(tIter);++tIter;}
// }


MStatus triTreeData::create(MArrayDataHandle &ah) {


	MStatus st = MS::kSuccess;
	//	MString method("triTreeData::create" );
	//	cerr << "here 1" << endl;
	// removeCachesOutsideRange(cacheStartTime, cacheEndTime);
	//	cerr << "here 2" << endl;
	// bool doCaching = ((currentTime >= cacheStartTime) && (currentTime <= cacheEndTime));



	clear();

	unsigned n = ah.elementCount();

	// make a new tree
	m_pTree = new triKdTree();
	if (m_pTree)  {

		for (unsigned i = 0; i < n; i++, ah.next()) {
			int el = ah.elementIndex(&st);
			MDataHandle h = ah.inputValue();
			MObject  d = h.data();
			MFnDynSweptGeometryData fnG(d, &st);
			if (!(st.error())) {
				m_pTree->populate(fnG, el);
			}
		}
		m_pTree->build();
	}
	else {
		st = MS::kFailure;
	}
	return st;
	// NOTE: there is now a triKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree

}
MStatus triTreeData::create(
  const MPointArray &lastPoints,
  const MPointArray &points,
  const MIntArray &triangleVertices
) {
	MStatus st = MS::kSuccess;
	clear();
	// make a new tree
	m_pTree = new triKdTree();
	if (m_pTree)  {
		m_pTree->populate(lastPoints, points, triangleVertices);
		m_pTree->build();
	}
	else {
		st = MS::kFailure;
	}
	return st;
	// NOTE: there is now a triKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree
}


// MStatus triTreeData::create(
// 	MArrayDataHandle &ah,
// 	const MObject & childGeomPlug,
// 	const MObject & childActivePlug,
// 	MTime currentTime,
// 	MTime cacheStartTime,
// 	MTime cacheEndTime
// ) {
// 	// create new tree from an array of curves

// 	MStatus st = MS::kSuccess;
// //	MString method("triTreeData::create" );
// 	//cerr << method << endl;


// // 	cerr << method << endl;

// //	cerr << "removeCachesOutsideRange(cacheStartTime, cacheEndTime);" << cacheStartTime << " "<< cacheEndTime << endl;
// 	removeCachesOutsideRange(cacheStartTime, cacheEndTime);
// 	bool doCaching = ((currentTime >= cacheStartTime) && (currentTime <= cacheEndTime));

// //	cerr << "doCaching" << doCaching << endl;

// 	triKdTreeCacheMap::iterator tIter = m_treeCacheMap.find(currentTime);
// 	bool frameIsCached =  (tIter !=  m_treeCacheMap.end()) ;
// //	cerr << "frameIsCached" << frameIsCached << endl;

// 	if  (frameIsCached && doCaching) {
// 		m_pTree = tIter->second ;
// 	} else  {
// 	//	cerr << "making a cache" <<  currentTime << endl;
// 		// either the frame is not cached, or we should not be caching - therefore we need to make a cache for this frame
// 		if (frameIsCached) clear(tIter); // if its there

// 		unsigned n = ah.elementCount();

// 		m_pTree = new triKdTree();
// 		if (m_pTree) {
// 			m_treeCacheMap[currentTime] = m_pTree;

// 			if (childGeomPlug != MObject::kNullObj) {
// 				//cerr << "childGeomPlug != MObject::kNullObj" << endl;

// 				for(unsigned i = 0;i < n; i++, ah.next()) {
// 					//int el = ah.elementIndex(&st);
// 					MDataHandle hCmp = ah.inputValue();
// 					bool active = hCmp.child(childActivePlug).asBool();
// 					//cerr << "plug "<< i << "  ---  active = " << active << endl;
// 					if (active) {
// 						MDataHandle hGeo = hCmp.child(childGeomPlug);
// 						MObject  d = hGeo.data();
// 						MFnDynSweptGeometryData fnG(d,&st);

// 						if (!(st.error())){
// 							//cerr << "success getting MFnGeo" << endl;
// 							m_treeCacheMap[currentTime]->populate(fnG, i);
// 						} else {
// 						//	cerr << "failure getting MFnGeo" << endl;

// 						}
// 					}
// 				}
// 			} else {

// 				//cerr << "childGeomPlug == MObject::kNullObj" << endl;
// 				for(unsigned i = 0;i < n; i++, ah.next()) {
// 					// int el = ah.elementIndex(&st);
// 					MDataHandle h = ah.inputValue();
// 					MObject  d = h.data();
// 					MFnDynSweptGeometryData fnG(d,&st);
// 					if (!(st.error())){
// 						// m_pTree->populate(fnG, el);
// 						m_treeCacheMap[currentTime]->populate(fnG, i);
// 					}
// 				}
// 			}
// 			m_treeCacheMap[currentTime]->build();
// 		}  else {
// 			st = MS::kFailure;
// 		}
// 		// NOTE: there is now a triKdTree which is either
// 		// null, has zero size, or has size
// 		// Any calling function should check the state of the tree
// 	}
// //
// 	return st;

// }



int triTreeData::size() {
	if (m_pTree)
	{ return m_pTree->size(); }
	else
	{ return 0; }
}
MStatus triTreeData::closestPointOnTri(
  const MVector &searchPoint,
  MVector &resultPoint,
  MVector &resultUV,
  MVector &resultBary
) const {

	MStatus st = MS::kSuccess;
	MString method("triTreeData::closestPointOnTri" );

	MPoint c = MPoint(searchPoint);
	double searchRad = bigNum;
	triData tb;
	m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
	if (searchRad < bigNum) {  // we found a point
		resultUV = MVector(tb.calcU(), tb.calcV(), 0.0 );
		resultPoint =  tb.cachedPoint() ;
		resultBary = tb.cachedBary() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

MStatus triTreeData::closestPointOnTri(
  const MVector &searchPoint,
  MVector &resultPoint
) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::closestPointOnTri" );

	MPoint c = MPoint(searchPoint);
	double searchRad = bigNum;
	triData tb;
	m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
	if (searchRad < bigNum) {  // we found a point
		resultPoint =  tb.cachedPoint() ;
		st = MS::kSuccess;
	}
	else {   // we never found a closest point, so something is seriously wrong
		st = MS::kFailure; mser;
	}
	return st;
}

MStatus triTreeData::closestPointsOnTri(
  const MVectorArray &searchPoints,
  MVectorArray &resultPoints,
  MVectorArray &resultUVs,
  MIntArray &resultIds,
  MVectorArray &resultBarys
) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::closestPointsOnTri" );

	unsigned len = searchPoints.length();
	resultPoints.setLength(len) ;
	resultIds.setLength(len) ;
	resultUVs.setLength(len) ;
	resultBarys.setLength(len) ;


	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < bigNum) {  // we found a point
			resultPoints.set( tb.cachedPoint(), i );
			resultUVs.set(MVector(tb.calcU(), tb.calcV(), 0.0 ), i);
			resultIds.set(tb.id(), i);
			resultBarys.set( tb.cachedBary() , i);
			//st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			resultPoints.set( MVector::zero , i);
			resultUVs.set(MVector::zero, i);
			resultIds.set(0, i);
			resultBarys.set( MVector::zero , i);
		}
	}
	return st;
}

MStatus triTreeData::appendClosestPointsOnTri(
  const MVectorArray &searchPoints,
  MVectorArray &resultPoints,
  MVectorArray &resultUVs,
  MIntArray &resultIds,
  MVectorArray &resultBarys
) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::appendClosestPointsOnTri" );
	unsigned len = searchPoints.length();
	unsigned currlen =	resultPoints.length();
	unsigned newlen = len + currlen;
	resultPoints.setLength(newlen) ;
	resultIds.setLength(newlen) ;
	resultUVs.setLength(newlen) ;
	resultBarys.setLength(newlen) ;


	MPoint c   ; // the center of the search sphere
	unsigned index = currlen;
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < bigNum) {  // we found a point
			resultPoints.set( tb.cachedPoint(), index );
			resultUVs.set(MVector(tb.calcU(), tb.calcV(), 0.0 ), index);
			resultIds.set(tb.id(), index);
			resultBarys.set( tb.cachedBary() , index);
			//st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			resultPoints.set( MVector::zero , index);
			resultUVs.set(MVector::zero, index);
			resultIds.set(0, index);
			resultBarys.set( MVector::zero , index);
		}
		index++;
	}
	return st;
}

MStatus triTreeData::closestDistancesToTriPlane(
  const MVectorArray &searchPoints,
  MFloatVectorArray &resultBarys,
  MFloatArray &resultDistances,
  MIntArray &resultIds,
  MIntArray &resultParentIds
) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::closestPointsOnTri" );

	unsigned len = searchPoints.length();
	//resultBarys.clear() ;
	resultBarys.setLength(len) ;
	resultDistances.setLength(len) ;
	resultIds.setLength(len) ;
	resultParentIds.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = bigNum;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < bigNum) {  // we found a point

			resultBarys.set(tb.cachedBary(), i);
			resultDistances.set( float( MVector(c - tb.cachedPoint()) * tb.normal() ) , i);
			resultIds.set(tb.id(), i);
			resultParentIds.set(tb.parentId(), i);

			//resultPoints.append( tb.cachedPoint() );
			//resultUVs.append(MVector(tb.calcU(),tb.calcV(),0.0 ));
			//st = MS::kSuccess;

		}
		else {   // we never found a closest point, so something is seriously wrong
			MGlobal::displayError("Problem in triTreeData::closestDistancesToTriPlane()");

			resultBarys.set( MVector::zero, i );
			resultDistances.set( 0.0f, i );
			resultIds.set(-1, i);
			resultParentIds.set(-1, i);
			//st = MS::kFailure; mser;
		}
	}
	return st;
}



/*
MStatus triTreeData::closestPointsOnTri(
const MVectorArray &searchPoints,
MVectorArray &resultPoints,
MVectorArray &resultNormals
) const {
MStatus st = MS::kSuccess;
MString method("triTreeData::closestPointsOnTri" );

unsigned len = searchPoints.length();
//resultPoints.setLength(len) ;
//resultIds.setLength(len) ;
//resultUVs.setLength(len) ;
//resultBarys.setLength(len) ;


MPoint c   ; // the center of the search sphere
for (unsigned i = 0; i<len; i++ ) {
c = MPoint(searchPoints[i]);
double searchRad = bigNum;
triData tb;
m_pTree->closestTri( m_pTree->root(), c ,searchRad,  tb);
if (searchRad < bigNum) {  // we found a point
resultPoints.append( tb.cachedPoint() );
resultNormals.append(tb.normal());
st = MS::kSuccess;
} else { // we never found a closest point, so something is seriously wrong
st = MS::kFailure; mser;
}
}
return st;
}
*/

MStatus triTreeData::closestPointsOnTriWithinRadius(
  const MFloatVectorArray &searchPoints,
  double initialRad,
  MIntArray &resultMask,
  MFloatVectorArray &resultPoints
) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::closestPointsOnTri" );

	if (initialRad <= 0.0) { return MS::kUnknownParameter; }
	unsigned len = searchPoints.length();
	if (!len) { return MS::kUnknownParameter; }

	resultPoints.setLength(len) ;
	resultMask.setLength(len);
	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(searchPoints[i]);
		double searchRad = initialRad;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < initialRad) {  // we found a point
			resultPoints.set( tb.cachedPoint(), i );
			resultMask.set(1, i);
		}
		else {
			resultMask.set(0, i);
		}
	}
	return MS::kSuccess;
}





// in place closest points snapper
MStatus triTreeData::snapToClosestPointsOnTri(MVectorArray &points) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::snapToClosestPointsOnTri" );

	unsigned len = points.length();
	//resultPoints.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(points[i]);
		double searchRad = bigNum;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < bigNum) {  // we found a point
			points.set( MVector(tb.cachedPoint()) , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}
// in place closest points snapper, returns normals too
MStatus triTreeData::snapToClosestPointsOnTri(MVectorArray &points,
    MVectorArray &normals) const {
	MStatus st = MS::kSuccess;
	MString method("triTreeData::snapToClosestPointsOnTri" );

	unsigned len = points.length();
	//resultPoints.setLength(len) ;

	MPoint c   ; // the center of the search sphere
	for (unsigned i = 0; i < len; i++ ) {
		c = MPoint(points[i]);
		double searchRad = bigNum;
		triData tb;
		m_pTree->closestTri( m_pTree->root(), c , searchRad,  tb);
		if (searchRad < bigNum) {  // we found a point
			points.set( MVector(tb.cachedPoint()) , i);
			normals.set(tb.normal() , i);
			st = MS::kSuccess;
		}
		else {   // we never found a closest point, so something is seriously wrong
			st = MS::kFailure; mser;
		}
	}
	return st;
}


void triTreeData::copy(const MPxData &otherData)
{
	m_pTree = ((const triTreeData &)otherData).tree();
	//m_treeCacheMap = ((const triTreeData&)otherData).treeMap();
}

triTreeData &triTreeData::operator=(const triTreeData &otherData ) {
	if (this != &otherData ) {
		m_pTree = otherData.tree();
		// m_treeCacheMap = otherData.treeMap();
	}
	return *this;
}


